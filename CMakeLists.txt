cmake_minimum_required(VERSION 3.1)
project(N2HDECAY VERSION 1.1.0 LANGUAGES Fortran)


if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	set(CMAKE_BUILD_TYPE "RELEASE" CACHE STRING "Choose the type of build.")
	if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
		set(CMAKE_BUILD_TYPE "DEBUG")
	endif()
endif()

include(CTest)

add_executable( n2hdecay
	src/n2hdecay_main.f src/n2hdmcp_hdec.f
	src/read_hdec.f src/head_hdec.f src/write_hdec.f src/close_hdec.f
	src/hdec.f src/hdecay_core.f  src/hgaga.f src/hgg.f )

# test script requires python>3.5
find_package(PythonInterp 3.5)
if(PYTHONINTERP_FOUND)
	add_test(NAME Broken_Phase COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tests/testN2HDECAY.py ${CMAKE_BINARY_DIR}/n2hdecay 1 )
	add_test(NAME DarkSinglet_Phase COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tests/testN2HDECAY.py ${CMAKE_BINARY_DIR}/n2hdecay 2 )
  add_test(NAME InertDoublet_Phase COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tests/testN2HDECAY.py ${CMAKE_BINARY_DIR}/n2hdecay 3 )
  add_test(NAME DarkSingletDoublet_Phase COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tests/testN2HDECAY.py ${CMAKE_BINARY_DIR}/n2hdecay 4 )
endif()

# cpack configuration
set(CPACK_SOURCE_PACKAGE_FILE_NAME "N2HDECAY")
list(APPEND CPACK_SOURCE_IGNORE_FILES "\.git" )
list(APPEND CPACK_SOURCE_IGNORE_FILES ".gitignore" )
list(APPEND CPACK_SOURCE_IGNORE_FILES ".gitlab-ci.yml" )
list(APPEND CPACK_SOURCE_IGNORE_FILES "/build/")
list(APPEND CPACK_SOURCE_IGNORE_FILES "/__pycache__/")
list(APPEND CPACK_SOURCE_IGNORE_FILES "/*.o/")
list(APPEND CPACK_SOURCE_IGNODE_FILES "/n2hdecay/")
include(CPack)
