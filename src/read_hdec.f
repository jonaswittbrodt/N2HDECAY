      SUBROUTINE READ_HDEC(infile)
        Implicit None
      integer NI
      PARAMETER(NI=87)
      character(len=64) infile

      double precision ams,amc,amb,amt
      double precision AMSB
      double precision amcb,AMBB
      double precision GF,ALPH,AMTAU,AMMUON,AMZ,AMW
      double precision VTB,VTS,VTD,VCB,VCS,VCD,VUB,VUS,VUD
      double precision GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      double precision XLAMBDA,AMC0,AMB0,AMT0
      integer N0, IHIGGS,NNLO
      integer IPOLE,IONSH,IONWZ,IOFSUSY,NFGG
      double precision TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM
      integer ITYPE2HDM,I2HDM,IPARAM2HDM
      double precision ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM, AMSSQ_N2HDM
      integer IN2HDM,IPHASE
      double precision amhiggs(3)
      COMMON/MASSES_HDEC/AMS,AMC,AMB,AMT
      COMMON/STRANGE_HDEC/AMSB
      COMMON/MSBAR_HDEC/AMCB,AMBB
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
      COMMON/CKMPAR_HDEC/VTB,VTS,VTD,VCB,VCS,VCD,VUB,VUS,VUD
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      COMMON/ALS_HDEC/XLAMBDA,AMC0,AMB0,AMT0,N0
      COMMON/FLAG_HDEC/IHIGGS,NNLO,IPOLE
      COMMON/ONSHELL_HDEC/IONSH,IONWZ,IOFSUSY
      COMMON/OLDFASH_HDEC/NFGG
      COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
      Common/N2HDM_HDEC_PARAM/amhiggs,ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM,
     .      IN2HDM,IPHASE

      double precision amh1_broken,amh2_broken,AMH3_broken
      double precision amh2_dark, amh1_dark, amdm_dark
      double precision amh2_darkd, amh1_darkd, amdm_darkd,
     .      ALPH2HDM_dd, AL7_N2HDM_dd
      double precision AMHsm_darksd, AMHDD_darksd, AMHDS_darksd
      double precision alsmz, acc, del, ambl,ambu,fmb,xmb
      double precision runm_hdec,xit la_hdec
      integer nloop, nber


      OPEN(NI,FILE=infile)

      IHIGGS = 5
      I2HDM = 1
      IN2HDM = 1

      IPARAM2HDM = 1
      ALPH2HDM = 0
      AMHL2HDM = 10
      AMHH2HDM = 10
      A1LAM2HDM = 1
      A2LAM2HDM = 1
      A3LAM2HDM = 1
      A4LAM2HDM = 1
      A5LAM2HDM = 1
      NNLO = 1
      IONSH = 0
      IONWZ = 0
      IPOLE = 0
      IOFSUSY = 1
      NFGG = 5

      READ(NI,*)
      READ(NI,*)
      READ(NI,*)
      READ(NI,*)
      READ(NI,101)IPHASE
      READ(NI,101)ITYPE2HDM
      READ(NI,*)
      READ(NI,100)TGBET2HDM
      READ(NI,100)AM12SQ
      READ(NI,100)AMHA2HDM
      READ(NI,100)AMHC2HDM
      Read(NI,100)Avs_N2HDM
      READ(NI,*)
      Read(NI,100)AMH1_broken
      Read(NI,100)AMH2_broken
      Read(NI,100)AMH3_broken
      Read(NI,100)Alph1_N2HDM
      Read(NI,100)Alph2_N2HDM
      Read(NI,100)Alph3_N2HDM
      READ(NI,*)
      READ(NI,100)AMH1_dark
      READ(NI,100)AMH2_dark
      READ(NI,100)AMDM_dark
      READ(NI,100)ALPH2HDM
      READ(NI,100)AL7_N2HDM
      READ(NI,100)AL8_N2HDM
      READ(NI,*)
      READ(NI,*)
      READ(NI,100)AMH1_darkd
      READ(NI,100)AMH2_darkd
      READ(NI,100)AMDM_darkd
      READ(NI,100)ALPH2HDM_dd
      READ(NI,100)AL7_N2HDM_dd
      Read(NI,100)AM11SQ_N2HDM
      READ(NI,*)
      READ(NI,*)
      READ(NI,100)AMHsm_darksd
      READ(NI,100)AMHDD_darksd
      READ(NI,100)AMHDS_darksd
      READ(NI,100)AM22SQ_N2HDM
      READ(NI,100)AMSSQ_N2HDM
      READ(NI,*)
      READ(NI,*)
      READ(NI,*)
      READ(NI,100)ALSMZ
      READ(NI,100)AMS
      READ(NI,100)AMC
      READ(NI,100)AMB
      READ(NI,100)AMT
      READ(NI,100)AMTAU
      READ(NI,100)AMMUON
      READ(NI,100)ALPH
      READ(NI,100)GF
      READ(NI,100)GAMW
      READ(NI,100)GAMZ
      READ(NI,100)AMZ
      READ(NI,100)AMW
      READ(NI,100)VTB
      READ(NI,100)VTS
      READ(NI,100)VTD
      READ(NI,100)VCB
      READ(NI,100)VCS
      READ(NI,100)VCD
      READ(NI,100)VUB
      READ(NI,100)VUS
      READ(NI,100)VUD
      READ(NI,*)

      if(iphase.eq.1)then
        if(AMH1_broken>=AMH2_broken.or.AMH2_broken>=AMH3_broken) then
          print*,'Scalar masses must be ordered as m_H1<m_H2<m_H3.'
          STOP 1
        endif
        if(itype2hdm.lt.1.or.itype2hdm.gt.4)then
          print*,'YUKTYPE must be 1, 2, 3 or 4!'
          STOP 1
        endif
        amhiggs(1)=AMH1_broken
        amhiggs(2)=AMH2_broken
        amhiggs(3)=AMH3_broken
      elseif(iphase.eq.2)then
        if(AMH1_dark.ge.AMH2_dark)then
          print*,'Scalar masses must be ordered as m_H1<m_H2.'
          STOP 1
        endif
        if(itype2hdm.lt.1.or.itype2hdm.gt.4)then
          print*,'YUKTYPE must be 1, 2, 3 or 4!'
          STOP 1
        endif
        amhiggs(1)=AMDM_dark
        amhiggs(2)=AMH1_dark
        amhiggs(3)=AMH2_dark
      elseif(iphase.eq.3)then
        if(AMH1_darkd.ge.AMH2_darkd)then
          print*,'Scalar masses must be ordered as m_H1<m_H2.'
          STOP 1
        endif
        itype2hdm=1
        amhiggs(1)=AMDM_darkd
        amhiggs(2)=AMH1_darkd
        amhiggs(3)=AMH2_darkd
        ALPH2HDM=ALPH2HDM_dd
        AL7_N2HDM=AL7_N2HDM_dd
      elseif(iphase.eq.4)then
        itype2hdm=1
        amhiggs(1)=AMHDD_darksd
        amhiggs(2)=AMHDS_darksd
        amhiggs(3)=AMHsm_darksd
      else
        print*,'PHASE must be 1, 2, 3, or 4!'
        STOP 1
      endif


c **********************************************************************
c ***************   initialize alpha_s and polylogs    *****************
      ALPH=1.D0/ALPH
      AMSB = AMS
      AMCB = AMC
      AMBB = AMB

      AMC0=AMC
      AMB0=AMB
      AMT0=AMT
      ACC=1.D-10
      NLOOP=3
      XLAMBDA=XITLA_HDEC(NLOOP,ALSMZ,ACC)
      N0=5
      CALL ALSINI_HDEC(ACC)
C--DECOUPLING THE TOP QUARK FROM ALPHAS
      AMT0=3.D8

      del = 1.d-10
      ambl = ambb
      ambu = 2*ambb
      fmb = (ambl+ambu)/2
      acc=1.d-10
      nloop=3
12    amb=fmb
      amc = amb - 3.41d0
      amb0=amb
      amc0=amc
      n0=5
      call alsini_hdec(acc)
      xmb = runm_hdec(ambb,5)
      if(abs(xmb-ambb).lt.del)then
       ambl = fmb
       ambu = fmb
      elseif(xmb.gt.ambb)then
       ambu = fmb
      else
       ambl = fmb
      endif
      fmb = (ambl+ambu)/2
      if(dabs(xmb/ambb-1).gt.del) goto 12
      amb = fmb
      amc = amb - 3.41d0
      amb0=amb
      n0=5
      call alsini_hdec(acc)
C--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
      NBER = 18
      CALL BERNINI_HDEC(NBER)
c***********************************************************************

100   FORMAT(10X,G30.20)
101   FORMAT(10X,I30)

      CLOSE(NI)

      RETURN
      END
