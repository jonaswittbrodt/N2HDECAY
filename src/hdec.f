
C =====================================================================
C ===================== SUBROUTINE FOR THE DECAYS =====================
C =====================================================================

      SUBROUTINE HDEC()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMB_HDEC
c JW changed
      double precision ghihpwm(3),ghivv(3),ghipm(3),
     .     ghijk(3,3,3),ghiza(3),ghiaa(3),
     .     ghitop(3),ghibot(3),ghilep(3),
     .     higg(3),himm(3),hill(3),hiss(3),hicc(3),hibb(3),hitt(3),
     .     higa(3),hizga(3),hiww(3),hizz(3),hiaz(3),hihpwm(3),
     .     hiaa(3),hihjhk(3,3,3),hichch(3),
     .     gamtot(3),
     .     hibrt(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),hibrc(3),
     .     hibrg(3),hibrga(3),hibrzga(3),hibrw(3),hibrz(3),
     .     hibrhpwm(3),hibrchch(3),hibraa(3),hibraz(3),
     .     hcwh(3),hcbrwh(3),
     .     hahz(3),abrhz(3),
     .     xhgg(3),xhqq(3)
      double precision amhiggs(3)
      integer IN2HDM,IPHASE
c end JW changed
      COMPLEX*16 CFACQ_HDEC
      DIMENSION XX(4),YY(4)
      COMPLEX*16 CF,CG,CI1,CI2,CA,CB,CTT,CTB,CTC,CTW,CLT,CLB,CLC,CLW,
     .           CAT,CAB,CAC,CAW,CAH,CTH,CLH,CTL,CAL
      COMPLEX*16 CLE
      COMMON/MASSES_HDEC/AMS,AMC,AMB,AMT
      COMMON/MSBAR_HDEC/AMCB,AMBB
      COMMON/ALS_HDEC/XLAMBDA,AMC0,AMB0,AMT0,N0
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
      COMMON/CKMPAR_HDEC/VTB,VTS,VTD,VCB,VCS,VCD,VUB,VUS,VUD
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      COMMON/ONSHELL_HDEC/IONSH,IONWZ,IOFSUSY
      COMMON/OLDFASH_HDEC/NFGG
      COMMON/FLAG_HDEC/IHIGGS,NNLO,IPOLE
      COMMON/WIDTHA_HDEC/ABRB,ABRL,ABRM,ABRS,ABRC,ABRT,ABRG,ABRGA,
     .              ABRZGA,ABRZ,AWDTH
      COMMON/WIDTHHL_HDEC/HLBRB,HLBRL,HLBRM,HLBRS,HLBRC,HLBRT,HLBRG,
     .               HLBRGA,HLBRZGA,HLBRW,HLBRZ,HLBRA,HLBRAZ,HLBRHW,
     .               HLWDTH
      COMMON/WIDTHHH_HDEC/HHBRB,HHBRL,HHBRM,HHBRS,HHBRC,HHBRT,HHBRG,
     .               HHBRGA,HHBRZGA,HHBRW,HHBRZ,HHBRH,HHBRA,HHBRAZ,
     .               HHBRHW,HHWDTH
      COMMON/WIDTHHC_HDEC/HCBRB,HCBRL,HCBRM,HCBRBU,HCBRS,HCBRC,HCBRT,
     .               HCBRW,HCBRA,HCWDTH
      COMMON/COUP_HDEC/GAT,GAB,GLT,GLB,GHT,GHB,GZAH,GZAL,
     .            GHHH,GLLL,GHLL,GLHH,GHAA,GLAA,GLVV,GHVV,
     .            GLPM,GHPM,B,A
      Common/N2HDM_HDEC_PARAM/amhiggs,ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM,
     .      IN2HDM,IPHASE
      Common/N2HDM_COUP/ghitop,ghibot,ghilep,
     .      ghihpwm,ghivv,ghiza,
     .      ghipm,ghiaa,ghijk
      Common/N2HDM_BR_H/h2brh1h1,h3brh1h1,h3brh2h2,h3brh1h2,
     .      hibrt,hibrb,hibrl,hibrm,hibrs,hibrc,
     .      hibrg,hibrga,hibrzga,hibrw,hibrz,
     .      hibrhpwm,hibraz,hibrchch,hibraa,
     .      gamtot
      Common/N2HDM_BR_Hc_A/hcbrwh,abrhz
      COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
      COMMON/THDM_COUP_HDEC/gllep,ghlep,galep
      COMMON/WIDTH_HC_ADD/hcbrcd,hcbrts,hcbrtd
      COMMON/WIDTH_2HDM/hcbrwhh,hhbrchch,hlbrchch,abrhhaz,abrhawphm
      common/DECPARAMETERS/amhi,amhj,amhk,gamtotj,gamtotk

      HVV(X,Y)= GF/(4.D0*PI*DSQRT(2.D0))*X**3/2.D0*BETA_HDEC(Y)
     .            *(1.D0-4.D0*Y+12.D0*Y**2)
      AFF(X,Y)= GF/(4*PI*DSQRT(2.D0))*X**3*Y*(BETA_HDEC(Y))
      HFF(X,Y)= GF/(4*PI*DSQRT(2.D0))*X**3*Y*(BETA_HDEC(Y))**3
      CFF(Z,TB,X,Y)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2+Y/TB**2)-4.D0*X*Y)
      HV(V)=3.D0*(1.D0-8.D0*V+20.D0*V**2)/DSQRT((4.D0*V-1.D0))
     .      *DACOS((3.D0*V-1.D0)/2.D0/DSQRT(V**3))
     .      -(1.D0-V)*(47.D0/2.D0*V-13.D0/2.D0+1.D0/V)
     .      -3.D0/2.D0*(1.D0-6.D0*V+4.D0*V**2)*DLOG(V)
      HVH(X,Y)=0.25D0*( (1-X)*(-2+4*X-2*X**2+9*Y+9*X*Y-6*Y**2)
     .        /(3*Y)-2*(1-X-X**2+X**3-3*Y-2*X*Y-3*X**2*Y+3*Y**2
     .        +3*X*Y**2-Y**3)*(-PI/2- DATAN((1-2*X+X**2-Y-X*Y)/
     .         ((1-X)*DSQRT(-1.D0+2*X+2*Y-(X-Y)**2))))/DSQRT(-1.D0
     .         +2*X-(X-Y)**2+2*Y)-(1+X**2-2*Y-2*X*Y+Y**2)*DLOG(X))

      QCD0(X) = (1+X**2)*(4*SP_HDEC((1-X)/(1+X))+2*SP_HDEC((X-1)/(X+1))
     .        - 3*DLOG((1+X)/(1-X))*DLOG(2/(1+X))
     .        - 2*DLOG((1+X)/(1-X))*DLOG(X))
     .        - 3*X*DLOG(4/(1-X**2)) - 4*X*DLOG(X)
      HQCDM(X)=QCD0(X)/X+(3+34*X**2-13*X**4)/16/X**3*DLOG((1+X)/(1-X))
     .        + 3.D0/8/X**2*(7*X**2-1)
      AQCDM(X)=QCD0(X)/X + (19+2*X**2+3*X**4)/16/X*DLOG((1+X)/(1-X))
     .        + 3.D0/8*(7-X**2)
      HQCD(X)=(4.D0/3*HQCDM(BETA_HDEC(X))
     .        +2*(4.D0/3-DLOG(X))*(1-10*X)/(1-4*X))*ASH/PI
     .       + (29.14671D0
     .         +RATCOUP*(1.570D0 - 2*DLOG(HIGTOP)/3
     .                                     + DLOG(X)**2/9))*(ASH/PI)**2
     .       + (164.14D0 - 25.77D0*5 + 0.259D0*5**2)*(ASH/PI)**3
     .       +(39.34D0-220.9D0*5+9.685D0*5**2-0.0205D0*5**3)*(ASH/PI)**4
      AQCD(X)=(4.D0/3*AQCDM(BETA_HDEC(X))
     .        +2*(4.D0/3-DLOG(X))*(1-6*X)/(1-4*X))*ASH/PI
     .       + (29.14671D0 + RATCOUP*(23/6.D0 - DLOG(HIGTOP)
     .                                     + DLOG(X)**2/6))*(ASH/PI)**2
     .       + (164.14D0 - 25.77D0*5 + 0.259D0*5**2)*(ASH/PI)**3
     .       +(39.34D0-220.9D0*5+9.685D0*5**2-0.0205D0*5**3)*(ASH/PI)**4
      QCDH(X)=1.D0+HQCD(X)
      QCDA(X)=1.D0+AQCD(X)
      TQCDH(X)=1.D0+4.D0/3*HQCDM(BETA_HDEC(X))*ASH/PI
      TQCDA(X)=1.D0+4.D0/3*AQCDM(BETA_HDEC(X))*ASH/PI
      QCDC(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X))
     .         - (X*2*(4/3.D0-DLOG(X)) + Y*2*(4/3.D0-DLOG(Y)))/(1-X-Y)
     .         - (X*2*(4/3.D0-DLOG(X))*(1-X+Y)
     .           +Y*2*(4/3.D0-DLOG(Y))*(1+X-Y))/LAMB_HDEC(X,Y)**2)
      QCDCI(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)) + 2*(4/3.D0-DLOG(Y))
     .         - (X*2*(4/3.D0-DLOG(X))*(1-X+Y)
     .           +Y*2*(4/3.D0-DLOG(Y))*(1+X-Y))/LAMB_HDEC(X,Y)**2)
      QCDCM(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
      QCDCMI(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
c------------------------------------------------------------------------
c--running x Yukawa coupling
      QCDCM1(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)))
      QCDCMI1(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)))
c------------------------------------------------------------------------
      CQCD(Z,TB,X,Y,R)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2*R**2*QCDC(X,Y)
     .                           +Y/TB**2*QCDC(Y,X))
     .               -4.D0*X*Y*R*QCDCI(X,Y))
      CQCDM(Z,TB,X,Y,R)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2*R**2*QCDCM1(X,Y)
     .                           +Y/TB**2*QCDCM(Y,X))
     .               -4.D0*X*Y*R*QCDCMI1(X,Y))
c MMM changed 21/8/13
      CQCD2HDM(Z,GCPD,GCPU,X,Y,R)=
     .     GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .     *((1.D0-X-Y)*(X*GCPD**2*R**2*QCDC(X,Y)
     .                  +Y*GCPU**2*QCDC(Y,X))
     .     -4.D0*GCPD*GCPU*X*Y*R*QCDCI(X,Y))
      CQCDM2HDM(Z,GCPD,GCPU,X,Y,R)=
     .     GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .     *((1.D0-X-Y)*(X*GCPD**2*R**2*QCDCM1(X,Y)
     .                  +Y*GCPU**2*QCDCM(Y,X))
     .     -4.D0*GCPD*GCPU*X*Y*R*QCDCMI1(X,Y))
c end MMM changed 21/8/13
      ELW(AMH,AMF,AMFP,QF,AI3F)=IOELW*(
     .        ELWFULL_HDEC(AMH,AMF,AMFP,QF,AI3F,AMW)
     .      + ALPH/PI*QF**2*HQCDM(BETA_HDEC(AMF**2/AMH**2))
     .      - GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0)
      ELW0(AMH,AMF,QF,ACF)=IOELW*(ALPH/PI*3.D0/2*QF**2
     .                              *(3.D0/2-DLOG(AMH**2/AMF**2))
     .      +GF/8/DSQRT(2.D0)/PI**2*(ACF*AMT**2
     .        +AMW**2*(3*DLOG(CS)/SS-5)+AMZ**2*(0.5D0
     .          -3*(1-4*SS*DABS(QF))**2)))
      CF(CA) = -CDLOG(-(1+CDSQRT(1-CA))/(1-CDSQRT(1-CA)))**2/4
      CG(CA) = CDSQRT(1-CA)/2*CDLOG(-(1+CDSQRT(1-CA))/(1-CDSQRT(1-CA)))
      CI1(CA,CB) = CA*CB/2/(CA-CB)
     .           + CA**2*CB**2/2/(CA-CB)**2*(CF(CA)-CF(CB))
     .           + CA**2*CB/(CA-CB)**2*(CG(CA)-CG(CB))
      CI2(CA,CB) = -CA*CB/2/(CA-CB)*(CF(CA)-CF(CB))
      HGGQCD(ASG,NF)=1.D0+ASG/PI*(95.D0/4.D0-NF*7.D0/6.D0)
      HGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(95.D0/4.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(149533/288.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     .              +19/8.D0*DLOG(AMH**2/AMT**2)
     . +NF*(-4157/72.D0+11/2.D0*ZETA2+5/4.D0*ZETA3
     . +2/3.D0*DLOG(AMH**2/AMT**2))
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(467.683620788D0+122.440972222D0*DLOG(AMH**2/AMT**2)
     .              +10.9409722222D0*DLOG(AMH**2/AMT**2)**2)
      PHGGQCD(ASG,NF)=1.D0+ASG/PI*(73/4.D0-7/6.D0*NF)
      PHGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(73/4.D0-7/6.D0*NF)
     . +(ASG/PI)**2*(37631/96.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     . +NF*(-7189/144.D0+11/2.D0*ZETA2+5/4.D0*ZETA3)
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(-212.447364638D0)
      DHGGQCD(ASG,NF)=1.D0+ASG/PI*(21.D0-NF*7.D0/6.D0)
      DHGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(21.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(32531/72.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     .              +19/16.D0*DLOG(AMH**2/AMT**2)
     . +NF*(-15503/288.D0+11/2.D0*ZETA2+5/4.D0*ZETA3
     .     +1/3.D0*DLOG(AMH**2/AMT**2))
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(63.7474683529D0+53.3715277778D0*DLOG(AMH**2/AMT**2)
     .              +5.47048611111D0*DLOG(AMH**2/AMT**2)**2)
      SGGQCD(ASG)=ASG/PI*7.D0/2.D0
      AGGQCD(ASG,NF)=1.D0+ASG/PI*(97.D0/4.D0-NF*7.D0/6.D0)
      AGGQCD2(ASG,NF,AMA,AMT)=1.D0+ASG/PI*(97.D0/4.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(237311/864.D0-529/24.D0*ZETA2-445/8.D0*ZETA3
     . +5*DLOG(AMA**2/AMT**2))
      HFFSELF(AMH)=1.D0+IOELW*(
     .              GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0
     .            -(GF*AMH**2/16.D0/PI**2/DSQRT(2.D0))**2*32.6567D0)
      HVVSELF(AMH)=1.D0+IOELW*(
     .              GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.800952D0
     .            +(GF*AMH**2/16.D0/PI**2/DSQRT(2.D0))**2*62.0308D0)
c     WTOP0(X,ASG,NF)=ASG/PI*(-2/3.D0*(PI**2+2*SP_HDEC(X)-2*SP_HDEC(1-X)
c    .               +(4*X*(1-X-2*X**2)*DLOG(X)
c    .                +2*(1-X)**2*(5+4*X)*DLOG(1-X)
c    .                -(1-X)*(5+9*X-6*X**2))
c    .               /2/(1-X)**2/(1+2*X)))
      WTOP(X,ASG,NF)=ASG/PI*(-2/3.D0*(PI**2+2*SP_HDEC(X)-2*SP_HDEC(1-X)
     .               +(4*X*(1-X-2*X**2)*DLOG(X)
     .                +2*(1-X)**2*(5+4*X)*DLOG(1-X)
     .                -(1-X)*(5+9*X-6*X**2))
     .               /2/(1-X)**2/(1+2*X)))
     .       + (ASG/2/PI)**2*(-110.4176D0+7.978D0*NF)
c     CHTOP0(X)=-4/3.D0*((5/2.D0-1/X)*DLOG(1-X)+X/(1-X)*DLOG(X)
c    .            +SP_HDEC(X)-SP_HDEC(1-X)+PI**2/2-9/4.D0)
      CHTOP(X)=8/3.D0*(SP_HDEC(1-X)-X/2/(1-X)*DLOG(X)
     .        + DLOG(X)*DLOG(1-X)/2+(1-2.5D0*X)/2/X*DLOG(1-X)-PI**2/3
     .        +9/8.D0)
      CHTOP1(X)=8/3.D0*(SP_HDEC(1-X)-X/2/(1-X)*DLOG(X)
     .        + DLOG(X)*DLOG(1-X)/2+(1-2.5D0*X)/2/X*DLOG(1-X)-PI**2/3
     .        +17/8.D0)

      PI=4D0*DATAN(1D0)
      SS=1.D0-(AMW/AMZ)**2
      CS=1.D0-SS

      ZETA2 = PI**2/6
      ZETA3 = 1.202056903159594D0
      ZETA4 = PI**4/90
      ZETA5 = 1.03692775514337D0






c JW changed
C ==================================================================== C
C                        HIGGS DECAYS IN THE N2HDM
C ==================================================================== C

         tgbet = TGBET2HDM
         amch = AMHC2HDM
         ama = AMHA2HDM

         call N2HDMcp_hdec()

         v = 1/dsqrt(dsqrt(2.D0)*gf)

         do ii=1,3,1
            ghipm(ii) = v/amz**2*ghipm(ii)
            ghiaa(ii) = v/amz**2*ghiaa(ii)
            do jj=1,3,1
               do kk=1,3,1
                  ghijk(ii,jj,kk) = v/amz**2*ghijk(ii,jj,kk)
               end do
            end do
         end do

c         print*,'v/mz^2*v**2',v/amz**2*v**2


C--DECOUPLING THE TOP QUARK FROM ALPHAS
      AMT0=3.D8
C--TOP QUARK DECAY WIDTH
      GAMT0 = GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2
     .      * LAMB_HDEC(AMB**2/AMT**2,AMW**2/AMT**2)
     .      * ((1-AMW**2/AMT**2)*(1+2*AMW**2/AMT**2)
     .         -AMB**2/AMT**2*(2-AMW**2/AMT**2-AMB**2/AMT**2))
     .      * (1+WTOP(AMW**2/AMT**2,ALPHAS_HDEC(AMT,3),5))
      IF(IHIGGS.NE.0.AND.AMT.GT.AMCH+AMB)THEN
      XGLB = GLB
      XGHB = GHB
      XGAB = GAB
       YMB = RUNM_HDEC(AMT,5)
       GAMT1 = GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2
     .      * LAMB_HDEC(AMB**2/AMT**2,AMCH**2/AMT**2)
     .      * (GAT**2*(1+AMB**2/AMT**2-AMCH**2/AMT**2)
     .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP(AMCH**2/AMT**2))
     .        +(YMB/AMT)**2*XGAB**2*(1+AMB**2/AMT**2-AMCH**2/AMT**2)
     .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP1(AMCH**2/AMT**2))
     .      + 4*YMB**2/AMT**2*XGAB*GAT)
      ELSE
       GAMT1 = 0
      ENDIF
      GAMT1 = GAMT0+GAMT1

c -------------------------------------------------------------------- c
c                       Neutral Higgs boson decays
c -------------------------------------------------------------------- c
      do i=1,3,1
         amhi = amhiggs(i)

         ghit = ghitop(i)
         ghib = ghibot(i)
         ghil = ghilep(i)

         RMS = RUNM_HDEC(amhi,3)
         RMC = RUNM_HDEC(amhi,4)
         RMB = RUNM_HDEC(amhi,5)
         RMT = RUNM_HDEC(amhi,6)

         HIGTOP = amhi**2/AMT**2

         ASH=ALPHAS_HDEC(amhi,3)

C     =============== PARTIAL WIDTHS
C  Hi ---> G G
       EPS=1.D-8
       NFEXT = 5
       ASG = ASH
       CTT = 4*AMT**2/amhi**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/amhi**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/amhi**2*DCMPLX(1D0,-EPS)

       CAT = 2*CTT*(1+(1-CTT)*CF(CTT))*ghit
       CAB = 2*CTB*(1+(1-CTB)*CF(CTB))*ghib
       CAC = 2*CTC*(1+(1-CTC)*CF(CTC))*ghit

       FQCD=HGGQCD2(ASG,5,amhi,AMT)
       XFAC = CDABS(CAT+CAB+CAC)**2*FQCD
       HGG=HVV(amhi,0.D0)*(ASG/PI)**2*XFAC/8

      higg(i) = hgg

c      print*,'hgg_NNLO',higg(i)

C  H ---> MU MU
      IF(amhi.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
       HMM=HFF(amhi,(AMMUON/amhi)**2)*ghil**2
      ENDIF

      himm(i) = hmm

c      print*,'Hi -> mumu',himm(i)

C  H ---> TAU TAU
      IF(amhi.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
         HLL=HFF(amhi,(AMTAU/amhi)**2)*ghil**2
      ENDIF

      hill(i) = hll

c      print*,'Hi -> tautau',hill(i)

C  H --> SS
      if(abs(ghib).lt.1e-10) then
        RATCOUP = 1
      ELSE
        RATCOUP = ghit/ghib
      endif

      IF(amhi.LE.2*AMS) THEN
       HSS = 0
      ELSE
         HS1=3.D0*HFF(amhi,(AMS/amhi)**2)
     .        *ghib**2
     .        *TQCDH(AMS**2/amhi**2)
         HS2=3.D0*HFF(amhi,(RMS/amhi)**2)*ghib**2
     .        *QCDH(RMS**2/amhi**2)
         IF(HS2.LT.0.D0) HS2 = 0
         RAT = 2*AMS/amhi
         HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF

      hiss(i) = hss

c      print*,'Hi -> ss',hiss(i)

C  H --> CC
      RATCOUP = 1
      IF(amhi.LE.2*AMC) THEN
       HCC = 0
      ELSE
          HC1=3.D0*HFF(amhi,(AMC/amhi)**2)
     .         *ghit**2
     .         *TQCDH(AMC**2/amhi**2)
          HC2=3.D0*HFF(amhi,(RMC/amhi)**2)*ghit**2
     .         *QCDH(RMC**2/amhi**2)
          IF(HC2.LT.0.D0) HC2 = 0
          RAT = 2*AMC/amhi
          HCC = QQINT_HDEC(RAT,HC1,HC2)
      ENDIF

      hicc(i) = hcc

c      print*,'Hi -> cc',hicc(i)

C  H --> BB :

      QQ = AMB
      if(abs(ghib).lt.1e-10) then
        RATCOUP = 1
      ELSE
        RATCOUP = ghit/ghib
      endif
      IF(amhi.LE.2*AMB) THEN
       HBB = 0
      ELSE
         HB1=3.D0*HFF(amhi,(AMB/amhi)**2)
     .        *ghib**2
     .        *TQCDH(AMB**2/amhi**2)
         HB2=3.D0*HFF(amhi,(RMB/amhi)**2)
     .        *ghib**2
     .        *QCDH(RMB**2/amhi**2)
         IF(HB2.LT.0.D0) HB2 = 0
         RAT = 2*AMB/amhi
         HBB = QQINT_HDEC(RAT,HB1,HB2)
      ENDIF

      hibb(i) = hbb

c      print*,'Hi -> bb',hibb(i)

C  H ---> TT
      RATCOUP = 0
      raatcoup = 0.D0

      if(ionsh.eq.0)then
         dld=5.D0
         dlu=3.D0
         xm1 = 2d0*amt-dld
         xm2 = 2d0*amt+dlu
         if (amhi.le.amt+amw+amb.or.abs(ghit).lt.1d-100) then
            htt=0.d0
         elseif (amhi.le.xm1) then
            factt=6.d0*gf**2*amhi**3*amt**2/2.d0/128.d0/pi**3
            call HTOTT_HDEC(amhi,amt,amb,amw,amch,
     .                 ghit,ghib,gat,gab,ghivv(i),
     .                 ghihpwm(i),htt0)
            htt=factt*htt0
         elseif (amhi.le.xm2) then
            XX(1) = XM1-1D0
            XX(2) = XM1
            XX(3) = XM2
            XX(4) = XM2+1D0

            factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
            call HTOTT_HDEC(xx(1),amt,amb,amw,amch,
     .                 ghit,ghib,gat,gab,ghivv(i),
     .                 ghihpwm(i),htt0)
            yy(1)=factt*htt0

            factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
            call HTOTT_HDEC(xx(2),amt,amb,amw,amch,
     .                 ghit,ghib,gat,gab,ghivv(i),
     .                 ghihpwm(i),htt0)
            yy(2)=factt*htt0

            xmt = runm_hdec(xx(3),6)
            ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*ghit**2
     .           *tqcdh(amt**2/xx(3)**2)
            ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*ghit**2
     .           *qcdh(xmt**2/xx(3)**2)
            if(ht2.lt.0.d0) ht2 = 0
            rat = 2*amt/xx(3)
            yy(3) = qqint_hdec(rat,ht1,ht2)

            xmt = runm_hdec(xx(4),6)
            ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*ghit**2
     .           *tqcdh(amt**2/xx(4)**2)
            ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*ghit**2
     .           *qcdh(xmt**2/xx(4)**2)
            if(ht2.lt.0.d0) ht2 = 0
            rat = 2*amt/xx(4)
            yy(4) = qqint_hdec(rat,ht1,ht2)

            htt=fint_hdec(amhi,xx,yy)
         else
            ht1=3.d0*hff(amhi,(amt/amhi)**2)*ghit**2
     .           *tqcdh(amt**2/amhi**2)
            ht2=3.d0*hff(amhi,(rmt/amhi)**2)*ghit**2
     .           *qcdh(rmt**2/amhi**2)
            if(ht2.lt.0.d0) ht2 = 0
            rat = 2.D0*amt/amhi
            htt = qqint_hdec(rat,ht1,ht2)
         endif
      else
         if (amhi.le.2.d0*amt) then
            htt=0.d0
         else
            ht1=3.d0*hff(amhi,(amt/amhi)**2)*ghit**2
     .           *tqcdh(amt**2/amhi**2)
            ht2=3.d0*hff(amhi,(rmt/amhi)**2)*ghit**2
     .           *qcdh(rmt**2/amhi**2)
            if(ht2.lt.0.d0) ht2 = 0
            rat = 2.D0*amt/amhi
            htt = qqint_hdec(rat,ht1,ht2)
         endif
      endif

      hitt(i) = htt

c      print*,'Hi -> tt',hitt(i)

C  H ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(amhi/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(amhi/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(amhi/2,6)*AMT/RUNM_HDEC(AMT,6)

       CTT = 4*XRMT**2/amhi**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/amhi**2*DCMPLX(1D0,-EPS)
       CTC = 4*XRMC**2/amhi**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/amhi**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/amhi**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/amhi**2*DCMPLX(1D0,-EPS)
       CAT = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*ghit
     .     * CFACQ_HDEC(0,amhi,XRMT)

c       print*,'ghit',ghit,ghib,ghil,ghivv(i),
c     .      ghipm(i)

       CAB = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*ghib
     .     * CFACQ_HDEC(0,amhi,XRMB)
       CAC = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*ghit
     .     * CFACQ_HDEC(0,amhi,XRMC)
       CAL = 1.D0  * 2*CTL*(1+(1-CTL)*CF(CTL))*ghil
       CAW = -(2+3*CTW+3*CTW*(2-CTW)*CF(CTW))*ghivv(i)
       CAH = -AMZ**2/2/AMCH**2*CTH*(1-CTH*CF(CTH))*ghipm(i)

       xfac = cdabs(cat+cab+cac+cal+caw+cah)**2

       HGA=HVV(amhi,0.D0)*(ALPH/PI)**2/16.D0*XFAC

       higa(i) = hga

c       print*,'Hi -> gamgam',higa(i),amhi
c       print*,'cat,cab,cac',cat,cab,cac
c       print*,'cal,caw,cah',cal,caw,cah

C  H ---> Z GAMMA
      XRMC = RUNM_HDEC(amhi/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(amhi/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(amhi/2,6)*AMT/RUNM_HDEC(AMT,6)

      IF(amhi.LE.AMZ)THEN
       HZGA=0
      ELSE
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*ghit
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS)*ghib
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*ghit
       FL = (-1+4*SS)/DSQRT(SS*CS)*ghil

       EPS=1.D-8

       CTT = 4*AMT**2/amhi**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/amhi**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/amhi**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/amhi**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/amhi**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/amhi**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLW = 4*AMW**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLH = 4*AMCH**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
       CAB = FB*(CI1(CTB,CLB) - CI2(CTB,CLB))
       CAC = FC*(CI1(CTC,CLC) - CI2(CTC,CLC))
       CAL = FL*(CI1(CTL,CLE) - CI2(CTL,CLE))
       CAW = -1/DSQRT(TS)*(4*(3-TS)*CI2(CTW,CLW)
     .      + ((1+2/CTW)*TS - (5+2/CTW))*CI1(CTW,CLW))*GHIVV(i)
       CAH = (1-2*SS)/DSQRT(SS*CS)*AMZ**2/2/AMCH**2*
     .      CI1(CTH,CLH)*GHIPM(i)

       xfac = cdabs(cat+cab+cac+cal+caw+cah)**2

       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*amhi**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/amhi**2)**3
      ENDIF

      hizga(i) = hzga

c      print*,'Hi -> Zgam',hizga(i)

C  H ---> W W
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,amhi,AMW,GAMW,HTWW)
       HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/amhi**3*HTWW*ghivv(i)**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMW-DLD
       XM2 = 2D0*AMW+DLU
       IF (amhi.LE.XM1) THEN
        CALL HTOVV_HDEC(0,amhi,AMW,GAMW,HTWW)
        HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/amhi**3*HTWW*ghivv(i)**2
       ELSEIF (amhi.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMW,GAMW,HTWW)
        YY(1)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(1)**3*HTWW
        CALL HTOVV_HDEC(0,XX(2),AMW,GAMW,HTWW)
        YY(2)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(2)**3*HTWW
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
        HWW = FINT_HDEC(amhi,XX,YY)*ghivv(i)**2
       ELSE
        HWW=HVV(amhi,AMW**2/amhi**2)*ghivv(i)**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMW-DLD
      XM2 = 2D0*AMW+DLU
      IF (amhi.LE.AMW) THEN
       HWW=0
      ELSE IF (amhi.LE.XM1) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       HWW=HV(AMW**2/amhi**2)*CWW*amhi*ghivv(i)**2
      ELSE IF (amhi.LT.XM2) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMW**2/XX(1)**2)*CWW*XX(1)
       YY(2)=HV(AMW**2/XX(2)**2)*CWW*XX(2)
       YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
       YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
       HWW = FINT_HDEC(amhi,XX,YY)*ghivv(i)**2
      ELSE
       HWW=HVV(amhi,AMW**2/amhi**2)*ghivv(i)**2
      ENDIF
      ENDIF

      hiww(i) = hww

c      print*,'Hi -> WW',hiww(i)

C  H ---> Z Z
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,amhi,AMZ,GAMZ,HTZZ)
       HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/amhi**3*HTZZ*ghivv(i)**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMZ-DLD
       XM2 = 2D0*AMZ+DLU
       IF (amhi.LE.XM1) THEN
        CALL HTOVV_HDEC(0,amhi,AMZ,GAMZ,HTZZ)
        HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/amhi**3*HTZZ*ghivv(i)**2
       ELSEIF (amhi.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMZ,GAMZ,HTZZ)
        YY(1)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(1)**3*HTZZ
        CALL HTOVV_HDEC(0,XX(2),AMZ,GAMZ,HTZZ)
        YY(2)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(2)**3*HTZZ
        YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
        YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
        HZZ = FINT_HDEC(amhi,XX,YY)*ghivv(i)**2
       ELSE
        HZZ=HVV(amhi,AMZ**2/amhi**2)/2.D0*ghivv(i)**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMZ-DLD
      XM2 = 2D0*AMZ+DLU
      IF (amhi.LE.AMZ) THEN
       HZZ=0
      ELSE IF (amhi.LE.XM1) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       HZZ=HV(AMZ**2/amhi**2)*CZZ*amhi*ghivv(i)**2
      ELSE IF (amhi.LT.XM2) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMZ**2/XX(1)**2)*CZZ*XX(1)
       YY(2)=HV(AMZ**2/XX(2)**2)*CZZ*XX(2)
       YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2D0
       YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2D0
       HZZ = FINT_HDEC(amhi,XX,YY)*ghivv(i)**2
      ELSE
       HZZ=HVV(amhi,AMZ**2/amhi**2)/2.D0*ghivv(i)**2
      ENDIF
      ENDIF

      hizz(i) = hzz

c      print*,'Hi -> ZZ',hizz(i)

C  Hi ---> H+ W+

      if(ionsh.eq.0) then
         dld=3d0
         dlu=9d0
         xm1 = amch+amw-dld
         xm2 = amch+amw+dlu
         if (amhi.le.amch) then
            hihpwm(i)=0.d0
         elseif (amhi.lt.xm1) then
            if(amhi.le.dabs(amw-amch))then
               hihpwm(i)=0.D0
            else
               hihpwm(i)=9.d0*gf**2/16.d0/pi**3*amw**4*amhi*
     .              ghihpwm(i)**2*2
     .              *hvh((amch/amhi)**2,(amw/amhi)**2)
            endif
         elseif (amhi.lt.xm2) then
            xx(1) = xm1-1d0
            xx(2) = xm1
            xx(3) = xm2
            xx(4) = xm2+1d0
            yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .           *hvh((amch/xx(1))**2,(amw/xx(1))**2)
            yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .           *hvh((amch/xx(2))**2,(amw/xx(2))**2)
            chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .           *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
            yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
            chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .           *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
            yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw

            hihpwm(i)=fint_hdec(amhi,xx,yy)* ghihpwm(i)**2
         else
            chw=lamb_hdec(amch**2/amhi**2,amw**2/amhi**2)
     .           *lamb_hdec(amhi**2/amw**2,amch**2/amw**2)**2
            hihpwm(i)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amhi*chw*
     .           ghihpwm(i)**2
         endif
      else
         if (amhi.lt.amw+amch) then
            hihpwm(i)=0.d0
         else
            chw=lamb_hdec(amch**2/amhi**2,amw**2/amhi**2)
     .           *lamb_hdec(amhi**2/amw**2,amch**2/amw**2)**2
            hihpwm(i)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amhi*chw*
     .           ghihpwm(i)**2
         endif
      endif

c      print*,'Hi -> H+W- + H-W+',hihpwm(i),hihpwm(i)/2.D0

c  Hi ---> A Z
      if(ionsh.eq.0) then
         dld=1d0
         dlu=8d0
         xm1 = ama+amz-dld
         xm2 = ama+amz+dlu
         if (amhi.le.ama) then
            haz=0
         elseif (amhi.lt.xm1) then
            if(amhi.le.dabs(amz-ama).or.ghiza(i).le.1D-10)then
               haz=0
            else
c maggie changed 10/10/16
               haz=9.d0*gf**2/8.d0/pi**3*amz**4*amhi*ghiza(i)**2*
     .                 (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .                 *hvh((ama/amhi)**2,(amz/amhi)**2)
c end maggie changed 10/10/16
            endif
         elseif (amhi.lt.xm2) then
            xx(1) = xm1-1d0
            xx(2) = xm1
            xx(3) = xm2
            xx(4) = xm2+1d0
c maggie changed 10/10/16
            yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(1))**2,(amz/xx(1))**2)
            yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(2))**2,(amz/xx(2))**2)
c end maggie changed 10/10/16
            caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
            yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
            caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
            yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
            haz = fint_hdec(amhi,xx,yy)*ghiza(i)**2
         else
            caz=lamb_hdec(ama**2/amhi**2,amz**2/amhi**2)
     .              *lamb_hdec(amhi**2/amz**2,ama**2/amz**2)**2
            haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amhi*caz*ghiza(i)**2
         endif
      else
         if (amhi.lt.amz+ama) then
            haz=0
         else
            caz=lamb_hdec(ama**2/amhi**2,amz**2/amhi**2)
     .              *lamb_hdec(amhi**2/amz**2,ama**2/amz**2)**2
            haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amhi*caz*ghiza(i)**2
         endif
      endif

      hiaz(i) = haz

C  Hi ---> A A
      haa = 0.D0
      if (amhi.le.2*ama) then
         haa=0
      else
         haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amhi*
     .       beta_hdec(ama**2/amhi**2)*ghiaa(i)**2
      endif
      hiaa(i) = haa


C  Hi ---> Hj Hk

      do ii=1,3,1
         do jj=1,3,1
            do kk = 1,3,1
               hihjhk(ii,jj,kk) = 0.D0
            end do
         end do
      end do

      if(i.eq.2) then
         if(amhi.le.2.D0*amhiggs(1)) then
            hihjhk(2,1,1) = 0.D0
         else
            hihjhk(2,1,1) = GF/16.D0/DSQRT(2D0)/PI*AMZ**4/amhi
     .   *BETA_HDEC(amhiggs(1)**2/amhi**2)*ghijk(1,1,2)**2
         endif
      endif

      if(i.eq.3) then
         if(amhi.le.2.D0*amhiggs(1)) then
            hihjhk(3,1,1) = 0.D0
         else
            hihjhk(3,1,1) = GF/16.D0/DSQRT(2D0)/PI*AMZ**4/amhi
     .   *BETA_HDEC(amhiggs(1)**2/amhi**2)*ghijk(1,1,3)**2
         endif
         if(amhi.le.2.D0*amhiggs(2)) then
            hihjhk(3,2,2) = 0.D0
         else
            hihjhk(3,2,2) = GF/16.D0/DSQRT(2D0)/PI*AMZ**4/amhi
     .   *BETA_HDEC(amhiggs(2)**2/amhi**2)*ghijk(2,2,3)**2
         endif
         if(amhi.le.amhiggs(1)+amhiggs(2)) then
            hihjhk(3,1,2) = 0.D0
         else
            hihjhk(3,1,2) = GF/8.D0/DSQRT(2D0)/PI*AMZ**4/amhi
     .   *lamb_HDEC(amhiggs(2)**2/amhi**2,amhiggs(1)**2/amhi**2)*
     .           ghijk(1,2,3)**2
         endif
      endif

c      print*,'h',hihjhk(2,1,1),hihjhk(3,1,1),hihjhk(3,2,2),hihjhk(1,2,3)

C  Hi ---> H+ H-

      if (amhi.le.2*amch) then
         hichch(i)=0.D0
      else
         hichch(i)=gf/8d0/dsqrt(2d0)/pi*amz**4/amhi*
     .        beta_hdec(amch**2/amhi**2)*ghipm(i)**2
      endif

c      print*,'Hi -> H+ H-',hichch(i)

c -------------------- total width and branching ratios -------------- c
      gamtot(i)=hill(i)+himm(i)+hiss(i)+hicc(i)+hibb(i)+hitt(i)
     .     +higg(i)+higa(i)+hizga(i)+hiww(i)+hizz(i)+hiaz(i)+hiaa(i)
     .     +hichch(i)+hihpwm(i)

c      print*,''
c      print*,'val',hill(i),himm(i),hiss(i),hicc(i),hibb(i),hitt(i),
c     .     higg(i),higa(i),hizga(i),hiww(i),hizz(i),hichch(i),
c     .     hihpwm(i),gamtot(i),i
c      print*,''
c      print*,'HLL',hill(i)
c      print*,'HMM',himm(i)
c      print*,'HSS',hiss(i)
c      print*,'HCC',hicc(i)
c      print*,'HBB',hibb(i)
c      print*,'HTT',hitt(i)
c      print*,'HGG',higg(i)
c      print*,'HGA',higa(i)
c      print*,'HZGA',hizga(i)
c      print*,'HWW',hiww(i)
c      print*,'HZZ',hizz(i)
c      print*,'HAZ',hiaz(i)
c      print*,'HAA',hiaa(i)
c      print*,'HHpHm',hichch(i)
c      print*,'HHpWm',hihpwm(i)
c      print*,'WTOT',gamtot(i)
c      print*,'1,tot',gamtot(i)

      if(i.eq.2) then
         gamtot(i) = gamtot(i)+hihjhk(i,1,1)
      endif

      if(i.eq.3) then
         gamtot(i) = gamtot(i)+hihjhk(i,1,1)+hihjhk(i,2,2)+hihjhk(i,1,2)
      endif
      if(gamtot(i).gt.0) then
        hibrt(i)=hitt(i)/gamtot(i)
        hibrb(i)=hibb(i)/gamtot(i)
        hibrl(i)=hill(i)/gamtot(i)
        hibrm(i)=himm(i)/gamtot(i)
        hibrs(i)=hiss(i)/gamtot(i)
        hibrc(i)=hicc(i)/gamtot(i)

        hibrg(i)=higg(i)/gamtot(i)
        hibrga(i)=higa(i)/gamtot(i)
        hibrzga(i)=hizga(i)/gamtot(i)
        hibrw(i)=hiww(i)/gamtot(i)
        hibrz(i)=hizz(i)/gamtot(i)

        hibrhpwm(i) = hihpwm(i)/gamtot(i)
        hibraz(i) = hiaz(i)/gamtot(i)

        hibrchch(i)=hichch(i)/gamtot(i)
        hibraa(i) = hiaa(i)/gamtot(i)

        if(i.eq.2) then
           h2brh1h1 = hihjhk(i,1,1)/gamtot(i)
        endif

        if(i.eq.3) then
           h3brh1h1 = hihjhk(i,1,1)/gamtot(i)
           h3brh2h2 = hihjhk(i,2,2)/gamtot(i)
           h3brh1h2 = hihjhk(i,1,2)/gamtot(i)
        endif
      else
        hibrt(i)=hitt(i)
        hibrb(i)=hibb(i)
        hibrl(i)=hill(i)
        hibrm(i)=himm(i)
        hibrs(i)=hiss(i)
        hibrc(i)=hicc(i)
        hibrg(i)=higg(i)
        hibrga(i)=higa(i)
        hibrzga(i)=hizga(i)
        hibrw(i)=hiww(i)
        hibrz(i)=hizz(i)
        hibrhpwm(i) = hihpwm(i)
        hibraz(i) = hiaz(i)
        hibrchch(i)=hichch(i)
        hibraa(i) = hiaa(i)
      endif

      end do

c -------------------------------------------------------------------- c
c                       Charged Higgs boson decays
c -------------------------------------------------------------------- c
C =============  RUNNING MASSES
      RMS = RUNM_HDEC(AMCH,3)
      RMC = RUNM_HDEC(AMCH,4)
      RMB = RUNM_HDEC(AMCH,5)
      RMT = RUNM_HDEC(AMCH,6)
      ASH=ALPHAS_HDEC(AMCH,3)

C  H+ ---> MU NMU
      IF((AMCH.LE.AMMUON).or.(abs(galep).lt.1d-100)) THEN
       HcMN = 0
      ELSE
      HcMN=CFF(AMCH,galep,(AMMUON/AMCH)**2,0.D0)
      ENDIF

c      print*,''
c      print*,'H+ decay widths',amch
c      print*,'H+ -> nu mu',hcmn

C  H+ ---> TAU NTAU
      IF((AMCH.LE.AMTAU).or.(abs(galep).lt.1d-100)) THEN
       hcLN = 0
      ELSE
      HcLN=CFF(AMCH,galep,(AMTAU/AMCH)**2,0.D0)
      ENDIF

c      print*,'H+ -> tau ntau',hcln

C  H+ --> SU
      EPS = 1.D-12
      RATX = 1
      IF(AMCH.LE.AMS+EPS) THEN
       hcSU = 0
      ELSE
       hsu1=3.d0*vus**2*
     .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,eps,ratx)
       hsu2=3.d0*vus**2*cqcd2hdm(amch,gab,gat,(rms/amch)**2,eps,
     .         ratx)
       IF(HSU2.LT.0.D0) HSU2 = 0
       RAT = AMS/AMCH
       HcSU = QQINT_HDEC(RAT,HSU1,HSU2)
      ENDIF

c      print*,'H+ -> su',hcsu

C  H+ --> CS
      RATX = RMS/AMS
      RATY = 1
      IF(AMCH.LE.AMS+AMC) THEN
       hcSC = 0
      ELSE
       hsc1=3.d0*VCS**2*
     .        cqcdm2hdm(amch,gab,gat,(ams/amch)**2,(amc/amch)**2,ratx)
       hsc2=3.d0*VCS**2*
     .      cqcd2hdm(amch,gab,gat,(rms/amch)**2,(rmc/amch)**2,raty)
       IF(HSC2.LT.0.D0) HSC2 = 0
       RAT = (AMS+AMC)/AMCH
       HcSC = QQINT_HDEC(RAT,HSC1,HSC2)
      ENDIF

c      print*,'H+ -> cs',hcsc

C  H+ --> CD
      RATX = 1
      EPS = 1D-12
      IF(AMCH.LE.AMC) THEN
       hcCD = 0
      ELSE
       hCD1=3.d0*vcd**2*
     .        cqcdm2hdm(amch,gab,gat,eps,(amc/amch)**2,ratx)
       hCD2=3.d0*vcd**2*
     .      cqcd2hdm(amch,gab,gat,eps,(rmc/amch)**2,ratx)
       IF(HCD2.LT.0.D0) HCD2 = 0
       RAT = (EPS+AMC)/AMCH
       HcCD = QQINT_HDEC(RAT,HCD1,HCD2)
      ENDIF

c      print*,'H+ -> cd',hccd

C  H+ --> CB
      RATX = 1
      QQ = AMB
      IF(AMCH.LE.AMB+AMC) THEN
       hcBC = 0
      ELSE
       hbc1=3.d0*vcb**2*
     .      cqcdm2hdm(amch,gab,gat,(amb/amch)**2,(amc/amch)**2,ratx)
       hbc2=3.d0*vcb**2*
     .      cqcd2hdm(amch,gab,gat,(rmb/amch)**2,(rmc/amch)**2,ratx)
       IF(HBC2.LT.0.D0) HBC2 = 0
       RAT = (AMB+AMC)/AMCH
       HcBC = QQINT_HDEC(RAT,HBC1,HBC2)
      ENDIF

c      print*,'H+ -> cb',hcbc

C  H+ --> BU
      EPS = 1.D-12
      IF(AMCH.LE.AMB+EPS) THEN
       hcBU = 0
      ELSE
       hbu1=3.d0*vub**2*
     .        cqcdm2hdm(amch,gab,gat,(amb/amch)**2,eps,ratx)
       hbu2=3.d0*vub**2*
     .      cqcd2hdm(amch,gab,gat,(rmb/amch)**2,eps,ratx)
       IF(HBU2.LT.0.D0) HBU2 = 0
       RAT = AMB/AMCH
       HcBU = QQINT_HDEC(RAT,HBU1,HBU2)
      ENDIF

c      print*,'H+ -> ub',hcbu

C  H+ --> TD :
      EPS = 1.D-12
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT-DLD
       XM2 = AMT+DLU
       IF (AMCH.LE.AMW.or.abs(gat).lt.1D-100) THEN
        hcDT=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        hcDT=VTD**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(1)=VTD**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(2)=VTD**2*FACTB*CTT0
        XMB = RUNM_HDEC(XX(3),5)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(3),gab,gat,(EPS/XX(3))**2,(XMT/XX(3))**2,RATX)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(3),gab,gat,(EPS/XX(3))**2,(AMT/XX(3))**2,RATX)
        RAT = (AMT)/XX(3)
        YY(3) = VTD**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMB = RUNM_HDEC(XX(4),5)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(4),gab,gat,(EPS/XX(4))**2,(XMT/XX(4))**2,RATX)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(4),gab,gat,(EPS/XX(4))**2,(AMT/XX(4))**2,RATX)
        RAT = (AMT)/XX(4)
        YY(4) = VTD**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        hcDT = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HDT2=3.D0*VTD**2*
     .  CQCD2HDM(AMCH,gab,gat,(EPS/AMCH)**2,(RMT/AMCH)**2,RATX)
        IF(HDT2.LT.0.D0) HDT2 = 0
        HDT1=3.D0*VTD**2*
     .  CQCDM2HDM(AMCH,gab,gat,(EPS/AMCH)**2,(AMT/AMCH)**2,RATX)
        RAT = (AMT)/AMCH
        hcDT = QQINT_HDEC(RAT,HDT1,HDT2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT) THEN
        hcDT=0.D0
       ELSE
        HDT2=3.D0*VTD**2*
     .  CQCD2HDM(AMCH,gab,gat,EPS,(RMT/AMCH)**2,RATX)
        IF(HDT2.LT.0.D0) HDT2 = 0
        HDT1=3.D0*VTD**2*
     .  CQCDM2HDM(AMCH,gab,gat,EPS,(AMT/AMCH)**2,RATX)
        RAT = (AMT)/AMCH
        hcDT = QQINT_HDEC(RAT,HDT1,HDT2)
       ENDIF
      ENDIF

c      print*,'H+ -> td',hcdt

C  H+ --> TS :
      EPS = 1.D-12
      RATX = RMS/AMS
      RATY = 1
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT+AMS-DLD
       XM2 = AMT+AMS+DLU
       IF (AMCH.LE.AMW+2*AMS.or.abs(gat).le.1D-100) THEN
        hcST=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        hcST=VTS**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(1)=VTS**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(2)=VTS**2*FACTB*CTT0
        XMS = RUNM_HDEC(XX(3),3)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(3),gab,gat,(XMS/XX(3))**2,(XMT/XX(3))**2,RATY)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(3),gab,gat,(AMS/XX(3))**2,(AMT/XX(3))**2,RATX)
        RAT = (AMS+AMT)/XX(3)
        YY(3) = VTS**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMS = RUNM_HDEC(XX(4),3)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(4),gab,gat,(XMS/XX(4))**2,(XMT/XX(4))**2,RATY)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(4),gab,gat,(AMS/XX(4))**2,(AMT/XX(4))**2,RATX)
        RAT = (AMS+AMT)/XX(4)
        YY(4) = VTS**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        hcST = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HST2=3.D0*VTS**2*
     .  CQCD2HDM(AMCH,gab,gat,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
        IF(HST2.LT.0.D0) HST2 = 0
        HST1=3.D0*VTS**2*
     .  CQCDM2HDM(AMCH,gab,gat,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
        RAT = (AMS+AMT)/AMCH
        hcST = QQINT_HDEC(RAT,HST1,HST2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT+AMS) THEN
        hcST=0.D0
       ELSE
        HST2=3.D0*VTS**2*
     .  CQCD2HDM(AMCH,gab,gat,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
        IF(HST2.LT.0.D0) HST2 = 0
        HST1=3.D0*VTS**2*
     .  CQCDM2HDM(AMCH,gab,gat,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
        RAT = (AMS+AMT)/AMCH
        hcST = QQINT_HDEC(RAT,HST1,HST2)
       ENDIF
      ENDIF

c      print*,'H+ -> ts',hcst

C  H+ --> TB :
      RATX = RMB/AMB
      RATY = 1
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT+AMB-DLD
       XM2 = AMT+AMB+DLU
       IF (AMCH.LE.AMW+2*AMB.or.abs(gat).le.1D-100) THEN
        hcBT=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,AMB,AMW,i2hdm,gat,gab,CTT0)
        hcBT=VTB**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,AMB,AMW,i2hdm,gat,gab,CTT0)
        YY(1)=VTB**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,AMB,AMW,i2hdm,gat,gab,CTT0)
        YY(2)=VTB**2*FACTB*CTT0
        XMB = RUNM_HDEC(XX(3),5)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(3),gab,gat,(XMB/XX(3))**2,(XMT/XX(3))**2,RATY)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(3),gab,gat,(AMB/XX(3))**2,(AMT/XX(3))**2,RATX)
        RAT = (AMB+AMT)/XX(3)
        YY(3) = VTB**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMB = RUNM_HDEC(XX(4),5)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*
     .  CQCD2HDM(XX(4),gab,gat,(XMB/XX(4))**2,(XMT/XX(4))**2,RATY)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*
     .  CQCDM2HDM(XX(4),gab,gat,(AMB/XX(4))**2,(AMT/XX(4))**2,RATX)
        RAT = (AMB+AMT)/XX(4)
        YY(4) = VTB**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        hcBT = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HBT2=3.D0*VTB**2*
     .  CQCD2HDM(AMCH,gab,gat,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
        IF(HBT2.LT.0.D0) HBT2 = 0
        HBT1=3.D0*VTB**2*
     .  CQCDM2HDM(AMCH,gab,gat,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
        RAT = (AMB+AMT)/AMCH
        hcBT = QQINT_HDEC(RAT,HBT1,HBT2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT+AMB) THEN
        HBT=0.D0
       ELSE
        HBT2=3.D0*VTB**2*
     .  CQCD2HDM(AMCH,gab,gat,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
        IF(HBT2.LT.0.D0) HBT2 = 0
        HBT1=3.D0*VTB**2*
     .  CQCDM2HDM(AMCH,gab,gat,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
        RAT = (AMB+AMT)/AMCH
        hcBT = QQINT_HDEC(RAT,HBT1,HBT2)
       ENDIF
      ENDIF

c      print*,'H+ -> tb',hcbt

c  H+ ---> W+ Hi

      do i=1,3,1

      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = AMW+amhiggs(i)-DLD
       XM2 = AMW+amhiggs(i)+DLU
       IF (AMCH.LE.amhiggs(i)) THEN
        hcWH(i)=0
       ELSEIF (AMCH.LE.XM1) THEN
        IF(AMCH.LE.DABS(AMW-amhiggs(i)))THEN
         hcWH(i)=0
        ELSE
         hcWH(i)=9.D0*GF**2/16.D0/PI**3*AMW**4*AMCH*
     .           ghihpwm(i)**2
     .           *HVH((amhiggs(i)/AMCH)**2,(AMW/AMCH)**2)
        ENDIF
       ELSEIF (AMCH.LT.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(1)
     .         *HVH((amhiggs(i)/XX(1))**2,(AMW/XX(1))**2)
        YY(2) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(2)
     .         *HVH((amhiggs(i)/XX(2))**2,(AMW/XX(2))**2)
        CWH=LAMB_HDEC(amhiggs(i)**2/XX(3)**2,AMW**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMW**2,amhiggs(i)**2/AMW**2)**2
        YY(3)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(3)*CWH
        CWH=LAMB_HDEC(amhiggs(i)**2/XX(4)**2,AMW**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMW**2,amhiggs(i)**2/AMW**2)**2
        YY(4)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(4)*CWH
        hcWH(i)= FINT_HDEC(AMCH,XX,YY)*
     .           ghihpwm(i)**2
       ELSE
        CWH=LAMB_HDEC(amhiggs(i)**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,amhiggs(i)**2/AMW**2)**2
        hcWH(i)= GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*CWH*
     .           ghihpwm(i)**2
c        print*,'hallo'
       ENDIF
      ELSE
       IF (AMCH.LT.AMW+amhiggs(i)) THEN
        hcWH(i)=0
       ELSE
        CWH=LAMB_HDEC(amhiggs(i)**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,amhiggs(i)**2/AMW**2)**2
        hcWH(i)= GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*CWH*
     .           ghihpwm(i)**2
       ENDIF
      ENDIF

c      print*,'H+ -> W+ Hi',hcwh(i),ghihpwm(i),amhiggs(i),
c     .     amw,amch,gf,pi

      end do

C  H+ ---> W A
      if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+ama-dld
            xm2 = amw+ama+dlu
            if (amch.lt.ama) then
               hcwa=0
            elseif (amch.le.xm1) then
               if(amch.le.dabs(amw-ama))then
                  hcwa=0
               else
                  hcwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch
     .                 *hvh((ama/amch)**2,(amw/amch)**2)
               endif
            elseif (amch.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((ama/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((ama/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(ama**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,ama**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(ama**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,ama**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hcwa = fint_hdec(amch,xx,yy)
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hcwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
      else
            if (amch.lt.amw+ama) then
               hcwa=0
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hcwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
      endif


c -------------------- total width and branching ratios -------------- c
      hcwdth=hcLN+hcMN+hcSU+hcBU+hcSC+hcBC+hcBT+hcCD+hcST+hcwa
     .     +hcDT+hcWH(1)+hcWH(2)+hcWH(3)

      if(hcwdth.gt.0) then
        hcbrl=hcln/hcwdth
        hcbrm=hcmn/hcwdth
        hcbrs=hcsu/hcwdth
        hcbrbu=hcbu/hcwdth
        hcbrc=hcsc/hcwdth
        hcbrb=hcbc/hcwdth
        hcbrt=hcbt/hcwdth
        hcbrcd=hccd/hcwdth
        hcbrts=hcst/hcwdth
        hcbrtd=hcdt/hcwdth
        hcbra=hcwa/hcwdth
        hcbrwh(1)=hcwh(1)/hcwdth
        hcbrwh(2)=hcwh(2)/hcwdth
        hcbrwh(3)=hcwh(3)/hcwdth
      else
        hcbrl=hcln
        hcbrm=hcmn
        hcbrs=hcsu
        hcbrbu=hcbu
        hcbrc=hcsc
        hcbrb=hcbc
        hcbrt=hcbt
        hcbrcd=hccd
        hcbrts=hcst
        hcbrtd=hcdt
        hcbra=hcwa
        hcbrwh(1)=hcwh(1)
        hcbrwh(2)=hcwh(2)
        hcbrwh(3)=hcwh(3)
      endif


C        =========================================================
C                       CP ODD  HIGGS DECAYS
C        =========================================================
C     =============  RUNNING MASSES
      RMS = RUNM_HDEC(AMA,3)
      RMC = RUNM_HDEC(AMA,4)
      RMB = RUNM_HDEC(AMA,5)
      RMT = RUNM_HDEC(AMA,6)
      if(abs(GAB).lt.1e-10) then
        RATCOUP = 1
      else
        RATCOUP = GAT/GAB
      endif
      HIGTOP = AMA**2/AMT**2

      ASH=ALPHAS_HDEC(AMA,3)
      AMC0=1.D8
      AMB0=2.D8
C     AMT0=3.D8
      AS3=ALPHAS_HDEC(AMA,3)
      AMC0=AMC
      AS4=ALPHAS_HDEC(AMA,3)
      AMB0=AMB
C     AMT0=AMT

C     =============== PARTIAL WIDTHS
C  A ---> G G
       EPS=1.D-8
       NFEXT = 5
       ASG = ASH
       CTT = 4*AMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMA**2*DCMPLX(1D0,-EPS)

       CAT = CTT*CF(CTT)*GAT
       CAB = CTB*CF(CTB)*GAB
       CAC = CTC*CF(CTC)*GAT

       FQCD=AGGQCD2(ASG,5,AMA,AMT)
       XFAC = CDABS(CAT+CAB+CAC)**2*FQCD
       HGG=GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC


c     print*,'hgg_NNLO',hgg

C  A ---> MU MU
      IF(AMA.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
      HMM=AFF(AMA,(AMMUON/AMA)**2)*galep**2
      ENDIF

c     print*,'A -> mumu',hmm

C  A ---> LL
      IF(AMA.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
      HLL=AFF(AMA,(AMTAU/AMA)**2)*galep**2
      ENDIF

c     print*,'A -> tautau',hll

C  A --> SS
      IF(AMA.LE.2*AMS) THEN
       HSS = 0
      ELSE
       HS1=3.D0*AFF(AMA,(AMS/AMA)**2)
     .    *GAB**2
     .    *TQCDA(AMS**2/AMA**2)
       HS2=3.D0*AFF(AMA,(RMS/AMA)**2)
     .    *GAB**2
     .    *QCDA(RMS**2/AMA**2)
       IF(HS2.LT.0.D0) HS2 = 0
       RAT = 2*AMS/AMA
       HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF

c     print*,'A -> ss',hss
C  A --> CC
      RATCOUP = 1
      IF(AMA.LE.2*AMC) THEN
       HCC = 0
      ELSE
       HC1=3.D0*AFF(AMA,(AMC/AMA)**2)
     .    *GAT**2
     .    *TQCDA(AMC**2/AMA**2)
       HC2=3.D0*AFF(AMA,(RMC/AMA)**2)
     .    *GAT**2
     .    *QCDA(RMC**2/AMA**2)
       IF(HC2.LT.0.D0) HC2 = 0
       RAT = 2*AMC/AMA
       HCC = QQINT_HDEC(RAT,HC1,HC2)
      ENDIF

c     print*,'A -> cc',hcc

C  A --> BB :
      if(abs(GAB).lt.1e-10) then
        RATCOUP = 1
      else
        RATCOUP = GAT/GAB
      endif
      IF(AMA.LE.2*AMB) THEN
       HBB = 0
      ELSE
       HB1=3.D0*AFF(AMA,(AMB/AMA)**2)
     .    *GAB**2
     .    *TQCDA(AMB**2/AMA**2)
       HB2=3.D0*AFF(AMA,(RMB/AMA)**2)
     .    *GAB**2
     .    *QCDA(RMB**2/AMA**2)
       IF(HB2.LT.0.D0) HB2 = 0
       RAT = 2*AMB/AMA
       HBB = QQINT_HDEC(RAT,HB1,HB2)
      ENDIF

c     print*,'A -> bb',hbb

C  A --> TT :
      RATCOUP = 0
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=4D0
       XM1 = 2D0*AMT-DLD
       XM2 = 2D0*AMT+DLU
       IF (AMA.LE.AMT+AMW+AMB.or.abs(gat).le.1D-100) THEN
        HTT=0.D0
       ELSEIF (AMA.LE.XM1) THEN
        FACTT=6.D0*GF**2*AMA**3*AMT**2/2.D0/128.D0/PI**3*GAT**2
        call ATOTT_HDEC(ama,amt,amb,amw,amch,att0,gab,gat)
        HTT=FACTT*ATT0
       ELSEIF (AMA.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTT=6.D0*GF**2*XX(1)**3*AMT**2/2.D0/128.D0/PI**3
        call ATOTT_HDEC(xx(1),amt,amb,amw,amch,att0,gab,gat)
        YY(1)=FACTT*ATT0
        FACTT=6.D0*GF**2*XX(2)**3*AMT**2/2.D0/128.D0/PI**3
        call ATOTT_HDEC(xx(2),amt,amb,amw,amch,att0,gab,gat)
        YY(2)=FACTT*ATT0
        XMT = RUNM_HDEC(XX(3),6)
        XYZ1 =3.D0*AFF(XX(3),(AMT/XX(3))**2)
     .    *TQCDA(AMT**2/XX(3)**2)
        XYZ2 =3.D0*AFF(XX(3),(XMT/XX(3))**2)
     .    *QCDA(XMT**2/XX(3)**2)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        RAT = 2*AMT/XX(3)
        YY(3) = QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ1 =3.D0*AFF(XX(4),(AMT/XX(4))**2)
     .    *TQCDA(AMT**2/XX(4)**2)
        XYZ2 =3.D0*AFF(XX(4),(XMT/XX(4))**2)
     .    *QCDA(XMT**2/XX(4)**2)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        RAT = 2*AMT/XX(4)
        YY(4) = QQINT_HDEC(RAT,XYZ1,XYZ2)
        HTT = FINT_HDEC(AMA,XX,YY)*GAT**2
       ELSE
        HT1=3.D0*AFF(AMA,(AMT/AMA)**2)*GAT**2
     .    *TQCDA(AMT**2/AMA**2)
        HT2=3.D0*AFF(AMA,(RMT/AMA)**2)*GAT**2
     .    *QCDA(RMT**2/AMA**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMA
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ELSE
       IF (AMA.LE.2.D0*AMT) THEN
        HTT=0.D0
       ELSE
        HT1=3.D0*AFF(AMA,(AMT/AMA)**2)*GAT**2
     .    *TQCDA(AMT**2/AMA**2)
        HT2=3.D0*AFF(AMA,(RMT/AMA)**2)*GAT**2
     .    *QCDA(RMT**2/AMA**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMA
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ENDIF

c     print*,'A -> tt',htt

C  A ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(AMA/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(AMA/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(AMA/2,6)*AMT/RUNM_HDEC(AMT,6)
       CTT = 4*XRMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CAT = 4/3D0 * CTT*CF(CTT)*GAT
     .     * CFACQ_HDEC(1,AMA,XRMT)
       CAB = 1/3D0 * CTB*CF(CTB)*GAB
     .     * CFACQ_HDEC(1,AMA,XRMB)
       CTC = 4*XRMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CAC = 4/3D0 * CTC*CF(CTC)*GAT
     .     * CFACQ_HDEC(1,AMA,XRMC)
       CTL = 4*AMTAU**2/AMA**2*DCMPLX(1D0,-EPS)
       CAL = 1.D0  * CTL*CF(CTL)*galep
       XFAC = CDABS(CAT+CAB+CAC+CAL)**2
       HGA=GF/(32.D0*PI*DSQRT(2.D0))*AMA**3*(ALPH/PI)**2*XFAC

c      print*,'A -> gamgam',hga
C  A ---> Z GAMMA
      XRMC = RUNM_HDEC(AMA/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(AMA/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(AMA/2,6)*AMT/RUNM_HDEC(AMT,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      IF(AMA.LE.AMZ)THEN
       HZGA=0
      ELSE
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GAT
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS)*GAB
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GAT
       FL = (-1+4*SS)/DSQRT(SS*CS)*galep
       EPS=1.D-8
       CTT = 4*AMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMA**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(- CI2(CTT,CLT))
       CAB = FB*(- CI2(CTB,CLB))
       CAC = FC*(- CI2(CTC,CLC))
       CAL = FL*(- CI2(CTL,CLE))
       XFAC = CDABS(CAT+CAB+CAC+CAL)**2
       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*AMA**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/AMA**2)**3
      ENDIF

c     print*,'A -> Zgam',hzga

C  A ---> hi Z
      do i=1,3,1

      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = amhiggs(i)+AMZ-DLD
       XM2 = amhiggs(i)+AMZ+DLU
       IF (AMA.LE.amhiggs(i)) THEN
        hahz(i)=0
       ELSEIF (AMA.LE.XM1) THEN
        IF (AMA.LE.DABS(AMZ-amhiggs(i))) THEN
         hahz(i)=0
        ELSE
c maggie changed 10/10/16
         hahz(i)=9.D0*GF**2/8.D0/PI**3*AMZ**4*AMA*ghiza(i)**2*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((amhiggs(i)/AMA)**2,(AMZ/AMA)**2)
c end maggie changed 10/10/16
        ENDIF
       ELSEIF (AMA.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
c maggie changed 10/10/16
        YY(1)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(1)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((amhiggs(i)/XX(1))**2,(AMZ/XX(1))**2)
        YY(2)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(2)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((amhiggs(i)/XX(2))**2,(AMZ/XX(2))**2)
c end maggie changed 10/10/16
        CAZ=LAMB_HDEC(amhiggs(i)**2/XX(3)**2,AMZ**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMZ**2,amhiggs(i)**2/AMZ**2)**2
        YY(3)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(3)*CAZ
        CAZ=LAMB_HDEC(amhiggs(i)**2/XX(4)**2,AMZ**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMZ**2,amhiggs(i)**2/AMZ**2)**2
        YY(4)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(4)*CAZ
        hahz(i) = FINT_HDEC(AMA,XX,YY)*ghiza(i)**2
       ELSE
        CAZ=LAMB_HDEC(amhiggs(i)**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,amhiggs(i)**2/AMZ**2)**2
        hahz(i)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*ghiza(i)**2*CAZ
       ENDIF
      ELSE
       IF (AMA.LE.AMZ+amhiggs(i)) THEN
        hahz(i)=0
       ELSE
        CAZ=LAMB_HDEC(amhiggs(i)**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,amhiggs(i)**2/AMZ**2)**2
        hahz(i)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*ghiza(i)**2*CAZ
       ENDIF
      ENDIF
      enddo

c     print*,'A -> Z hi',hahz(1),hahz(2),hahz(3)

C  A ---> W+ H-

      if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+amch-dld
            xm2 = amw+amch+dlu
            if (ama.lt.amch) then
               hawphm=0
            elseif (ama.le.xm1) then
               if(ama.le.dabs(amw-amch))then
                  hawphm=0
               else
                  hawphm=9.d0*gf**2/16.d0/pi**3*amw**4*ama
     .                 *hvh((amch/ama)**2,(amw/ama)**2)
               endif
            elseif (ama.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hawphm = fint_hdec(ama,xx,yy)
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
      else
            if (ama.lt.amw+amch) then
               hawphm=0
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
      endif


      hawphm = 2.D0*hawphm

c     print*,'A -> W+H- + W-H+',hawphm,hawphm/2.D0

C    ==========  TOTAL WIDTH AND BRANCHING RATIOS
      WTOT=HLL+HMM+HSS+HCC+HBB+HGG+HGA+HZGA+HTT
     .    +hahz(1)+hahz(2)+hahz(3) + hawphm

      if(WTOT.gt.0) then
        ABRT=HTT/WTOT
        ABRB=HBB/WTOT
        ABRL=HLL/WTOT
        ABRM=HMM/WTOT
        ABRS=HSS/WTOT
        ABRC=HCC/WTOT
        ABRG=HGG/WTOT
        ABRGA=HGA/WTOT
        ABRZGA=HZGA/WTOT
        abrhawphm = hawphm/wtot
        do i=1,3,1
              abrhz(i) = hahz(i)/wtot
        enddo
        AWDTH=WTOT
      else
        ABRT=HTT
        ABRB=HBB
        ABRL=HLL
        ABRM=HMM
        ABRS=HSS
        ABRC=HCC
        ABRG=HGG
        ABRGA=HGA
        ABRZGA=HZGA
        abrhawphm = hawphm
        do i=1,3,1
              abrhz(i) = hahz(i)
        enddo
        AWDTH=WTOT
      endif

      RETURN
      END
