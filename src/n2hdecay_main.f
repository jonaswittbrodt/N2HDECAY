C   N2HDECAY
C   Copyright (C) 2018  Isabell Engeln, Margarete Mühlleitner, Jonas Wittbrodt
C   Copyright (C) 1997-2018  Michael Spira for parts of N2HDECAY as part
C                            of HDECAY-6.52
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of version 2 of the the GNU General Public License as
C   published by the Free Software Foundation.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.

      PROGRAM N2HDECAY
      implicit none
      integer nargs,i
      CHARACTER(len=64) arg,infile,outpath


      infile = 'n2hdecay.in'
      outpath = ''
      nargs = IARGC()
      if(MOD(nargs,2).eq.0)then
        do 10 i=1,nargs/2
          call GETARG(2*i-1,arg)
          if(arg.eq.'-i')then
            call GETARG(2*i,arg)
            infile = arg
          elseif(arg.eq.'-o') then
            call GETARG(2*i,arg)
            outpath = arg
          endif
  10    continue
      endif

      CALL READ_HDEC(infile)
      CALL HEAD_HDEC(outpath,LENGTH(outpath))
      CALL HDEC()
      CALL WRITE_HDEC()
      CALL CLOSE_HDEC()

      STOP
      CONTAINS

      INTEGER FUNCTION LENGTH(STRING)
      CHARACTER*(*) STRING
      DO 15, I = LEN(STRING), 1, -1
        IF(STRING(I:I) .NE. ' ') GO TO 20
  15  CONTINUE
  20  LENGTH = I
      END

      END PROGRAM
