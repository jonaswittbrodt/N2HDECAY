      SUBROUTINE CLOSE_HDEC
      IMPLICIT none
      integer NAa,NAb,NAc,NCa,NCb,NCc,NTa,NH1a,NH1b,NH1c,NH2a,NH2b,NH2c,
     .  NH3a,NH3b,NH3c,NH3d
      PARAMETER(NAa=80,NAb=81,NAc=82,NCa=83,NCb=84,NCc=85,NTa=86)
      PARAMETER(NH1a=90,NH1b=91,NH1c=92,NH2a=93,NH2b=94,NH2c=95,
     .  NH3a=96,NH3b=97,NH3c=98,NH3d=99)

      close(NAa)
      close(NAb)
      close(NAc)

      close(NCa)
      close(NCb)
      close(NCc)

      close(NTa)

      close(NH1a)
      close(NH1b)
      close(NH1c)

      close(NH2a)
      close(NH2b)
      close(NH2c)

      close(NH3a)
      close(NH3b)
      close(NH3c)
      close(NH3d)

      RETURN
      END
