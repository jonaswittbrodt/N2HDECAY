      SUBROUTINE WRITE_HDEC()
      IMPLICIT none
      integer NAa,NAb,NAc,NCa,NCb,NCc,NTa,NH1a,NH1b,NH1c,NH2a,NH2b,NH2c,
     .  NH3a,NH3b,NH3c,NH3d
      PARAMETER(NAa=80,NAb=81,NAc=82,NCa=83,NCb=84,NCc=85,NTa=86)
      PARAMETER(NH1a=90,NH1b=91,NH1c=92,NH2a=93,NH2b=94,NH2c=95,
     .  NH3a=96,NH3b=97,NH3c=98,NH3d=99)
      double precision ABRB,ABRL,ABRM,ABRS,ABRC,ABRT,ABRG,ABRGA,
     .              ABRZGA,ABRZ,AWDTH
      double precision HCBRB,HCBRL,HCBRM,HCBRBU,HCBRS,HCBRC,HCBRT,
     .               HCBRW,HCBRA,HCWDTH
      double precision GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      double precision TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM
      integer ITYPE2HDM,I2HDM,IPARAM2HDM
      double precision hcbrcd,hcbrts,hcbrtd
      double precision hcbrwhh,hhbrchch,hlbrchch,abrhhaz,abrhawphm
      double precision ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM
      double precision amhiggs(3)
      integer IN2HDM,IPHASE
      double precision h2brh1h1,h3brh1h1,h3brh2h2,h3brh1h2,
     .      hibrt(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),hibrc(3),
     .      hibrg(3),hibrga(3),hibrzga(3),hibrw(3),hibrz(3),
     .      hibrhpwm(3),hibraz(3),hibrchch(3),hibraa(3),
     .      gamtot(3)
      double precision hcbrwh(3),abrhz(3)
      COMMON/WIDTHA_HDEC/ABRB,ABRL,ABRM,ABRS,ABRC,ABRT,ABRG,ABRGA,
     .              ABRZGA,ABRZ,AWDTH
      COMMON/WIDTHHC_HDEC/HCBRB,HCBRL,HCBRM,HCBRBU,HCBRS,HCBRC,HCBRT,
     .               HCBRW,HCBRA,HCWDTH
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
      COMMON/WIDTH_HC_ADD/hcbrcd,hcbrts,hcbrtd
      COMMON/WIDTH_2HDM/hcbrwhh,hhbrchch,hlbrchch,abrhhaz,abrhawphm
      Common/N2HDM_HDEC_PARAM/amhiggs,ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM,
     .      IN2HDM,IPHASE
      Common/N2HDM_BR_H/h2brh1h1,h3brh1h1,h3brh2h2,h3brh1h2,
     .      hibrt,hibrb,hibrl,hibrm,hibrs,hibrc,
     .      hibrg,hibrga,hibrzga,hibrw,hibrz,
     .      hibrhpwm,hibraz,hibrchch,hibraa,
     .      gamtot
      Common/N2HDM_BR_Hc_A/hcbrwh,abrhz

c broken phase **************************
      if(IPHASE.eq.1) then
        write(NH1a,806)amhiggs(1),hibrb(1),hibrl(1),hibrm(1),hibrs(1),
     .        hibrc(1),hibrt(1)
        write(NH1b,805)amhiggs(1),hibrg(1),hibrga(1),hibrzga(1),
     .        hibrw(1),hibrz(1)
        write(NH1c,805)amhiggs(1),hibraa(1),hibraz(1),hibrhpwm(1),
     .        hibrchch(1),gamtot(1)

        write(NH2a,806)amhiggs(2),hibrb(2),hibrl(2),hibrm(2),hibrs(2),
     .        hibrc(2),hibrt(2)
        write(NH2b,805)amhiggs(2),hibrg(2),hibrga(2),hibrzga(2),
     .        hibrw(2),hibrz(2)
        write(NH2c,806)amhiggs(2),h2brh1h1,hibraa(2),hibraz(2),
     .        hibrhpwm(2),hibrchch(2),gamtot(2)

        write(NH3a,806)amhiggs(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),
     .        hibrc(3),hibrt(3)
        write(NH3b,805)amhiggs(3),hibrg(3),hibrga(3),hibrzga(3),
     .        hibrw(3),hibrz(3)
        write(NH3c,806)amhiggs(3),h3brh1h1,h3brh1h2,h3brh2h2,
     .        hibraa(3),hibraz(3),hibrhpwm(3)
        write(NH3d,802)amhiggs(3),hibrchch(3),gamtot(3)

        write(NAa,806)AMHA2HDM,abrb,abrl,abrm,abrs,abrc,abrt
        write(NAb,803)AMHA2HDM,abrg,abrga,abrzga
        write(NAc,805)AMHA2HDM,abrhz(1),abrhz(2),
     .        abrhz(3),abrhawphm,awdth

        write(NCa,806)AMHC2HDM,hcbrb,hcbrl,hcbrm,hcbrs,hcbrc,hcbrt
        write(NCb,804)AMHC2HDM,hcbrcd,hcbrbu,hcbrts,hcbrtd
        write(NCc,805)AMHC2HDM,hcbrwh(1),hcbrwh(2),hcbrwh(3),hcbra,
     .    hcwdth
c dark phase ****************************
      elseif(iphase.eq.2)then
        write(NH2a,806)amhiggs(2),hibrb(2),hibrl(2),hibrm(2),hibrs(2),
     .        hibrc(2),hibrt(2)
        write(NH2b,805)amhiggs(2),hibrg(2),hibrga(2),hibrzga(2),
     .        hibrw(2),hibrz(2)
        write(NH2c,806)amhiggs(2),h2brh1h1,hibraa(2),hibraz(2),
     .        hibrhpwm(2),hibrchch(2),gamtot(2)
        write(NH3a,806)amhiggs(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),
     .        hibrc(3),hibrt(3)
        write(NH3b,805)amhiggs(3),hibrg(3),hibrga(3),hibrzga(3),
     .        hibrw(3),hibrz(3)
        write(NH3c,806)amhiggs(3),h3brh1h1,h3brh2h2,
     .        hibraa(3),hibraz(3),hibrhpwm(3),hibrchch(3)
        write(NH3d,801)amhiggs(3),gamtot(3)

        write(NAa,806)AMHA2HDM,abrb,abrl,abrm,abrs,abrc,abrt
        write(NAb,803)AMHA2HDM,abrg,abrga,abrzga
        write(NAc,804)AMHA2HDM,abrhz(2),abrhz(3),abrhawphm,awdth

        write(NCa,806)AMHC2HDM,hcbrb,hcbrl,hcbrm,hcbrs,hcbrc,hcbrt
        write(NCb,804)AMHC2HDM,hcbrcd,hcbrbu,hcbrts,hcbrtd
        write(NCc,804)AMHC2HDM,hcbrwh(2),hcbrwh(3),hcbra,hcwdth

      elseif(iphase.eq.3)then
        write(NH1a,803)amhiggs(1),hibraz(1),hibrhpwm(1),gamtot(1)

        write(NH2a,806)amhiggs(2),hibrb(2),hibrl(2),hibrm(2),hibrs(2),
     .        hibrc(2),hibrt(2)
        write(NH2b,805)amhiggs(2),hibrg(2),hibrga(2),hibrzga(2),
     .        hibrw(2),hibrz(2)
        write(NH2c,806)amhiggs(2),h2brh1h1,hibraa(2),hibraz(2),
     .        hibrhpwm(2),hibrchch(2),gamtot(2)

        write(NH3a,806)amhiggs(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),
     .        hibrc(3),hibrt(3)
        write(NH3b,805)amhiggs(3),hibrg(3),hibrga(3),hibrzga(3),
     .        hibrw(3),hibrz(3)
        write(NH3c,806)amhiggs(3),h3brh1h1,h3brh2h2,
     .        hibraa(3),hibraz(3),hibrhpwm(3),hibrchch(3)
        write(NH3d,801)amhiggs(3),gamtot(3)

        write(NAa,803)AMHA2HDM,abrhz(1),abrhawphm,awdth

        write(NCa,803)AMHC2HDM,hcbrwh(1),hcbra,hcwdth
      elseif(iphase.eq.4)then
        write(NH1a,803)amhiggs(1),hibraz(1),hibrhpwm(1),gamtot(1)

        write(NH3a,806)amhiggs(3),hibrb(3),hibrl(3),hibrm(3),hibrs(3),
     .        hibrc(3),hibrt(3)
        write(NH3b,805)amhiggs(3),hibrg(3),hibrga(3),hibrzga(3),
     .        hibrw(3),hibrz(3)
        write(NH3c,806)amhiggs(3),h3brh1h1,hibraa(3),hibrchch(3),
     .        h3brh2h2,gamtot(3)

        write(NAa,803)AMHA2HDM,abrhz(1),abrhawphm,awdth
        write(NCa,803)AMHC2HDM,hcbrwh(1),hcbra,hcwdth
      endif

      if(iphase.eq.1.or.iphase.eq.2)then
        write(NTa,803)AMHC2HDM,gamt0/gamt1,(gamt1-gamt0)/gamt1,gamt1
      endif

801    FORMAT(G12.6,1X,G10.4)
802    FORMAT(G12.6,2(1X,G10.4))
803    FORMAT(G12.6,3(1X,G10.4))
804    FORMAT(G12.6,4(1X,G10.4))
805    FORMAT(G12.6,5(1X,G10.4))
806    FORMAT(G12.6,6(1X,G10.4))

      RETURN
      END
