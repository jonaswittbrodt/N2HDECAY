      SUBROUTINE HEAD_HDEC(outpath, loutpath)
      IMPLICIT none
      character(len=64)outpath
      integer NAa,NAb,NAc,NCa,NCb,NCc,NTa,NH1a,NH1b,NH1c,NH2a,NH2b,NH2c,
     .  NH3a,NH3b,NH3c,NH3d,loutpath
      PARAMETER(NAa=80,NAb=81,NAc=82,NCa=83,NCb=84,NCc=85,NTa=86)
      PARAMETER(NH1a=90,NH1b=91,NH1c=92,NH2a=93,NH2b=94,NH2c=95,
     .  NH3a=96,NH3b=97,NH3c=98,NH3d=99)
      double precision ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM
      double precision amhiggs(3)
      integer IN2HDM,IPHASE
      Common/N2HDM_HDEC_PARAM/amhiggs,ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM,
     .      IN2HDM,IPHASE

c header top
      if(iphase.eq.1.or.iphase.eq.2)then
        OPEN(NTA,FILE=outpath(1:loutpath) // 'br.top')
        WRITE(NTA,903)'MH+   ','W b  ','H+ b ','WIDTH '
        WRITE(NTA,69)
        WRITE(NTA,*)
      endif


c broken phase ************************************
      if(iphase.eq.1) then
c header H1
        Open(NH1a,File =outpath(1:loutpath) //  'br.H1_N2HDM_a')
        Open(NH1b,File =outpath(1:loutpath) //  'br.H1_N2HDM_b')
        Open(NH1c,File =outpath(1:loutpath) //  'br.H1_N2HDM_c')
        WRITE(NH1a,906)'MH1   ','B B ','TAU TAU','MU MU ','S S ','C C ',
     .    'T T '
        WRITE(NH1a,69)
        WRITE(NH1a,*)
        Write(NH1b,905)'MH1   ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH1b,69)
        WRITE(NH1b,*)
        WRITE(NH1C,905)'MH1   ','A A','Z A ','W+- H-+','H+ H- ','WIDTH'
        WRITE(NH1C,69)
        WRITE(NH1C,*)
c header H2
        Open(NH2a,File =outpath(1:loutpath) //  'br.H2_N2HDM_a')
        Open(NH2b,File =outpath(1:loutpath) //  'br.H2_N2HDM_b')
        Open(NH2c,File =outpath(1:loutpath) //  'br.H2_N2HDM_c')
        WRITE(NH2a,906)'MH2   ','B B  ','TAU TAU','MU MU ','S S ',
     .    'C C ','TT '
        WRITE(NH2a,69)
        WRITE(NH2a,*)
        WRITE(NH2b,905)'MH2   ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH2b,69)
        WRITE(NH2b,*)
        WRITE(NH2c,906)'MH2   ','H1 H1','A A ','Z A ','W+- H-+',
     .    'H+ H- ','WIDTH '
        WRITE(NH2c,69)
        WRITE(NH2c,*)
c header H3
        Open(NH3a,File =outpath(1:loutpath) //  'br.H3_N2HDM_a')
        Open(NH3b,File =outpath(1:loutpath) //  'br.H3_N2HDM_b')
        Open(NH3c,File =outpath(1:loutpath) //  'br.H3_N2HDM_c')
        Open(NH3d,File =outpath(1:loutpath) //  'br.H3_N2HDM_d')
        WRITE(NH3a,906)'MH3   ','B B   ','TAU TAU','MU MU ','S S ',
     .    'C C ','T T '
        WRITE(NH3a,69)
        WRITE(NH3a,*)
        WRITE(NH3b,905)'MH3   ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH3b,69)
        WRITE(NH3b,*)
        WRITE(NH3c,906)'MH3   ','H1 H1','H1 H2','H2 H2','A A ','Z A ',
     .    'W+- H-+'
        WRITE(NH3c,69)
        WRITE(NH3c,*)
        WRITE(NH3d,902)'MH3   ','H+ H- ','WIDTH '
        WRITE(NH3d,69)
        WRITE(NH3d,*)
c header charged Higgs H+-
        Open(NCa,File =outpath(1:loutpath) //  'br.H+_N2HDM_a')
        Open(NCb,File =outpath(1:loutpath) //  'br.H+_N2HDM_b')
        Open(NCc,File =outpath(1:loutpath) //  'br.H+_N2HDM_c')
        WRITE(NCA,906)'MH+   ','B C  ','TAU NU ','MU NU ','S U ','C S ',
     .    'T B '
        WRITE(NCA,69)
        WRITE(NCA,*)
        WRITE(NCB,904)'MH+   ','C D  ','B U  ','T S  ','T D  '
        WRITE(NCB,69)
        WRITE(NCB,*)
        WRITE(NCC,905)'MH+   ','H1 W ','H2 W ','H3 W ','A W','WIDTH '
        WRITE(NCC,69)
        WRITE(NCC,*)
c header pseudoscalar A
        Open(NAa,File =outpath(1:loutpath) //  'br.A_N2HDM_a')
        Open(NAb,File =outpath(1:loutpath) //  'br.A_N2HDM_b')
        Open(NAc,File =outpath(1:loutpath) //  'br.A_N2HDM_c')

        WRITE(NAa,906)'MA   ','BB   ','TAU TAU','MU MU ','SS ','CC ',
     .    'T T '
        WRITE(NAa,69)
        WRITE(NAa,*)
        WRITE(NAb,903)'MA   ','GG ','GAM GAM','Z GAM '
        WRITE(NAb,69)
        WRITE(NAb,*)
        WRITE(NAc,905)'MA   ','Z H1 ','Z H2','Z H3','W+- H-+','WIDTH '
        WRITE(NAc,69)
        WRITE(NAc,*)
c dark phase *********************************
      elseif(IPHASE.eq.2)then
c header h
        Open(NH2a,File =outpath(1:loutpath) //  'br.H1_N2HDM_a')
        Open(NH2b,File =outpath(1:loutpath) //  'br.H1_N2HDM_b')
        Open(NH2c,File =outpath(1:loutpath) //  'br.H1_N2HDM_c')
        WRITE(NH2a,906)'MH1   ','B B  ','TAU TAU','MU MU ','S S','C C',
     .    'T T'
        WRITE(NH2a,69)
        WRITE(NH2a,*)
        WRITE(NH2b,905)'MH1   ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH2b,69)
        WRITE(NH2b,*)
        WRITE(NH2c,906)'MH1   ','HD HD','A A ','Z A ','W+- H-+',
     .    'H+ H- ','WIDTH'
        WRITE(NH2c,69)
        WRITE(NH2c,*)
c header H
        Open(NH3a,File =outpath(1:loutpath) //  'br.H2_N2HDM_a')
        Open(NH3b,File =outpath(1:loutpath) //  'br.H2_N2HDM_b')
        Open(NH3c,File =outpath(1:loutpath) //  'br.H2_N2HDM_c')
        Open(NH3d,File =outpath(1:loutpath) //  'br.H2_N2HDM_d')
        WRITE(NH3a,906)'MH2   ','B B ','TAU TAU','MU MU ','S S ','C C ',
     .    'T T '
        WRITE(NH3a,69)
        WRITE(NH3a,*)
        WRITE(NH3b,905)'MH2   ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH3b,69)
        WRITE(NH3b,*)
        WRITE(NH3c,906)'MH2   ','HD HD','H1 H1','AA ','Z A ','W+- H-+',
     .    'H+ H-'
        WRITE(NH3c,69)
        WRITE(NH3c,*)
        WRITE(NH3d,901)'MH2   ','WIDTH '
        WRITE(NH3d,69)
        WRITE(NH3d,*)
c header charged Higgs H+-
        Open(NCa,File =outpath(1:loutpath) //  'br.H+_N2HDM_a')
        Open(NCb,File =outpath(1:loutpath) //  'br.H+_N2HDM_b')
        Open(NCc,File =outpath(1:loutpath) //  'br.H+_N2HDM_c')
        WRITE(NCA,906)'MH+   ','B C  ','TAU NU ','MU NU ','S U ','C S ',
     .     'T B '
        WRITE(NCA,69)
        WRITE(NCA,*)
        WRITE(NCB,904)'MH+   ','C D  ','B U  ','T S  ','T D  '
        WRITE(NCB,69)
        WRITE(NCB,*)
        WRITE(NCC,904)'MH+   ','H1 W ','H2 W ','A W','WIDTH '
        WRITE(NCC,69)
        WRITE(NCC,*)
c header pseudoscalar A
        Open(NAa,File =outpath(1:loutpath) //  'br.A_N2HDM_a')
        Open(NAb,File =outpath(1:loutpath) //  'br.A_N2HDM_b')
        Open(NAc,File =outpath(1:loutpath) //  'br.A_N2HDM_c')

        WRITE(NAa,906)'MA   ','B B   ','TAU TAU','MU MU ','S S ','CC ',
     .    'T T '
        WRITE(NAa,69)
        WRITE(NAa,*)
        WRITE(NAb,903)'MA   ','G G ','GAM GAM','Z GAM '
        WRITE(NAb,69)
        WRITE(NAb,*)
        WRITE(NAc,904)'MA   ','Z H1 ','Z H2','W+- H-+','WIDTH '
        WRITE(NAc,69)
        WRITE(NAc,*)

      elseif(IPHASE.eq.3)then
c header HD
        Open(NH1a,File =outpath(1:loutpath) //  'br.HD_N2HDM')
        WRITE(NH1a,903)'MHD   ','Z AD ','W+- HD+-','WIDTH '
        WRITE(NH1a,69)
        WRITE(NH1a,*)
c header H1
        Open(NH2a,File =outpath(1:loutpath) //  'br.H1_N2HDM_a')
        Open(NH2b,File =outpath(1:loutpath) //  'br.H1_N2HDM_b')
        Open(NH2c,File =outpath(1:loutpath) //  'br.H1_N2HDM_c')
        WRITE(NH2a,906)'MH1  ','B B  ','TAU TAU','MU MU ','S S ','C C ',
     .    'T T '
        WRITE(NH2a,69)
        WRITE(NH2a,*)
        WRITE(NH2b,905)'MH1  ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH2b,69)
        WRITE(NH2b,*)
        WRITE(NH2c,906)'MH1   ','HD HD','AD AD ','Z AD ','W+- HD+-',
     .    'HD+ HD- ', 'WIDTH '
        WRITE(NH2c,69)
        WRITE(NH2c,*)
c header H2
        Open(NH3a,File =outpath(1:loutpath) //  'br.H2_N2HDM_a')
        Open(NH3b,File =outpath(1:loutpath) //  'br.H2_N2HDM_b')
        Open(NH3c,File =outpath(1:loutpath) //  'br.H2_N2HDM_c')
        Open(NH3d,File =outpath(1:loutpath) //  'br.H2_N2HDM_d')
        WRITE(NH3a,906)'MH2  ','B B  ','TAU TAU','MU MU ','S S ','C C ',
     .    'T T '
        WRITE(NH3a,69)
        WRITE(NH3a,*)
        WRITE(NH3b,905)'MH2  ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH3b,69)
        WRITE(NH3b,*)
        WRITE(NH3c,906)'MH2  ','HD HD','H1 H1 ','AD AD ','Z AD ',
     .    'W+- HD+-','HD+ HD- '
        WRITE(NH3c,69)
        WRITE(NH3c,*)
        WRITE(NH3d,901)'MH2  ','WIDTH '
        WRITE(NH3d,69)
        WRITE(NH3d,*)
c header dark charged Higgs H+-
        Open(NCa,File =outpath(1:loutpath) //  'br.HD+_N2HDM')

        WRITE(NCA,903)'MHD+   ','HD W ','AD W','WIDTH '
        WRITE(NCA,69)
        WRITE(NCA,*)
c header pseudoscalar AD
        Open(NAa,File =outpath(1:loutpath) //  'br.AD_N2HDM')

        WRITE(NAa,903)'MAD   ','Z HD ','W+- HD+-','WIDTH '
        WRITE(NAa,69)
        WRITE(NAa,*)

      elseif(IPHASE.eq.4)then
c header HDD
        Open(NH1a,File =outpath(1:loutpath) //  'br.HDD_N2HDM')
        WRITE(NH1a,903)'MHDD   ','Z AD ','W+- HD+-','WIDTH '
        WRITE(NH1a,69)
        WRITE(NH1a,*)
c header Hsm
        Open(NH3a,File =outpath(1:loutpath) //  'br.Hsm_N2HDM_a')
        Open(NH3b,File =outpath(1:loutpath) //  'br.Hsm_N2HDM_b')
        Open(NH3c,File =outpath(1:loutpath) //  'br.Hsm_N2HDM_c')
        WRITE(NH3a,906)'MHsm ','B B  ','TAU TAU','MU MU ','S S ','C C ',
     .    'T T '
        WRITE(NH3a,69)
        WRITE(NH3a,*)
        WRITE(NH3b,905)'MHsm  ','G G ','GAM GAM','Z GAM ','W W ','Z Z '
        WRITE(NH3b,69)
        WRITE(NH3b,*)
        WRITE(NH3c,905)'MHsm   ','HDD HDD','AD AD ','HD+ HD- ',
     .                 'HDS HDS', 'WIDTH '
        WRITE(NH3c,69)
        WRITE(NH3c,*)
c header dark charged Higgs H+-
        Open(NCa,File =outpath(1:loutpath) //  'br.HD+_N2HDM')
        WRITE(NCA,903)'MHD+   ','HDD W ','AD W','WIDTH '
        WRITE(NCA,69)
        WRITE(NCA,*)
c header pseudoscalar AD
        Open(NAa,File =outpath(1:loutpath) //  'br.AD_N2HDM')
        WRITE(NAa,903)'MAD   ','Z HDD ','W+- HD+-','WIDTH '
        WRITE(NAa,69)
        WRITE(NAa,*)
      endif


69     FORMAT(79('_'))
901    FORMAT(A9,1X,A10)
902    FORMAT(A9,2(1X,A10))
903    FORMAT(A9,3(1X,A10))
904    FORMAT(A9,4(1X,A10))
905    FORMAT(A9,5(1X,A10))
906    FORMAT(A9,6(1X,A10))

      RETURN
      END
