      DOUBLE PRECISION FUNCTION BIJ_HDEC(X,Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMB_HDEC
      BIJ_HDEC = (1-X-Y)/LAMB_HDEC(X,Y)*(
     .          4*SP_HDEC(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .        - 2*SP_HDEC(-XI_HDEC(X,Y)) - 2*SP_HDEC(-XI_HDEC(Y,X))
     .        + 2*DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .           *DLOG(1-XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .        - DLOG(XI_HDEC(X,Y))*DLOG(1+XI_HDEC(X,Y))
     .        - DLOG(XI_HDEC(Y,X))*DLOG(1+XI_HDEC(Y,X))
     .          )
     .        -4*(DLOG(1-XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .        +XI_HDEC(X,Y)*XI_HDEC(Y,X)/(1-XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .          *DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X)))
     .        +(LAMB_HDEC(X,Y)+X-Y)/LAMB_HDEC(X,Y)*(DLOG(1+XI_HDEC(X,Y))
     .              - XI_HDEC(X,Y)/(1+XI_HDEC(X,Y))*DLOG(XI_HDEC(X,Y)))
     .        +(LAMB_HDEC(X,Y)-X+Y)/LAMB_HDEC(X,Y)*(DLOG(1+XI_HDEC(Y,X))
     .              - XI_HDEC(Y,X)/(1+XI_HDEC(Y,X))*DLOG(XI_HDEC(Y,X)))
      RETURN
      END

      DOUBLE PRECISION FUNCTION BETA_HDEC(X)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      BETA_HDEC=DSQRT(1.D0-4.D0*X)
      RETURN
      END

      DOUBLE PRECISION FUNCTION LAMB_HDEC(X,Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LAMB_HDEC=DSQRT((1.D0-X-Y)**2-4.D0*X*Y)
      RETURN
      END

      DOUBLE PRECISION FUNCTION XI_HDEC(X,Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMB_HDEC
      XI_HDEC = 2*X/(1-X-Y+LAMB_HDEC(X,Y))
      RETURN
      END

C ===================== THE FUNCTION F0 ===============
      COMPLEX*16 FUNCTION F0_HDEC(M1,M2,QSQ)
      IMPLICIT REAL*8 (A-H,M,O-Z)
      COMPLEX*16 CD,CR,CQ2,IEPS,CBET,CXX
      M1SQ = M1*M1
      M2SQ = M2*M2
      AQSQ = DABS(QSQ)
      IEPS = DCMPLX(1.D0,1.D-12)
      CQ2 = QSQ*IEPS
      CD = (M1SQ-M2SQ)/CQ2
      CR = CDSQRT((1+CD)**2 - 4*M1SQ/CQ2)
      IF(abs(QSQ).lt.1D-10) THEN
       F0_HDEC = 0.D0
      ELSE
       IF(abs(M1-M2).lt.1e-10) THEN
        F0_HDEC = -2.D0 + CR*CDLOG(-(1+CR)/(1-CR))
       ELSE
        CBET = CDSQRT(1-4*M1*M2/(CQ2 - (M1-M2)**2))
        CXX = (CBET-1)/(CBET+1)
        F0_HDEC = -1 + ((QSQ+M2SQ-M1SQ)/2/QSQ - M2SQ/(M2SQ-M1SQ))
     .                                           *DLOG(M2SQ/M1SQ)
     .     - (QSQ-(M1-M2)**2)/QSQ*CBET*CDLOG(CXX)
       ENDIF
      ENDIF
      RETURN
      END

C     ************************************************************
C     SUBROUTINE FOR HSM ---> V*V* ---> 4F
C     ************************************************************
      SUBROUTINE HTOVV_HDEC(IV,AMH,AMV,GAMV,HTVV)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/VVOFF_HDEC/AMH1,AMV1,GAMV1
      COMMON/VVOFFFLAG_HDEC/IV1
      COMMON/PREC_HDEC/IP
      EXTERNAL FTOVV1_HDEC
c     IP=20
      IP=50
      AMH1=AMH
      AMV1=AMV
      GAMV1=GAMV
      IV1 = IV
      DLT=1D0/IP
      SUM=0D0
      DO 1 I=1,IP
       UU=DLT*I
       DD=UU-DLT
       CALL QGAUS1_HDEC(FTOVV1_HDEC,DD,UU,RES)
       SUM=SUM+RES
1     CONTINUE
      HTVV=SUM
      RETURN
      END

      DOUBLE PRECISION FUNCTION FTOVV1_HDEC(XX)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/FIRST_HDEC/X1
      COMMON/PREC_HDEC/IP
      EXTERNAL FTOVV2_HDEC
      X1=XX
      DLT=1D0/IP
      SUM=0D0
      DO 1 I=1,IP
       UU=DLT*I
       DD=UU-DLT
       CALL QGAUS2_HDEC(FTOVV2_HDEC,DD,UU,RES)
       SUM=SUM+RES
1     CONTINUE
      FTOVV1_HDEC=SUM
      RETURN
      END

      DOUBLE PRECISION FUNCTION FTOVV2_HDEC(XX)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION YY(2)
      COMMON/FIRST_HDEC/X1
      YY(1)=X1
      YY(2)=XX
      FTOVV2_HDEC=FTOVV_HDEC(YY)
      RETURN
      END

      DOUBLE PRECISION FUNCTION FTOVV_HDEC(XX)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMB
      DIMENSION XX(2)
      COMMON/VVOFF_HDEC/AMH,AMW,GAMW
      LAMB(X,Y)=DSQRT((1.D0-X-Y)**2-4.D0*X*Y)
      PI=4D0*DATAN(1D0)
      IF(AMH.LT.2*AMW)THEN
       ICASE = 1
      ELSE
       ICASE = 0
      ENDIF
      IF(ICASE.EQ.0)THEN
       YY = AMH**2
       Y1 = DATAN((YY-AMW**2)/AMW/GAMW)
       Y2 = -DATAN((AMW**2)/AMW/GAMW)
       DJAC = Y1-Y2
       T1 = TAN(Y1*XX(1)+Y2*(1.D0-XX(1)))
       SP = AMW**2 + AMW*GAMW*T1
       YY = (AMH-DSQRT(SP))**2
       Y1 = DATAN((YY-AMW**2)/AMW/GAMW)
       Y2 = -DATAN((AMW**2)/AMW/GAMW)
       DJAC = DJAC*(Y1-Y2)
       T2 = TAN(Y1*XX(2)+Y2*(1.D0-XX(2)))
       SM = AMW**2 + AMW*GAMW*T2
       AM2=AMH**2
       GAM = AM2*LAMB(SP/AM2,SM/AM2)*(1+LAMB(SP/AM2,SM/AM2)**2*AMH**4
     .                               /SP/SM/12)
       PRO1 = SP/AMW**2
       PRO2 = SM/AMW**2
       FTOVV_HDEC = PRO1*PRO2*GAM*DJAC/PI**2
     .            * RADVV_HDEC(SP,SM)
      ELSE
       SP = AMH**2*XX(1)
       SM = (AMH-DSQRT(SP))**2*XX(2)
       DJAC = AMH**2*(AMH-DSQRT(SP))**2/PI**2
       AM2=AMH**2
       GAM = AM2*LAMB(SP/AM2,SM/AM2)*(1+LAMB(SP/AM2,SM/AM2)**2*AMH**4
     .                               /SP/SM/12)
       PRO1 = SP*GAMW/AMW/((SP-AMW**2)**2+AMW**2*GAMW**2)
       PRO2 = SM*GAMW/AMW/((SM-AMW**2)**2+AMW**2*GAMW**2)
       FTOVV_HDEC = PRO1*PRO2*GAM*DJAC
     .            * RADVV_HDEC(SP,SM)
      ENDIF
      RETURN
      END

      DOUBLE PRECISION FUNCTION RADVV_HDEC(Q12,Q22)
      IMPLICIT DOUBLE PRECISION (A-B,D-H,O-Z), COMPLEX*16 (C)
      COMMON/VVOFFFLAG_HDEC/IV
      q1 = dsqrt(q12)
      q2 = dsqrt(q22)
      if(iv.eq.0)then
       radvv_hdec=1
      elseif(iv.eq.1)then
       radvv_hdec=radww_hdec(q1,q2)
      else
       radvv_hdec=radzz_hdec(q1,q2)
      endif
      RETURN
      END

      DOUBLE PRECISION FUNCTION RADWW_HDEC(Q1W,Q2W)
C     ************************************************************
C     ELECTROWEAK CORRECTIONS TO HSM ---> W*W* ---> 4F (APPROX.)
C     (A. BREDENSTEIN ET AL., PHYS. REV. D74 (2006) 013004
C                             [ARXIV:HEP-PH/0604011])
C     ************************************************************
      IMPLICIT DOUBLE PRECISION (A-B,D-H,O-Z), COMPLEX*16 (C)
      DOUBLE PRECISION LAMB
      COMMON/VVOFF_HDEC/AMH,AMW,GAMW
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW0
      COMMON/MASSES_HDEC/AMS0,AMC0,AMB0,AMT
      COMMON/WZWDTH_HDEC/GAMC0,GAMT,GAMT1,GAMW0,GAMZ
      LAMB(X,Y)=DSQRT((1.D0-X-Y)**2-4.D0*X*Y)
      PI=4D0*DATAN(1D0)
      cmut2 = dcmplx(amt**2,-amt*gamt)
      cmuw2 = dcmplx(amw**2,-amw*gamw)
      cmuz2 = dcmplx(amz**2,-amz*gamz*3)
      cbett = cdsqrt(1-4*cmut2/amh**2)
      cbetw = cdsqrt(1-4*cmuw2/amh**2)
      cbetz = cdsqrt(1-4*cmuz2/amh**2)
      cxt = (cbett-1)/(cbett+1)
      cthww = 8+12*cbett**2+3*cbett*(3*cbett**2-1)*cdlog(cxt)
     .      + 3/2.d0*(1-cbett**2)**2*cdlog(cxt)**2
      cbetwb = cdsqrt(dcmplx(amh**4+q1w**4+q2w**4-2*amh**2*q1w**2
     .             -2*amh**2*q2w**2-2*q1w**2*q2w**2))/amh**2
      cgw = (1-cbetwb**2)**2
      dmw = dabs(q1w**2-q2w**2)/amh**2
      cdcoul = alph/cbetwb*dimag(cdlog((cbetw-cbetwb+dmw)
     .                                /(cbetw+cbetwb+dmw)))
c     cgz = 1
      cgz = 0.7d0
      cdcoulz = alph/2/cbetz*dimag(cdlog((cbetz-cbetwb)
     .                                  /(cbetz+cbetwb)))
c     bias = 0.04d0*(1+(100-amh)/500)
      bias = 0.05d0*(1+(100-amh)/500)
      radww_hdec=dreal(1.d0
     .           +gf*cmut2/8.d0/pi**2/dsqrt(2.d0)*(-5+cthww)
     .           +gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.800952d0
     .           +(gf*amh**2/16.d0/pi**2/dsqrt(2.d0))**2*62.0308d0
     .           +cgw*cdcoul + cgz*cdcoulz + bias)
      RETURN
      END

      DOUBLE PRECISION FUNCTION RADZZ_HDEC(Q1Z,Q2Z)
C     ************************************************************
C     ELECTROWEAK CORRECTIONS TO HSM ---> Z*Z* ---> 4F (APPROX.)
C     (A. BREDENSTEIN ET AL., PHYS. REV. D74 (2006) 013004
C                             [ARXIV:HEP-PH/0604011])
C     ************************************************************
      IMPLICIT DOUBLE PRECISION (A-B,D-H,O-Z), COMPLEX*16 (C)
      DOUBLE PRECISION LAMB
      COMMON/VVOFF_HDEC/AMH,AMZ,GAMZ
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ0,AMW
      COMMON/MASSES_HDEC/AMS0,AMC0,AMB0,AMT
      COMMON/WZWDTH_HDEC/GAMC0,GAMT,GAMT1,GAMW,GAMZ0
      LAMB(X,Y)=DSQRT((1.D0-X-Y)**2-4.D0*X*Y)
      PI=4D0*DATAN(1D0)
      cmut2 = dcmplx(amt**2,-amt*gamt)
      cmuw2 = dcmplx(amw**2,-amw*gamw)
      cmuz2 = dcmplx(amz**2,-amz*gamz*2)
      cbett = cdsqrt(1-4*cmut2/amh**2)
      cbetw = cdsqrt(1-4*cmuw2/amh**2)
      cbetz = cdsqrt(1-4*cmuz2/amh**2)
      cxt = (cbett-1)/(cbett+1)
      cthzz = 20+6*cbett**2+3*cbett*(cbett**2+1)*cdlog(cxt)
     .      + 3*(1-cbett**2)*cdlog(cxt)**2
      cbetzb = cdsqrt(dcmplx(amh**4+q1z**4+q2z**4-2*amh**2*q1z**2
     .             -2*amh**2*q2z**2-2*q1z**2*q2z**2))/amh**2
c     cgw =-0.15d0 * (1-cbetw**2)**2
      cgw =-0.10d0 * (1-cbetw**2)**2
      cdcoulw = alph/2/cbetw*dimag(cdlog((cbetw-cbetzb)
     .                                  /(cbetw+cbetzb)))
      cgz = 1
      cdcoulz = alph/2/cbetz*dimag(cdlog((cbetz-cbetzb)
     .                                  /(cbetz+cbetzb)))
c     bias = 0.02d0*(1+(100-amh)/350)
      bias = 0.02d0*(1+(100-amh)/300)
      radzz_hdec=dreal(1.d0
     .           +gf*cmut2/8.d0/pi**2/dsqrt(2.d0)*(1+cthzz)
     .           +gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.800952d0
     .           +(gf*amh**2/16.d0/pi**2/dsqrt(2.d0))**2*62.0308d0
     .           +cgw*cdcoulw + cgz*cdcoulz + bias)
      RETURN
      END


C     **************************************************
C     SUBROUTINE FOR A -> TT* -> TBW
C     **************************************************

      subroutine ATOTT_HDEC(ama,amt,amb,amw,amch,att0,gab,gat)

      implicit real*8(a-z)
      integer ip,k
      common/prec1_hdec/ip
      external funatt1_hdec
      common/iksy1_hdec/x1,x2,m1,m2,m3,ecm,s
      common/top1_hdec/ama1,amt1,amb1,amw1,amch1
      common/top6_hdec/gab1,gat1
      ama1=ama
      amt1=amt
      amb1=amb
      amw1=amw
      amch1=amch
      gab1 = gab
      gat1 = gat

      ip=5
      m1=amb
      m2=amt
      m3=amw
c        first integrate over x2, i.e. (1+3) system
c        check whether enough phase space
      mastot=m1+m2+m3
      if(mastot.ge.ama) goto 12
      ecm=ama
      s=ecm**2
      u1=(ecm-m2)**2
      d1=(m1+m3)**2
      u=(s-d1+m2**2)/s
      d=(s-u1+m2**2)/s
      del=(u-d)/ip
      u=d+del
      xsec=0.d0
      do k=1,ip
      call qgaus1_hdec(funatt1_hdec,d,u,ss)
      d=u
      u=d+del
      xsec=xsec+ss
      enddo
      att0=xsec
 12   continue
      return
      end

      DOUBLE PRECISION FUNCTION FUNATT1_HDEC(XL)
      IMPLICIT REAL*8(A-Z)
      INTEGER IP,I
      COMMON/IKSY1_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/PREC1_HDEC/IP
      EXTERNAL FUNATT2_HDEC
      X2=XL
      S13=S-S*X2+M2**2
      TEM=2.D0*DSQRT(S13)
      E2S=(S-S13-M2**2)/TEM
      E3S=(S13+M3**2-M1**2)/TEM
C     SECOND INTEGRAL OVER X1, i.e. (2+3) SYSTEM
      U1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)-DSQRT(E3S**2-M3**2))**2
      D1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)+DSQRT(E3S**2-M3**2))**2
      U=(S-D1+M1**2)/s
      D=(S-U1+M1**2)/s
      DEL=(U-D)/IP
      FUNATT1_HDEC=0.d0
      U=D+DEL
      DO I=1,IP
      CALL QGAUS2_HDEC(FUNATT2_HDEC,D,U,SS)
      FUNATT1_HDEC=FUNATT1_HDEC+SS
      D=U
      U=D+DEL
      ENDDO
      RETURN
      END

      DOUBLE PRECISION FUNCTION FUNATT2_HDEC(XK)
      IMPLICIT REAL*8(A-Z)
      COMMON/IKSY1_HDEC/X1,X2,M1,M2,M3,ECM,S
      X1=XK
      CALL ELEMATT_HDEC(SS)
      FUNATT2_HDEC=SS
      RETURN
      END

      subroutine ELEMATT_HDEC(res)
      implicit real*8(a-z)
      common/iksy1_hdec/x1,x2,m1,m2,m3,ecm,s
      common/top1_hdec/ama,amt,amb,amw,amch
      common/top6_hdec/gab,gat
      common/wzwdth_hdec/gamc0,gamt0,gamt1,gamw,gamz
      gamt=gamt1**2*amt**2/ama**4
      gamc=gamc0**2*amch**2/ama**4
      ch=amch**2/ama**2
      w=amw**2/ama**2
      t=amt**2/ama**2
      b=amb**2/ama**2
      y1=1-x1
      y2=1-x2
      x0=2.d0-x1-x2
      w1=(1.d0-x2)
      w2=(1.d0-x0+w-ch)
      w22=1.d0/ ((1.d0-x0+w-ch)**2+gamc)
      w11=1.d0/((1.d0-x2)**2+gamt)
      w12=w1*w2*w11*w22
      w3=(1.d0-x1)
      w33=1.d0/w3**2
      w13=w1*w11/w3
      w23=w2*w22/w3

      r11= 4.d0*b**2 - 8.d0*b*t + 4.d0*t**2 + 4.d0*b*w + 4.d0*t*w -
     .     8.d0*w**2 - 8.d0*b*y2 + 8.d0*t*y2 + 4.d0*b*y1*y2 -
     .     4.d0*t*y1*y2 + 8.d0*w*y1*y2 + 4.d0*y2**2 + 4.d0*b*y2**2 -
     .     4.d0*t*y2**2 + 4.d0*w*y2**2 - 4.d0*y1*y2**2 - 4.d0*y2**3
      r22= 4.d0/t*(4.d0*w - (y1+y2)**2)*(b**2*gab**2+gat**2*t*
     .     (-1.d0 + t - w + y1 + y2) + b*(4.d0*gab*gat*t + gat**2*t +
     .     gab**2*(-1 + t - w + y1 + y2)))
      r12= 2.d0*(-4.d0*(gat*(-2*w**2 -(t + y2)*(-1 + y1 + y2)*(y1 + y2)
     .     +w*(2*(-1 + t + y1) + (2 + y1)*y2 + y2**2)) +
     .     b*(gat*(2*w - y1 - y2) + gab*(4*w - (y1 + y2)**2))))
      r33= 4.d0*b**2 - 8.d0*b*t + 4.d0*t**2 + 4.d0*b*w + 4.d0*t*w -
     .     8.d0*w**2 + 8.d0*b*y1 - 8.d0*t*y1 + 4.d0*y1**2 -
     .     4.d0*b*y1**2 + 4.d0*t*y1**2 + 4.d0*w*y1**2 - 4.d0*y1**3 -
     .     4.d0*b*y1*y2 + 4.d0*t*y1*y2 + 8.d0*w*y1*y2 - 4.d0*y1**2*y2
      r13= -8.d0*b*(y1**2+2.d0*y1*y2+y2**2+2.d0*w)
      r23= -8.d0*(gat*t*(4.d0*w - (y1 + y2)**2) + gab*(-2.d0*w**2 -
     .     (y1 + y2)*(t + b*(-1.d0 + y1 + y2) + y1*(-1.d0 + y1 + y2)) +
     .     w*(2.d0*b + 2.d0*t +
     .     2.d0*(-1.d0 + y2) + y1*(2.d0 + y1 + y2))))

c --- take off charged Higgs exchange ---
      w22 = 0.D0
      w12 = 0.D0
      w23 = 0.D0
c ----------------------------------------

      res= (r11*w11*gat**2+r22*w22+r12*w12*gat+
     .     r33*w33*b/t*gab**2+r23*w23*gab*b/t+
     .     r13*w13*gab*gat)/gat**2

      return

      end

C     ************************************************************
C     SUBROUTINE FOR H ---> TT* ---> TBW
C     ************************************************************
      SUBROUTINE HTOTT_HDEC(AMH,AMT,AMB,AMW,AMCH,GHT,GHB,GAT,GAB,GHVV,
     .     GHIHPWM,HTT0)
      IMPLICIT REAL*8(A-Z)
      INTEGER IP,K
      COMMON/PREC1_HDEC/IP
      EXTERNAL FUNHTT1_HDEC
      COMMON/IKSY2_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/TOP2_HDEC/AMH2,AMT2,AMB2,AMW2,AMCH2,GHT2,GHB2,
     .     GAT2,GAB2,GHVV2
c maggie changed 20/8/2013
      COMMON/TOP4_HDEC/GLVV2
c end maggie changed 20/8/2013
      AMH2=AMH
      AMT2=AMT
      AMB2=AMB
      AMW2=AMW
      AMCH2=AMCH
      GHT2=GHT
      GHB2=GHB
      GAT2=GAT
      GAB2=GAB
      GHVV2=GHVV
c MMM changed 20/8/2013
      GLVV2=GHIHPWM
c end MMM changed 20/8/2013
      IP=5
      M1=AMB
      M2=AMT
      M3=AMW
C     FIRST INTEGRATE OVER X2, i.e. (1+3) SYSTEM
C        CHECK WHETHER ENOUGH PHASE SPACE
      MASTOT=M1+M2+M3
      IF(MASTOT.GE.AMH) GOTO 12
      ECM=AMH
      S=ECM**2
      U1=(ECM-M2)**2
      D1=(M1+M3)**2
      U=(S-D1+M2**2)/s
      D=(S-U1+M2**2)/s
      DEL=(U-D)/IP
      U=D+DEL
      XSEC=0.D0
      DO K=1,IP
      CALL QGAUS1_HDEC(FUNHTT1_HDEC,D,U,SS)
      D=U
      U=D+DEL
      XSEC=XSEC+SS
      ENDDO
      HTT0=XSEC
 12   CONTINUE
      RETURN
      END

      DOUBLE PRECISION FUNCTION FUNHTT1_HDEC(XL)
      IMPLICIT REAL*8(A-Z)
      INTEGER IP,I
      COMMON/IKSY2_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/PREC1_HDEC/IP
      EXTERNAL FUNHTT2_HDEC
      X2=XL
      S13=S-S*X2+M2**2
      TEM=2.D0*DSQRT(S13)
      E2S=(S-S13-M2**2)/TEM
      E3S=(S13+M3**2-M1**2)/TEM
C     SECOND INTEGRAL OVER X1, i.e. (2+3) SYSTEM
      U1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)-DSQRT(E3S**2-M3**2))**2
      D1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)+DSQRT(E3S**2-M3**2))**2
      U=(S-D1+M1**2)/s
      D=(S-U1+M1**2)/s
      DEL=(U-D)/IP
      FUNHTT1_HDEC=0.d0
      U=D+DEL
      DO I=1,IP
      CALL QGAUS2_HDEC(FUNHTT2_HDEC,D,U,SS)
      FUNHTT1_HDEC=FUNHTT1_HDEC+SS
      D=U
      U=D+DEL
      ENDDO
      RETURN
      END

      DOUBLE PRECISION FUNCTION FUNHTT2_HDEC(XK)
      IMPLICIT REAL*8(A-Z)
      COMMON/IKSY2_HDEC/X1,X2,M1,M2,M3,ECM,S
      X1=XK
      CALL ELEMHTT_HDEC(SS)
      FUNHTT2_HDEC=SS
      RETURN
      END

      SUBROUTINE ELEMHTT_HDEC(RES)
      IMPLICIT REAL*8(A-Z)
      COMMON/IKSY2_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/TOP2_HDEC/AMH,AMT,AMB,AMW,AMCH,GHT,GHB,GAT,GAB,GHVV
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW0,GAMZ0
      COMMON/TOP4_HDEC/GLVV
      GAMT=GAMT1**2*AMT**2/AMH**4
      GAMC=GAMC0**2*AMCH**2/AMH**4
      GAMW=GAMW0**2*AMW**2/AMH**4
      CH=AMCH**2/AMH**2
      W=AMW**2/AMH**2
      T=AMT**2/AMH**2
      B=AMB**2/AMH**2
      Y1=1-X2
      Y2=1-X1
      X0=2.D0-X1-X2

      W1=(1.D0-X2)
      W2=(1.D0-X0+W-CH)
      W3 = (1.D0-X0)
      W22=1.D0/ (W2**2+GAMC)
      W11=1.D0/(W1**2+GAMT)
      W33=1.D0/(W3**2+GAMW)
      W12=W1*W2*W11*W22
      W13=W1*W3*W11*W33
      W23=W2*W3*W22*W33
      W4=(1.d0-X1)
      W44=1.d0/W4**2
      W14=W1*W11/W4
      W24=W2*W22/W4
      W34=W3*W33/W4

      R11=4.d0*B**2-8.d0*B*T-16.d0*B**2*T+4.d0*T**2+32.d0*B*T**2-
     .     16.d0*T**3+4.d0*B*W+4.d0*T*W-16.d0*B*T*W-16.d0*T**2*W-
     .     8.d0*W**2+32.d0*T*W**2-8.d0*B*Y1+8.d0*T*Y1+32.d0*B*T*Y1-
     .     32.d0*T**2*Y1-16.d0*T*W*Y1+4.d0*Y1**2+4.d0*B*Y1**2-
     .     20.d0*T*Y1**2+4.d0*W*Y1**2-4.d0*Y1**3+4.d0*B*Y1*Y2 -
     .     4.d0*T*Y1*Y2+8.d0*W*Y1*Y2-4.d0*Y1**2*Y2
      R22= 4.d0*(4.d0*W - (Y1+Y2)**2)*(b**2*GAB**2+GAT**2*t*
     .     (-1.d0 + T - W + Y1 + Y2) + B*(4.d0*GAB*GAT*T + GAT**2*T +
     .     GAB**2*(-1 + T - W + Y1 + Y2)))/T
      R33=-4.d0*B*W+4.d0*B**2*W-4.d0*T*W-8.d0*B*T*W+4.d0*T**2*W+
     .     4.d0*B*W**2-4.d0*B**2*W**2+4.d0*T*W**2+8.d0*B*T*W**2-
     .     4.d0*T**2*W**2+4.d0*W**3-4.d0*B*W**3-4.d0*T*W**3+8.d0*W**4+
     .     4.d0*B*W*Y1-4.d0*B**2*W*Y1+4.d0*T*W*Y1+8.d0*B*T*W*Y1-
     .     4.d0*T**2*W*Y1-8.d0*W**3*Y1+B*Y1**2-B**2*Y1**2+T*Y1**2+
     .     2.d0*B*T*Y1**2-T**2*Y1**2+B*W*Y1**2-3.d0*T*W*Y1**2-B*Y1**3-
     .     T*Y1**3+4.d0*B*W*Y2-4.d0*B**2*W*Y2+4.d0*T*W*Y2+
     .     8.d0*B*T*W*Y2-4.d0*T**2*W*Y2-8.d0*W**3*Y2+2.d0*B*Y1*Y2-
     .     2.d0*B**2*Y1*Y2+2.d0*T*Y1*Y2+4.d0*B*T*Y1*Y2-2.d0*T**2*Y1*Y2-
     .     2.d0*B*W*Y1*Y2-2.d0*T*W*Y1*Y2+4.d0*W**2*Y1*Y2-
     .     3.d0*B*Y1**2*Y2-3.d0*T*Y1**2*Y2+B*Y2**2-B**2*Y2**2+T*Y2**2+
     .     2.d0*B*T*Y2**2-T**2*Y2**2-3.d0*B*W*Y2**2+T*W*Y2**2-
     .     3.d0*B*Y1*Y2**2-3.d0*T*Y1*Y2**2-B*Y2**3-T*Y2**3
      R12=4.d0*(B*GAB*((Y1*(2.d0*T - 2*W + Y1)) +
     .     2.d0*(T + W)*Y2 - Y2**2 - 2.d0*B*(Y1 + Y2)) +
     .     GAT*(2.d0*W**2 - W*(2.d0*B + 2.d0*T*(3 + Y1 - Y2) +
     .     2.d0*(-1.d0 + Y1 + Y2) + Y1*(Y1 + Y2)) +
     .     (Y1 + Y2)*(B - 2.d0*B*T +
     .     (T + Y1)*(-1.d0 + 2.d0*T + Y1 + Y2))))
c original HDECAY sign in R13 not correct
      R13=(8.d0*W-8*B*W+16.d0*B**2*W-24.d0*T*W-32.d0*B*T*W+16.d0*T**2*W+
     .     16.d0*B*W**2+16.d0*T*W**2-32.d0*W**3+4.d0*B*Y1+8.d0*B**2*Y1-
     .     4.d0*T*Y1-16.d0*B*T*Y1+8.d0*T**2*Y1-8.d0*W*Y1-16.d0*B*W*Y1+
     .     16.d0*T*W*Y1+24.d0*W**2*Y1-4.d0*Y1**2-4.d0*B*Y1**2+
     .     12.d0*T*Y1**2+4.d0*W*Y1**2+4.d0*Y1**3+4.d0*B*Y2+8.d0*B**2*Y2-
     .     4.d0*T*Y2-16.d0*B*T*Y2+8.d0*T**2*Y2-8.d0*W*Y2+16.d0*W**2*Y2-
     .     4.d0*Y1*Y2+16.d0*T*Y1*Y2-12.d0*W*Y1*Y2+8.d0*Y1**2*Y2+
     .     4.d0*B*Y2**2+4.d0*T*Y2**2+4.d0*Y1*Y2**2)*(-1.d0)
      R23=-4.d0*(B**2*GAB*(2.d0*W*(-2.d0 + Y1 + Y2) + (Y1 + Y2)**2) -
     .     GAT*T*(2*W**2*(-Y1 + Y2) + (Y1 + Y2)**2*(-1 + T + Y1 + Y2) +
     .     W*(4 + (-4 + Y1)*Y1 - Y2*(4 + Y2) + 2*T*(-2 + Y1 + Y2))) +
     .     B*(GAT*T*(2*W*(-2 + Y1 + Y2) + (Y1 + Y2)**2) + GAB*
     .     (2*W**2*(Y1 - Y2) + (Y1 + Y2)**2*(-1 - T + Y1 + Y2) -
     .     W*(Y1*(4 + Y1) - (-2 + Y2)**2 + 2*T*(-2 + Y1 + Y2)))))
      R44=4.d0*B**2-16.d0*B**3-8.d0*B*T+32.d0*B**2*T+4.d0*T**2-
     .     16.d0*B*T**2+4.d0*B*W-16.d0*B**2*W+4.d0*T*W-16.d0*B*T*W-
     .     8.d0*W**2+32.d0*B*W**2+8.d0*B*Y2-32.d0*B**2*Y2-8.d0*T*Y2+
     .     32.d0*B*T*Y2-16.d0*B*W*Y2-4.d0*B*Y1*Y2+4.d0*T*Y1*Y2+
     .     8.d0*W*Y1*Y2+4.d0*Y2**2-20.d0*B*Y2**2+4.d0*T*Y2**2+
     .     4.d0*W*Y2**2-4.d0*Y1*Y2**2-4.d0*Y2**3
      R14=-16.d0*B**2+32.d0*B*T-16.d0*T**2+8.d0*W-16.d0*B*W-16.d0*T*W+
     .     32.d0*W**2-16.d0*B*Y2+16.d0*T*Y2-8.d0*W*Y2-4.d0*Y2**2+
     .     16.d0*B*Y1-16.d0*T*Y1-8.d0*W*Y1+8.d0*Y2*Y1-4.d0*Y1**2
      R24= 4.d0*(GAT*T*(Y1*(2.d0*T - 2*W + Y1) + 2.d0*(T + W)*Y2 -
     .     Y2**2 - 2.d0*B*(Y1 + Y2)) + GAB*(-2.d0*W**2 +
     .     W*(2.d0*B*(3.d0 - Y1 + Y2) + Y2*(Y1 + Y2) +
     .     2.d0*(-1.d0 + T + Y1 + Y2)) - (Y1 + Y2)*(2.d0*B**2 + T +
     .     Y2*(-1.d0 + Y1 + Y2) + B*(-1.d0 - 2*T + Y1 + 3.d0*Y2))))
      R34=-8.d0*W+24.d0*B*W-16.d0*B**2*W+8*T*W+32.d0*B*T*W-16.d0*T**2*W-
     .     16.d0*B*W**2-16.d0*T*W**2+32.d0*W**3+4.d0*B*Y1-8.d0*B**2*Y1-
     .     4.d0*T*Y1+16.d0*B*T*Y1-8.d0*T**2*Y1+8.d0*W*Y1-16.d0*W**2*Y1-
     .     4.d0*B*Y1**2-4.d0*T*Y1**2+4.d0*B*Y2-8.d0*B**2*Y2-4.d0*T*Y2+
     .     16.d0*B*T*Y2-8.d0*T**2*Y2+8.d0*W*Y2-16.d0*B*W*Y2+
     .     16.d0*T*W*Y2-24.d0*W**2*Y2+4.d0*Y1*Y2-16.d0*B*Y1*Y2+
     .     12.d0*W*Y1*Y2-4.d0*Y1**2*Y2+4.d0*Y2**2-12.d0*B*Y2**2+
     .     4.d0*T*Y2**2-4.d0*W*Y2**2-8.d0*Y1*Y2**2-4.d0*Y2**3

c - take off the charged H+ and W+ exchange -
      W22 = 0.D0
      W33 = 0.D0
      W12 = 0.D0
      W23 = 0.D0
      W13 = 0.D0
      W24 = 0.D0
      W34 = 0.D0
c -------------------------------------------

      RES=GHT**2*R11*W11+GLVV**2*R22*W22+
     .     4.D0*GHVV**2*R33*W33/T+2.D0*GHT*GLVV*R12*W12+
     .     2.D0*GHT*GHVV*R13*W13+2.D0*GHVV*GLVV*R23*W23/T+
     .     GHB**2*R44*W44*B/T+2.D0*B*GHB*GHT*R14*W14+
     .     2.D0*GHB*GLVV*R24*W24*B/T+
     .     2.d0*GHB*GHVV*R34*W34*B/T
      RETURN
      END

C     ************************************************************
C     SUBROUTINE FOR H+ ---> BT* ---> BBW
C     ************************************************************
      SUBROUTINE CTOTT_HDEC(AMCH,AMT,AMB,AMW,i2hdm,gat,gab,CTT0)
      IMPLICIT REAL*8(A-Z)
      INTEGER IP,K,i2hdm3,i2hdm
      COMMON/PREC1_HDEC/IP
      EXTERNAL FUNCTT1_HDEC
      COMMON/IKSY3_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/TOP3_HDEC/AMH3,AMT3,AMB3,AMW3
      COMMON/TOP5_HDEC/gat3,gab3,i2hdm3
      AMH3=AMCH
      AMT3=AMT
      AMB3=AMB
      AMW3=AMW
      gat3=gat
      gab3=gab
      i2hdm3=i2hdm
      IP=5
      M1=AMB
      M2=AMB
      M3=AMW
C     FIRST INTEGRATE OVER X2, i.e. (1+3) SYSTEM
C        CHECK WHETHER ENOUGH PHASE SPACE
      MASTOT=M1+M2+M3
      IF(MASTOT.GE.AMCH) GOTO 12
      ECM=AMCH
      S=ECM**2
      U1=(ECM-M2)**2
      D1=(M1+M3)**2
      U=(S-D1+M2**2)/s
      D=(S-U1+M2**2)/s
      DEL=(U-D)/IP
      U=D+DEL
      XSEC=0.D0
      DO K=1,IP
      CALL QGAUS1_HDEC(FUNCTT1_HDEC,D,U,SS)
      D=U
      U=D+DEL
      XSEC=XSEC+SS
      ENDDO
      CTT0=XSEC
12    CONTINUE
      RETURN
      END

      DOUBLE PRECISION FUNCTION FUNCTT1_HDEC(XL)
      IMPLICIT REAL*8(A-Z)
      INTEGER IP,I
      COMMON/IKSY3_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/PREC1_HDEC/IP
      EXTERNAL FUNCTT2_HDEC
      X2=XL
      S13=S-S*X2+M2**2
      TEM=2.D0*DSQRT(S13)
      E2S=(S-S13-M2**2)/TEM
      E3S=(S13+M3**2-M1**2)/TEM
C     SECOND INTEGRAL OVER X1, i.e. (2+3) SYSTEM
      U1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)-DSQRT(E3S**2-M3**2))**2
      D1=(E2S+E3S)**2-(DSQRT(E2S**2-M2**2)+DSQRT(E3S**2-M3**2))**2
      U=(S-D1+M1**2)/s
      D=(S-U1+M1**2)/s
      DEL=(U-D)/IP
      FUNCTT1_HDEC=0.d0
      U=D+DEL
      DO I=1,IP
      CALL QGAUS2_HDEC(FUNCTT2_HDEC,D,U,SS)
      FUNCTT1_HDEC=FUNCTT1_HDEC+SS
      D=U
      U=D+DEL
      ENDDO
      RETURN
      END

      DOUBLE PRECISION FUNCTION FUNCTT2_HDEC(XK)
      IMPLICIT REAL*8(A-Z)
      COMMON/IKSY3_HDEC/X1,X2,M1,M2,M3,ECM,S
      X1=XK
      CALL ELEMCTT_HDEC(SS)
      FUNCTT2_HDEC=SS
      RETURN
      END

      SUBROUTINE ELEMCTT_HDEC(RES)
      IMPLICIT REAL*8(A-Z)
      integer i2hdm
      COMMON/IKSY3_HDEC/X1,X2,M1,M2,M3,ECM,S
      COMMON/TOP3_HDEC/AMCH,AMT,AMB,AMW
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      COMMON/TOP5_HDEC/gat,gab,i2hdm
      GAMT=GAMT1**2*AMT**2/AMCH**4
      W=AMW**2/AMCH**2
      T=AMT**2/AMCH**2
      B=AMB**2/AMCH**2

c ---- w/o b-quark mass dependence ----

      RES=((1.D0-X1-W)*(1.D0-X2-W)+W*(X1+X2-1.D0+W))/
     .   ((1.D0-X2+B-T)**2+GAMT)

c ---- w/ b-quark mass dependence ----

      res=(-2.d0*b**3*gab**2*w+gat**2*t**2*((-1.d0+2.d0*w)*(-1.d0+w+x1)+
     .     (-1.d0+2.d0*w+x1)*x2)+b*(-2.d0*gat**2*t**2*w+2*gab*gat*t*
     .     (1.d0+2.d0*w-x2)*(-1.d0+w+x2)+gab**2*(-2.d0*w**2+
     .     (-1.d0+x2)**2*(-1.d0+x1+x2)+w*(-1.d0+x2)*
     .     (-3.d0+2.d0*x1+x2)))+b**2*gab*(-4.d0*gat*t*w+gab*
     .     (2.d0*w**2+w*(3.d0-2.d0*x1)-(-1.d0+x2)*
     .     (-3.d0+x1+2.d0*x2))))/((1.d0-x2+b-t)**2+gamt)/t**2/gat**2

      RETURN
      END

C   *****************  INTEGRATION ROUTINE ***********************
C    Returns SS as integral of FUNC from A to B, by 10-point Gauss-
C    Legendre integration
      SUBROUTINE QGAUS1_HDEC(FUNC,A,B,SS)
      IMPLICIT REAL*8(A-Z)
      INTEGER J
      DIMENSION X(5),W(5)
      EXTERNAL FUNC
      DATA X/.1488743389D0,.4333953941D0,.6794095682D0
     .  ,.8650633666D0,.9739065285D0/
      DATA W/.2955242247D0,.2692667193D0,.2190863625D0
     .  ,.1494513491D0,.0666713443D0/
      XM=0.5D0*(B+A)
      XR=0.5D0*(B-A)
      SS=0.D0
      DO 11 J=1,5
        DX=XR*X(J)
        SS=SS+W(J)*(FUNC(XM+DX)+FUNC(XM-DX))
11    CONTINUE
      SS=XR*SS
      RETURN
      END

C     Returns SS as integral of FUNC from A to B, by 10-point Gauss-
C      Legendre integration
      SUBROUTINE QGAUS2_HDEC(FUNC,A,B,SS)
      IMPLICIT REAL*8(A-Z)
      INTEGER J
      DIMENSION X(5),W(5)
      EXTERNAL FUNC
      DATA X/.1488743389D0,.4333953941D0,.6794095682D0
     .  ,.8650633666D0,.9739065285D0/
      DATA W/.2955242247D0,.2692667193D0,.2190863625D0
     .  ,.1494513491D0,.0666713443D0/
      XM=0.5D0*(B+A)
      XR=0.5D0*(B-A)
      SS=0.D0
      DO 11 J=1,5
        DX=XR*X(J)
        SS=SS+W(J)*(FUNC(XM+DX)+FUNC(XM-DX))
11    CONTINUE
      SS=XR*SS
      RETURN
      END

C ******************************************************************

C      DOUBLE PRECISION FUNCTION RUNP_HDEC(Q,NF)
C      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C      COMMON/RUN_HDEC/XMSB,XMHAT,XKFAC
C      RUNP_HDEC = RUNM_HDEC(Q,NF)
C      RUNP_HDEC = RUNM_HDEC(Q/2.D0,NF)*XKFAC
C      RETURN
C      END

      DOUBLE PRECISION FUNCTION RUNM_HDEC(Q,NF0)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (NN=8)
      PARAMETER (ZETA3 = 1.202056903159594D0)
      DIMENSION AM(NN),YMSB(NN)
      COMMON/ALS_HDEC/XLAMBDA,AMCA,AMBA,AMTA,N0A
      COMMON/MASSES_HDEC/AMS,AMC,AMB,AMT
      COMMON/STRANGE_HDEC/AMSB
      COMMON/MSBAR_HDEC/AMCB,AMBB
      COMMON/RUN_HDEC/XMSB,XMHAT,XKFAC
      COMMON/FLAG_HDEC/IHIGGS,NNLO,IPOLE
c      COMMON/SM4_HDEC/AMTP,AMBP,AMNUP,AMEP,ISM4,IGGELW
      SAVE ISTRANGE
      B0(NF)=(33.D0-2.D0*NF)/12D0
      B1(NF) = (102D0-38D0/3D0*NF)/16D0
      B2(NF) = (2857D0/2D0-5033D0/18D0*NF+325D0/54D0*NF**2)/64D0
      G0(NF) = 1D0
      G1(NF) = (202D0/3D0-20D0/9D0*NF)/16D0
      G2(NF) = (1249D0-(2216D0/27D0+160D0/3D0*ZETA3)*NF
     .       - 140D0/81D0*NF**2)/64D0
      C1(NF) = G1(NF)/B0(NF) - B1(NF)*G0(NF)/B0(NF)**2
      C2(NF) = ((G1(NF)/B0(NF) - B1(NF)*G0(NF)/B0(NF)**2)**2
     .       + G2(NF)/B0(NF) + B1(NF)**2*G0(NF)/B0(NF)**3
     .       - B1(NF)*G1(NF)/B0(NF)**2 - B2(NF)*G0(NF)/B0(NF)**2)/2D0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     C1(NF) = 1.175d0
c     C2(NF) = 1.501d0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      TRAN(X,XK,XK2)=1D0+4D0/3D0*ALPHAS_HDEC(X,3)/PI
     .              +XK*(ALPHAS_HDEC(X,3)/PI)**2
     .              +XK2*(ALPHAS_HDEC(X,3)/PI)**3
      CQ(X,NF)=(2D0*B0(NF)*X)**(G0(NF)/B0(NF))
     .            *(1D0+LOOP2*C1(NF)*X+LOOP3*C2(NF)*X**2)
      DATA ISTRANGE/0/
      NF = NF0
      LOOP = 3
      LOOP2 = 1
      LOOP3 = 1
      IF(LOOP.LE.2)LOOP3 = 0
      IF(LOOP.LE.1)LOOP2 = 0
      PI=4D0*DATAN(1D0)
      ACC = 1.D-8
      AM(1) = 0
      AM(2) = 0
C--SCALE OF STRANGE AND CHARM MSBAR-MASS
c     QQS = 1.D0
      QQS = 2.D0
      QQC = 3.D0
C--------------------------------------------
      IMSBAR = 0
      IF(IMSBAR.EQ.1)THEN
       IF(ISTRANGE.EQ.0)THEN
C--STRANGE POLE MASS FROM MSBAR-MASS AT QQS
        AMSD = XLAMBDA
        AMSU = 1.D8
123     AMS  = (AMSU+AMSD)/2
        AM(3) = AMS
        XMSB = AMS/CQ(ALPHAS_HDEC(AMS,3)/PI,3)
     .            *CQ(ALPHAS_HDEC(QQS,3)/PI,3)/TRAN(AMS,0D0,0D0)
        DD = (XMSB-AMSB)/AMSB
        IF(DABS(DD).GE.ACC)THEN
         IF(DD.LE.0.D0)THEN
          AMSD = AM(3)
         ELSE
          AMSU = AM(3)
         ENDIF
         GOTO 123
        ENDIF
        ISTRANGE=1
       ENDIF
       AM(3) = AMSB
      ELSE
       AMS=AMSB
       AM(3) = AMS
      ENDIF
C--------------------------------------------
      AM(3) = AMSB
      AM(4) = AMC
      AM(5) = AMB
      AM(6) = AMT
      AM(7) = 100*AMT
      AM(8) = 200*AMT
      XK = 16.11D0
      DO 1 I=1,NF-1
       XK = XK - 1.0414D0*(1.D0-AM(I)/AM(NF))
1     CONTINUE
      XK2 = 0.65269D0*(NF-1)**2 - 29.7010D0*(NF-1) + 239.2966D0
      IF(NF.GE.4)THEN
c      XMSB = AM(NF)/TRAN(AM(NF),0D0,0D0)
       XMSB = AM(NF)/TRAN(AM(NF),XK,XK2)
       XMHAT = XMSB/CQ(ALPHAS_HDEC(AM(NF),3)/PI,NF)
      ELSE
       XMSB = 0
       XMHAT = 0
      ENDIF
      YMSB(3) = AMSB
      IF(NF.EQ.3)THEN
       IF(QQS.LT.AMC)THEN
        YMSB(4) = YMSB(3)*CQ(ALPHAS_HDEC(AM(4),3)/PI,3)/
     .                    CQ(ALPHAS_HDEC(QQS,3)/PI,3)
       ELSE
        YMSB(4) = AMSB*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                    CQ(ALPHAS_HDEC(QQS,3)/PI,4)
        YMSB(3) = YMSB(4)
       ENDIF
       YMSB(5) = YMSB(4)*CQ(ALPHAS_HDEC(AM(5),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,4)
       YMSB(6) = YMSB(5)*CQ(ALPHAS_HDEC(AM(6),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,5)
       YMSB(7) = YMSB(6)*CQ(ALPHAS_HDEC(AM(7),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,6)
       YMSB(8) = YMSB(7)*CQ(ALPHAS_HDEC(AM(8),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,7)
      ELSEIF(NF.EQ.4)THEN
       YMSB(4) = AMCB*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(QQC,3)/PI,4)
       YMSB(3) = YMSB(4)*CQ(ALPHAS_HDEC(QQS,3)/PI,3)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,3)
       YMSB(5) = YMSB(4)*CQ(ALPHAS_HDEC(AM(5),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,4)
       YMSB(6) = YMSB(5)*CQ(ALPHAS_HDEC(AM(6),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,5)
       YMSB(7) = YMSB(6)*CQ(ALPHAS_HDEC(AM(7),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,6)
       YMSB(8) = YMSB(7)*CQ(ALPHAS_HDEC(AM(8),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,7)
      ELSEIF(NF.EQ.5)THEN
       YMSB(5) = XMSB
       YMSB(4) = YMSB(5)*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,4)
       YMSB(3) = YMSB(4)*CQ(ALPHAS_HDEC(QQS,3)/PI,3)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,3)
       YMSB(6) = YMSB(5)*CQ(ALPHAS_HDEC(AM(6),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,5)
       YMSB(7) = YMSB(6)*CQ(ALPHAS_HDEC(AM(7),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,6)
       YMSB(8) = YMSB(7)*CQ(ALPHAS_HDEC(AM(8),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,7)
      ELSEIF(NF.EQ.6)THEN
       YMSB(6) = XMSB
       YMSB(5) = YMSB(6)*CQ(ALPHAS_HDEC(AM(5),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,5)
       YMSB(4) = YMSB(5)*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,4)
       YMSB(3) = YMSB(4)*CQ(ALPHAS_HDEC(QQS,3)/PI,3)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,3)
       YMSB(7) = YMSB(6)*CQ(ALPHAS_HDEC(AM(7),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,6)
       YMSB(8) = YMSB(7)*CQ(ALPHAS_HDEC(AM(8),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,7)
      ELSEIF(NF.EQ.7)THEN
       YMSB(7) = XMSB
       YMSB(6) = YMSB(7)*CQ(ALPHAS_HDEC(AM(6),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,6)
       YMSB(5) = YMSB(6)*CQ(ALPHAS_HDEC(AM(5),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,5)
       YMSB(4) = YMSB(5)*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,4)
       YMSB(3) = YMSB(4)*CQ(ALPHAS_HDEC(QQS,3)/PI,3)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,3)
       YMSB(8) = YMSB(7)*CQ(ALPHAS_HDEC(AM(8),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,7)
      ELSEIF(NF.EQ.8)THEN
       YMSB(8) = XMSB
       YMSB(7) = YMSB(8)*CQ(ALPHAS_HDEC(AM(7),3)/PI,7)/
     .                   CQ(ALPHAS_HDEC(AM(8),3)/PI,7)
       YMSB(6) = YMSB(7)*CQ(ALPHAS_HDEC(AM(6),3)/PI,6)/
     .                   CQ(ALPHAS_HDEC(AM(7),3)/PI,6)
       YMSB(5) = YMSB(6)*CQ(ALPHAS_HDEC(AM(5),3)/PI,5)/
     .                   CQ(ALPHAS_HDEC(AM(6),3)/PI,5)
       YMSB(4) = YMSB(5)*CQ(ALPHAS_HDEC(AM(4),3)/PI,4)/
     .                   CQ(ALPHAS_HDEC(AM(5),3)/PI,4)
       YMSB(3) = YMSB(4)*CQ(ALPHAS_HDEC(QQS,3)/PI,3)/
     .                   CQ(ALPHAS_HDEC(AM(4),3)/PI,3)
      ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     IF(Q.LT.AMC)THEN
c      N0=3
c      IF(QQS.LT.AMC)THEN
c       Q0 = QQS
c      ELSE
c       Q0 = AMC
c      ENDIF
c     ELSEIF(Q.LE.AMB)THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     IF(Q.LT.AMB)THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      IF(Q.LT.AMC)THEN
       N0=3
       Q0 = QQS
      ELSEIF(Q.LE.AMB)THEN
       N0=4
       Q0 = AMC
      ELSEIF(Q.LE.AMT)THEN
       N0=5
       Q0 = AMB
      ELSE
       N0=6
       Q0 = AMT
      ENDIF
      IF(NNLO.EQ.1.AND.NF.GT.3)THEN
       XKFAC = TRAN(AM(NF),0D0,0D0)/TRAN(AM(NF),XK,XK2)
      ELSE
       XKFAC = 1D0
      ENDIF
      RUNM_HDEC = YMSB(N0)*CQ(ALPHAS_HDEC(Q,3)/PI,N0)/
     .               CQ(ALPHAS_HDEC(Q0,3)/PI,N0)
c    .       * XKFAC
      RETURN
      END

      DOUBLE PRECISION FUNCTION ALPHAS_HDEC(Q,N)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XLB(6)
      COMMON/ALSLAM_HDEC/XLB1(6),XLB2(6),XLB3(6)
      COMMON/ALS_HDEC/XLAMBDA,AMC,AMB,AMT,N0
      B0(NF)=33.D0-2.D0*NF
      B1(NF)=6.D0*(153.D0-19.D0*NF)/B0(NF)**2
      B2(NF)=27/2.D0*(2857-5033/9.D0*NF+325/27.D0*NF**2)/B0(NF)**3
      ALS1(NF,X)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB(NF)**2))
      ALS2(NF,X)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB(NF)**2))
     .          *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB(NF)**2))
     .           /DLOG(X**2/XLB(NF)**2))
      ALS3(NF,X)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB(NF)**2))
     .          *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB(NF)**2))
     .           /DLOG(X**2/XLB(NF)**2)
     .           +(B1(NF)**2*(DLOG(DLOG(X**2/XLB(NF)**2))**2
     .                      -DLOG(DLOG(X**2/XLB(NF)**2))-1)+B2(NF))
     .           /DLOG(X**2/XLB(NF)**2)**2)
      PI=4.D0*DATAN(1.D0)
c     write(6,*)'ALS param: ',XLAMBDA,AMC,AMB,AMT,N0
      IF(N.EQ.1)THEN
       DO 1 I=1,6
        XLB(I)=XLB1(I)
1      CONTINUE
      ELSEIF(N.EQ.2)THEN
       DO 2 I=1,6
        XLB(I)=XLB2(I)
2      CONTINUE
      ELSE
       DO 3 I=1,6
        XLB(I)=XLB3(I)
3      CONTINUE
      ENDIF
      IF(Q.LT.AMC)THEN
       NF=3
      ELSEIF(Q.LE.AMB)THEN
       NF=4
      ELSEIF(Q.LE.AMT)THEN
       NF=5
      ELSE
       NF=6
      ENDIF
      IF(N.EQ.1)THEN
        ALPHAS_HDEC=ALS1(NF,Q)
      ELSEIF(N.EQ.2)THEN
        ALPHAS_HDEC=ALS2(NF,Q)
      ELSE
        ALPHAS_HDEC=ALS3(NF,Q)
c       ALPHAS_HDEC=ALS2(NF,Q)
      ENDIF
      RETURN
      END

      SUBROUTINE ALSINI_HDEC(ACC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XLB(6)
      COMMON/ALSLAM_HDEC/XLB1(6),XLB2(6),XLB3(6)
      COMMON/ALS_HDEC/XLAMBDA,AMC,AMB,AMT,N0
      PI=4.D0*DATAN(1.D0)
      XLB1(1)=0D0
      XLB1(2)=0D0
      XLB2(1)=0D0
      XLB2(2)=0D0
      IF(N0.EQ.3)THEN
       XLB(3)=XLAMBDA
       XLB(4)=XLB(3)*(XLB(3)/AMC)**(2.D0/25.D0)
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
      ELSEIF(N0.EQ.4)THEN
       XLB(4)=XLAMBDA
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
      ELSEIF(N0.EQ.5)THEN
       XLB(5)=XLAMBDA
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
      ELSEIF(N0.EQ.6)THEN
       XLB(6)=XLAMBDA
       XLB(5)=XLB(6)*(XLB(6)/AMT)**(-2.D0/23.D0)
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
      ENDIF
      DO 1 I=3,6
       XLB1(I)=XLB(I)
1     CONTINUE
      IF(N0.EQ.3)THEN
       XLB(3)=XLAMBDA
       XLB(4)=XLB(3)*(XLB(3)/AMC)**(2.D0/25.D0)
     .             *(2.D0*DLOG(AMC/XLB(3)))**(-107.D0/1875.D0)
       XLB(4)=XITER_HDEC(AMC,XLB(3),3,XLB(4),4,ACC)
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
     .             *(2.D0*DLOG(AMB/XLB(4)))**(-963.D0/13225.D0)
       XLB(5)=XITER_HDEC(AMB,XLB(4),4,XLB(5),5,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.4)THEN
       XLB(4)=XLAMBDA
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
     .             *(2.D0*DLOG(AMB/XLB(4)))**(-963.D0/13225.D0)
       XLB(5)=XITER_HDEC(AMB,XLB(4),4,XLB(5),5,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.5)THEN
       XLB(5)=XLAMBDA
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
     .             *(2.D0*DLOG(AMB/XLB(5)))**(963.D0/14375.D0)
       XLB(4)=XITER_HDEC(AMB,XLB(5),5,XLB(4),4,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.6)THEN
       XLB(6)=XLAMBDA
       XLB(5)=XLB(6)*(XLB(6)/AMT)**(-2.D0/23.D0)
     .            *(2.D0*DLOG(AMT/XLB(6)))**(321.D0/3703.D0)
       XLB(5)=XITER_HDEC(AMT,XLB(6),6,XLB(5),5,ACC)
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
     .             *(2.D0*DLOG(AMB/XLB(5)))**(963.D0/14375.D0)
       XLB(4)=XITER_HDEC(AMB,XLB(5),5,XLB(4),4,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
      ENDIF
      DO 2 I=3,6
       XLB2(I)=XLB(I)
2     CONTINUE
      IF(N0.EQ.3)THEN
       XLB(3)=XLAMBDA
       XLB(4)=XLB(3)*(XLB(3)/AMC)**(2.D0/25.D0)
     .             *(2.D0*DLOG(AMC/XLB(3)))**(-107.D0/1875.D0)
       XLB(4)=XITER3_HDEC(AMC,XLB(3),3,XLB(4),4,ACC)
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
     .             *(2.D0*DLOG(AMB/XLB(4)))**(-963.D0/13225.D0)
       XLB(5)=XITER3_HDEC(AMB,XLB(4),4,XLB(5),5,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER3_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.4)THEN
       XLB(4)=XLAMBDA
       XLB(5)=XLB(4)*(XLB(4)/AMB)**(2.D0/23.D0)
     .             *(2.D0*DLOG(AMB/XLB(4)))**(-963.D0/13225.D0)
       XLB(5)=XITER3_HDEC(AMB,XLB(4),4,XLB(5),5,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER3_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER3_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.5)THEN
       XLB(5)=XLAMBDA
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
     .             *(2.D0*DLOG(AMB/XLB(5)))**(963.D0/14375.D0)
       XLB(4)=XITER3_HDEC(AMB,XLB(5),5,XLB(4),4,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER3_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
       XLB(6)=XLB(5)*(XLB(5)/AMT)**(2.D0/21.D0)
     .            *(2.D0*DLOG(AMT/XLB(5)))**(-321.D0/3381.D0)
       XLB(6)=XITER3_HDEC(AMT,XLB(5),5,XLB(6),6,ACC)
      ELSEIF(N0.EQ.6)THEN
       XLB(6)=XLAMBDA
       XLB(5)=XLB(6)*(XLB(6)/AMT)**(-2.D0/23.D0)
     .            *(2.D0*DLOG(AMT/XLB(6)))**(321.D0/3703.D0)
       XLB(5)=XITER3_HDEC(AMT,XLB(6),6,XLB(5),5,ACC)
       XLB(4)=XLB(5)*(XLB(5)/AMB)**(-2.D0/25.D0)
     .             *(2.D0*DLOG(AMB/XLB(5)))**(963.D0/14375.D0)
       XLB(4)=XITER3_HDEC(AMB,XLB(5),5,XLB(4),4,ACC)
       XLB(3)=XLB(4)*(XLB(4)/AMC)**(-2.D0/27.D0)
     .             *(2.D0*DLOG(AMC/XLB(4)))**(107.D0/2025.D0)
       XLB(3)=XITER3_HDEC(AMC,XLB(4),4,XLB(3),3,ACC)
      ENDIF
      DO 3 I=3,6
       XLB3(I)=XLB(I)
3     CONTINUE
      RETURN
      END

      DOUBLE PRECISION FUNCTION XITER_HDEC(Q,XLB1,NF1,XLB,NF2,ACC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      B0(NF)=33.D0-2.D0*NF
      B1(NF)=6.D0*(153.D0-19.D0*NF)/B0(NF)**2
      ALS2(NF,X,XLB)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB**2))
     .              *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB**2))
     .              /DLOG(X**2/XLB**2))
      AA(NF)=12D0*PI/B0(NF)
      BB(NF)=B1(NF)/AA(NF)
      XIT(A,B,X)=A/2.D0*(1D0+DSQRT(1D0-4D0*B*DLOG(X)))
      PI=4.D0*DATAN(1.D0)
      XLB2=XLB
      II=0
1     II=II+1
      X=DLOG(Q**2/XLB2**2)
      ALP=ALS2(NF1,Q,XLB1)
      A=AA(NF2)/ALP
      B=BB(NF2)*ALP
      XX=XIT(A,B,X)
      XLB2=Q*DEXP(-XX/2.D0)
      Y1=ALS2(NF1,Q,XLB1)
      Y2=ALS2(NF2,Q,XLB2)
      DY=DABS(Y2-Y1)/Y1
      IF(DY.GE.ACC) GOTO 1
       XITER_HDEC=XLB2
      RETURN
      END

      DOUBLE PRECISION FUNCTION XITER3_HDEC(Q,XLB1,NF1,XLB,NF2,ACC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      B0(NF)=33.D0-2.D0*NF
      B1(NF)=6.D0*(153.D0-19.D0*NF)/B0(NF)**2
      B2(NF)=27/2.D0*(2857-5033/9.D0*NF+325/27.D0*NF**2)/B0(NF)**3
      ALS3(NF,X,XLB)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB**2))
     .          *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB**2))
     .           /DLOG(X**2/XLB**2)
     .           +(B1(NF)**2*(DLOG(DLOG(X**2/XLB**2))**2
     .                      -DLOG(DLOG(X**2/XLB**2))-1)+B2(NF))
     .           /DLOG(X**2/XLB**2)**2)
      AA(NF)=12D0*PI/B0(NF)
      BB(NF)=B1(NF)/AA(NF)
      CC(NF)=B2(NF)/AA(NF)
      XIT(A,B,C,X)=A/2.D0*(1D0+DSQRT(1D0-4D0*B*DLOG(X)
     .          *(1-(A*B*(DLOG(X)**2-DLOG(X)-1)+C/B)/X/DLOG(X))))
      PI=4.D0*DATAN(1.D0)
      XLB2=XLB
      II=0
1     II=II+1
      X=DLOG(Q**2/XLB2**2)
      IF(NF1.LT.NF2)THEN
       DELTA = 7*ALS3(NF1,Q,XLB1)**2/PI**2/24
       ALP=ALS3(NF1,Q,XLB1)*(1+DELTA)
      ELSE
       DELTA = 7*ALS3(NF1,Q,XLB1)**2/PI**2/24
       ALP=ALS3(NF1,Q,XLB1)/(1+DELTA)
      ENDIF
      A=AA(NF2)/ALP
      B=BB(NF2)*ALP
      C=CC(NF2)*ALP
      XX=XIT(A,B,C,X)
      XLB2=Q*DEXP(-XX/2.D0)
      IF(NF1.LT.NF2)THEN
       DELTA = 7*ALS3(NF1,Q,XLB1)**2/PI**2/24
       Y1=ALS3(NF1,Q,XLB1)*(1+DELTA)
       Y2=ALS3(NF2,Q,XLB2)
      ELSE
       DELTA = 7*ALS3(NF1,Q,XLB1)**2/PI**2/24
       Y1=ALS3(NF1,Q,XLB1)/(1+DELTA)
       Y2=ALS3(NF2,Q,XLB2)
      ENDIF
      DY=DABS(Y2-Y1)/Y1
      IF(DY.GE.ACC) GOTO 1
       XITER3_HDEC=XLB2
      RETURN
      END

      DOUBLE PRECISION FUNCTION FINT_HDEC(Z,XX,YY)
C--ONE-DIMENSIONAL CUBIC INTERPOLATION
C--Z  = WANTED POINT
C--XX = ARRAY OF 4 DISCRETE X-VALUES AROUND Z
C--YY = ARRAY OF 4 DISCRETE FUNCTION-VALUES AROUND Z
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XX(4),YY(4)
      X = DLOG(Z)
      X0=DLOG(XX(1))
      X1=DLOG(XX(2))
      X2=DLOG(XX(3))
      X3=DLOG(XX(4))
      Y0=DLOG(YY(1))
      Y1=DLOG(YY(2))
      Y2=DLOG(YY(3))
      Y3=DLOG(YY(4))
      A0=(X-X1)*(X-X2)*(X-X3)/(X0-X1)/(X0-X2)/(X0-X3)
      A1=(X-X0)*(X-X2)*(X-X3)/(X1-X0)/(X1-X2)/(X1-X3)
      A2=(X-X0)*(X-X1)*(X-X3)/(X2-X0)/(X2-X1)/(X2-X3)
      A3=(X-X0)*(X-X1)*(X-X2)/(X3-X0)/(X3-X1)/(X3-X2)
      FINT_HDEC=DEXP(A0*Y0+A1*Y1+A2*Y2+A3*Y3)
      RETURN
      END

      DOUBLE PRECISION FUNCTION SP_HDEC(X)
C--REAL DILOGARITHM (SPENCE-FUNCTION)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMPLEX*16 CX,LI2_HDEC
      CX = DCMPLX(X,0.D0)
      SP_HDEC = DREAL(LI2_HDEC(CX))
      RETURN
      END

      COMPLEX*16 FUNCTION LI2_HDEC(X)
C--COMPLEX DILOGARITHM (SPENCE-FUNCTION)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMPLEX*16 X,Y,CLI2_HDEC
      COMMON/CONST_HDEC/ZETA2,ZETA3
      ZERO=1.D-16
      XR=DREAL(X)
      XI=DIMAG(X)
      R2=XR*XR+XI*XI
      LI2_HDEC=0
      IF(R2.LE.ZERO)THEN
        LI2_HDEC=X
        RETURN
      ENDIF
      RR=XR/R2
      IF(abs(R2-1.D0).lt.zero.AND.abs(XI).lt.zero)THEN
        IF(abs(XR-1.D0).lt.zero)THEN
          LI2_HDEC=DCMPLX(ZETA2)
        ELSE
          LI2_HDEC=-DCMPLX(ZETA2/2.D0)
        ENDIF
        RETURN
      ELSEIF(R2.GT.1.D0.AND.RR.GT.0.5D0)THEN
        Y=(X-1.D0)/X
        LI2_HDEC=CLI2_HDEC(Y)+ZETA2-CDLOG(X)*CDLOG(1.D0-X)
     .          +0.5D0*CDLOG(X)**2
        RETURN
      ELSEIF(R2.GT.1.D0.AND.RR.LE.0.5D0)THEN
        Y=1.D0/X
        LI2_HDEC=-CLI2_HDEC(Y)-ZETA2-0.5D0*CDLOG(-X)**2
        RETURN
      ELSEIF(R2.LE.1.D0.AND.XR.GT.0.5D0)THEN
        Y=1.D0-X
        LI2_HDEC=-CLI2_HDEC(Y)+ZETA2-CDLOG(X)*CDLOG(1.D0-X)
       RETURN
      ELSEIF(R2.LE.1.D0.AND.XR.LE.0.5D0)THEN
        Y=X
        LI2_HDEC=CLI2_HDEC(Y)
        RETURN
      ENDIF
      END

      COMPLEX*16 FUNCTION CLI2_HDEC(X)
C--TAYLOR-EXPANSION FOR COMPLEX DILOGARITHM (SPENCE-FUNCTION)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMPLEX*16 X,Z
      COMMON/BERNOULLI_HDEC/B2(18),B12(18),B3(18)
      COMMON/POLY_HDEC/NBER
      N=NBER-1
      Z=-CDLOG(1.D0-X)
      CLI2_HDEC=B2(NBER)
      DO 111 I=N,1,-1
        CLI2_HDEC=Z*CLI2_HDEC+B2(I)
111   CONTINUE
      CLI2_HDEC=Z**2*CLI2_HDEC+Z
      RETURN
      END

      DOUBLE PRECISION FUNCTION FACTRL_HDEC(N)
C--DOUBLE PRECISION VERSION OF FACTORIAL
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      FACTRL_HDEC=1.D0
      IF(N.EQ.0)RETURN
      DO 999 I=1,N
        FACTRL_HDEC=FACTRL_HDEC*DFLOAT(I)
999   CONTINUE
      RETURN
      END

      SUBROUTINE BERNINI_HDEC(N)
C--INITIALIZATION OF COEFFICIENTS FOR POLYLOGARITHMS
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION B(18),PB(19)
      COMMON/BERNOULLI_HDEC/B2(18),B12(18),B3(18)
      COMMON/CONST_HDEC/ZETA2,ZETA3
      COMMON/POLY_HDEC/NBER
      NBER=N
      PI=4.D0*DATAN(1.D0)

      B(1)=-1.D0/2.D0
      B(2)=1.D0/6.D0
      B(3)=0.D0
      B(4)=-1.D0/30.D0
      B(5)=0.D0
      B(6)=1.D0/42.D0
      B(7)=0.D0
      B(8)=-1.D0/30.D0
      B(9)=0.D0
      B(10)=5.D0/66.D0
      B(11)=0.D0
      B(12)=-691.D0/2730.D0
      B(13)=0.D0
      B(14)=7.D0/6.D0
      B(15)=0.D0
      B(16)=-3617.D0/510.D0
      B(17)=0.D0
      B(18)=43867.D0/798.D0
      ZETA2=PI**2/6.D0
      ZETA3=1.202056903159594D0

      DO 995 I=1,18
        B2(I)=B(I)/FACTRL_HDEC(I+1)
        B12(I)=DFLOAT(I+1)/FACTRL_HDEC(I+2)*B(I)/2.D0
        PB(I+1)=B(I)
        B3(I)=0.D0
995   CONTINUE
      PB(1)=1.D0
      DO 996 I=1,18
      DO 996 J=0,I
        B3(I)=B3(I)+PB(J+1)*PB(I-J+1)/FACTRL_HDEC(I-J)/FACTRL_HDEC(J+1)
     .                                            /DFLOAT(I+1)
996   CONTINUE

      RETURN
      END

      DOUBLE PRECISION FUNCTION QQINT_HDEC(RAT,H1,H2)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      N = 2
      QQINT_HDEC = RAT**N * H1 + (1-RAT**N) * H2
      RETURN
      END

      DOUBLE PRECISION FUNCTION XITLA_HDEC(NO,ALP,ACC)
C--ITERATION ROUTINE TO DETERMINE IMPROVED LAMBDAS
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
      B0(NF)=33.D0-2.D0*NF
      B1(NF)=6.D0*(153.D0-19.D0*NF)/B0(NF)**2
      B2(NF)=27/2.D0*(2857-5033/9.D0*NF+325/27.D0*NF**2)/B0(NF)**3
      ALS2(NF,X,XLB)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB**2))
     .              *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB**2))
     .              /DLOG(X**2/XLB**2))
      ALS3(NF,X,XLB)=12.D0*PI/(B0(NF)*DLOG(X**2/XLB**2))
     .          *(1.D0-B1(NF)*DLOG(DLOG(X**2/XLB**2))
     .           /DLOG(X**2/XLB**2)
     .           +(B1(NF)**2*(DLOG(DLOG(X**2/XLB**2))**2
     .                      -DLOG(DLOG(X**2/XLB**2))-1)+B2(NF))
     .           /DLOG(X**2/XLB**2)**2)
      AA(NF)=12D0*PI/B0(NF)
      BB(NF)=B1(NF)/AA(NF)
      CC(NF)=B2(NF)/AA(NF)
      XIT(A,B,X)=A/2.D0*(1D0+DSQRT(1D0-4D0*B*DLOG(X)))
      XIT3(A,B,C,X)=A/2.D0*(1D0+DSQRT(1D0-4D0*B*DLOG(X)
     .          *(1-(A*B*(DLOG(X)**2-DLOG(X)-1)+C/B)/X/DLOG(X))))
      PI=4.D0*DATAN(1.D0)
      NF=5
      Q=AMZ
      XLB=Q*DEXP(-AA(NF)/ALP/2.D0)
      IF(NO.EQ.1)GOTO 111
      II=0
1     II=II+1
      X=DLOG(Q**2/XLB**2)
      A=AA(NF)/ALP
      B=BB(NF)*ALP
      C=CC(NF)*ALP
      IF(NO.EQ.2)THEN
       XX=XIT(A,B,X)
      ELSE
       XX=XIT3(A,B,C,X)
      ENDIF
      XLB=Q*DEXP(-XX/2.D0)
      Y1=ALP
      IF(NO.EQ.2)THEN
       Y2=ALS2(NF,Q,XLB)
      ELSE
       Y2=ALS3(NF,Q,XLB)
      ENDIF
      DY=DABS(Y2-Y1)/Y1
      IF(DY.GE.ACC) GOTO 1
111   XITLA_HDEC=XLB
      RETURN
      END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


***********************************************************************
        FUNCTION ETA_HDEC(C1,C2)
***********************************************************************
*       COMPLEX ETA-FUNKTION
*---------------------------------------------------------------------*
*       8.06.90    ANSGAR DENNER
***********************************************************************
        IMPLICIT   LOGICAL(A-Z)
        COMPLEX*16 ETA_HDEC,C1,C2
        REAL*8     PI,IM1,IM2,IM12

        PI     = 4D0*DATAN(1D0)
        IM1    = DIMAG(C1)
        IM2    = DIMAG(C2)
        IM12   = DIMAG(C1*C2)

        IF(IM1.LT.0D0.AND.IM2.LT.0D0.AND.IM12.GT.0D0) THEN
            ETA_HDEC = DCMPLX(0D0,2D0*PI)
        ELSE IF (IM1.GT.0D0.AND.IM2.GT.0D0.AND.IM12.LT.0D0) THEN
            ETA_HDEC = DCMPLX(0D0,-2D0*PI)
        ELSE
            ETA_HDEC = DCMPLX(0D0)
        END IF
        END
