      subroutine N2HDMcp_hdec()

      implicit none
c common block declarations
      double precision TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM
      integer ITYPE2HDM,I2HDM,IPARAM2HDM
      double precision ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM
      double precision amhiggs(3)
      integer IN2HDM,IPHASE
      double precision GAT,GAB,GLT,GLB,GHT,GHB,GZAH,GZAL,GHHH,GLLL,GHLL,
     .            GLHH,GHAA,GLAA,GLVV,GHVV,GLPM,GHPM,B,A,
     .            gllep,ghlep,galep
      double precision GF,ALPH,AMTAU,AMMUON,AMZ,AMW
c array declarations for output
      double precision ghihpwm(3),ghivv(3),ghipm(3),
     .     ghijk(3,3,3),ghiza(3),ghiaa(3),
     .     ghitop(3),ghibot(3),ghilep(3),gfd1(3),gfd2(3)
c internal variables
      double precision tgbet,cb,sb,sa1,ca1,sa2,ca2,sa3,ca3,v,vs,m12sq,
     .  L6,L7,L8,ma,mhc,L1,m11sq,L2
c loop variables
      integer i,j,k,l
c mixing matrix and internal temp array
      double precision U(3,3),ghitemp(3)
c 2HDM input for the type, mA, mH+, m12sq and tgbet (in)
      COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
c N2HDM input (in)
      Common/N2HDM_HDEC_PARAM/amhiggs,ALPH1_N2HDM,ALPH2_N2HDM,
     .      ALPH3_N2HDM,AVS_N2HDM,
     .      AL7_N2HDM,AL8_N2HDM,AM11SQ_N2HDM,
     .      AM22SQ_N2HDM,AMSSQ_N2HDM,
     .      IN2HDM,IPHASE
c SM input (in)
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
c N2HDM couplings (out)
      Common/N2HDM_COUP/ghitop,ghibot,ghilep,
     .      ghihpwm,ghivv,ghiza,
     .      ghipm,ghiaa,ghijk
c 2HDM couplings for the pseudoscalar (out)
      COMMON/COUP_HDEC/GAT,GAB,GLT,GLB,GHT,GHB,GZAH,GZAL,GHHH,GLLL,GHLL,
     .            GLHH,GHAA,GLAA,GLVV,GHVV,GLPM,GHPM,B,A
      COMMON/THDM_COUP_HDEC/gllep,ghlep,galep

c all phases **********************
      ma = AMHA2HDM
      mhc = AMHC2HDM
      v=1.d0/dsqrt(dsqrt(2.d0)*gf)

c broken phase *********************
      if(IPHASE.eq.1)then
        tgbet = TGBET2HDM
        cb = 1/dsqrt(tgbet*tgbet+1)
        sb = tgbet*cb
        m12sq = AM12SQ/cb/sb
c angles
        sa1 = dsin(ALPH1_N2HDM)
        ca1 = dcos(ALPH1_N2HDM)
        sa2 = dsin(ALPH2_N2HDM)
        ca2 = dcos(ALPH2_N2HDM)
        sa3 = dsin(ALPH3_N2HDM)
        ca3 = dcos(ALPH3_N2HDM)
c Mixing Matrix elements
        U(1,1) = ca1*ca2
        U(1,2) = sa1*ca2
        U(1,3) = sa2
        U(2,1) = -ca3*sa1 - ca1*sa2*sa3
        U(2,2) = ca1*ca3-sa1*sa2*sa3
        U(2,3) = ca2*sa3
        U(3,1) = -ca1*ca3*sa2+sa1*sa3
        U(3,2) = -ca3*sa1*sa2-ca1*sa3
        U(3,3) = ca2*ca3
c vs
        vs = AVS_N2HDM

c calculation of required lambdas from broken phase input
        L6=0
        L7=0
        L8=0
        do i=1,3,1
          L6 = L6 + amhiggs(i)**2*U(i,3)**2
          L7 = L7 + amhiggs(i)**2*U(i,1)*U(i,3)
          L8 = L8 + amhiggs(i)**2*U(i,2)*U(i,3)
        enddo
        L6=L6/vs**2
        L7=L7/cb/v/vs
        L8=L8/sb/v/vs

c dark phase with vs = 0 **************
      elseif(IPHASE.eq.2)then
        tgbet = TGBET2HDM
        cb = 1/dsqrt(tgbet*tgbet+1)
        sb = tgbet*cb
        m12sq = AM12SQ/cb/sb
c Mixing Matrix elements
        U(1,1) = 0
        U(1,2) = 0
        U(1,3) = 1
        U(2,1) = -dsin(ALPH2HDM)
        U(2,2) = dcos(ALPH2HDM)
        U(2,3) = 0
        U(3,1) = dcos(ALPH2HDM)
        U(3,2) = dsin(ALPH2HDM)
        U(3,3) = 0
c vs
        vs=0
c lambdas, L6 does not enter
        L6=1
        L7 = AL7_N2HDM
        L8 = AL8_N2HDM

c dark phase with v1=0 *************
      elseif(IPHASE.eq.3)then
c parameters
        m11sq = AM11SQ_N2HDM
c Mixing Matrix elements
        U(1,1) = 1
        U(1,2) = 0
        U(1,3) = 0
        U(2,1) = 0
        U(2,2) = dcos(ALPH2HDM)
        U(2,3) = dsin(ALPH2HDM)
        U(3,1) = 0
        U(3,2) = -dsin(ALPH2HDM)
        U(3,3) = dcos(ALPH2HDM)
c vs
        vs   = Avs_N2HDM
c Lambdas, L1 does not enter
        L2 = 0
        L6 = 0
        L8 = 0
        do i=1,3,1
          L2 = L2 + amhiggs(i)**2*U(i,2)**2
          L6 = L6 + amhiggs(i)**2*U(i,3)**2
          L8 = L8 + amhiggs(i)**2*U(i,2)*U(i,3)
        enddo
        L2 = L2/v**2
        L6 = L6/vs**2
        L8 = L8/v/vs
        L1   = 1.
        L7   = AL7_N2HDM
      endif

c **** couplings to fermions ****
c Higgs-fermion-fermion couplings
      if(iphase.eq.3)then
c       gfd1    = 0, fermions never couple to first doublet
        gfd2(1) = U(1,2)
        gfd2(2) = U(2,2)
        gfd2(3) = U(3,2)
      elseif(iphase.lt.3)then
        gfd1(1) = U(1,1)/cb
        gfd1(2) = U(2,1)/cb
        gfd1(3) = U(3,1)/cb
        gfd2(1) = U(1,2)/sb
        gfd2(2) = U(2,2)/sb
        gfd2(3) = U(3,2)/sb
      endif

c      print*,'ca1,-sa1',ca1,-sa1
c      print*,'sa1,ca1',sa1,ca1
      if(itype2hdm.eq.1) then
        do i=1,3,1
            ghitop(i) = gfd2(i)
            ghibot(i) = gfd2(i)
            ghilep(i) = gfd2(i)
        enddo
        if(iphase.eq.3)then
          gat   = 0
          gab   = 0
          galep = 0
        elseif(iphase.lt.3)then
          gat   = 1.D0/tgbet
          gab   = -1.D0/tgbet
          galep = -1.D0/tgbet
        endif
      elseif(itype2hdm.eq.2) then
            do i=1,3,1
                  ghitop(i) = gfd2(i)
                  ghibot(i) = gfd1(i)
                  ghilep(i) = gfd1(i)
            enddo
            gat   = 1.D0/tgbet
            gab   = tgbet
            galep = tgbet
      elseif(itype2hdm.eq.3) then
            do i=1,3,1
                  ghitop(i) = gfd2(i)
                  ghibot(i) = gfd2(i)
                  ghilep(i) = gfd1(i)
            enddo
            gat   = 1.D0/tgbet
            gab   =-1.D0/tgbet
            galep = tgbet
      elseif(itype2hdm.eq.4) then
            do i=1,3,1
                  ghitop(i) = gfd2(i)
                  ghibot(i) = gfd1(i)
                  ghilep(i) = gfd2(i)
            enddo
            gat   = 1.D0/tgbet
            gab   = tgbet
            galep = -1.D0/tgbet
      endif

c      print*,"fermion couplings"
c      do i=1,3,1
c            print*,i,ghitop(i),ghibot(i),ghilep(i)
c      enddo
c      print*,"A",gat,gab,galep
c      print*," "

c **** couplings with gauge bosons *****
      if(iphase.eq.3)then
        do i=1,3,1
          ghivv(i)=U(i,2)
          ghihpwm(i)= U(i,1)
          ghiza(i)=ghihpwm(i)
        enddo
      elseif(iphase.lt.3)then
        do i=1,3,1
            ghivv(i)=U(i,1)*cb+U(i,2)*sb
            ghihpwm(i)=U(i,1)*sb-U(i,2)*cb
            ghiza(i)=ghihpwm(i)
        enddo
      endif

c      print*,"gauge couplings"
c      do i=1,3,1
c            print*,i,ghivv(i),ghihpwm(i),ghiza(i)
c      enddo
c      print*," "

c **** triple Higgs couplings ****
c couplings to a charged Higgs and a pseudoscalar pair
      if(iphase.eq.3)then
        do i=1,3,1
          ghipm(i) = -2*m11sq/v*U(i,2)
     -             + L7*vs/v*(U(i,3)*v-U(i,2)*vs)
          ghiaa(i) = ghipm(i) + 2*ma**2/v*U(i,2)
          ghipm(i) = ghipm(i) + 2*mhc**2/v*U(i,2)
        enddo
      elseif(iphase.lt.3)then
      do i=1,3,1
        ghitemp(i)=amhiggs(i)**2*(U(i,2)*cb**2/sb+U(i,1)*sb**2/cb)
      enddo
      do i=1,3,1
        ghipm(i)= 1/v*(-m12sq*(U(i,2)/sb+U(i,1)/cb)
     -    + (1-U(i,3)**2) * ghitemp(i)
     -    - (U(i,3)*U(Mod(i,3)+1,3)) * ghitemp(Mod(i,3)+1)
     -    - (U(i,3)*U(Mod(i+1,3)+1,3)) * ghitemp(Mod(i+1,3)+1))
     -    + L7 * U(i,3)*vs*sb**2
     -    + L8 * U(i,3)*vs*cb**2
c       The ghipm below is not a typo.
        ghiaa(i) = ghipm(i) + 2*ma**2/v*(U(i,1)*cb+U(i,2)*sb)
        ghipm(i)   = ghipm(i) + 2*mhc**2/v*(U(i,1)*cb+U(i,2)*sb)
      enddo
      endif

c      print*,"charged and pseudoscalar couplings"
c      do i=1,3,1
c        ghipm(i) = 0D0
c        ghiaa(i) = 0D0
c        print*,i,ghipm(i),ghiaa(i)
c      enddo
c      print*," "

c triple scalar Higgs couplings

      if(IPHASE.eq.3)then
        ghijk=0
        do i=1,3,1
        do j=i,3,1
        do k=j,3,1
          ghijk(i,j,k) = ghijk(i,j,k) + 3*v*L2*U(i,2)*U(j,2)*U(k,2)
     -                 +3*vs*L6*U(i,3)*U(j,3)*U(k,3)
     -                 +L8*(v*(U(i,2)*U(j,3)*U(k,3)+U(i,3)*U(j,2)*U(k,3)
     -                         +U(i,3)*U(j,3)*U(k,2))
     -                    +vs*(U(i,3)*U(j,2)*U(k,2)+U(i,2)*U(j,3)*U(k,2)
     -                         +U(i,2)*U(j,2)*U(k,3)))
     -                 +2*(amhiggs(1)**2-m11sq)/v*(U(i,1)*U(j,1)*U(k,2)
     -                       +U(i,1)*U(j,2)*U(k,1)+U(i,2)*U(j,1)*U(k,1))
     -                 +L7*vs/v*(v*(U(i,1)*U(j,1)*U(k,3)
     -                             +U(i,1)*U(j,3)*U(k,1)
     -                             +U(i,3)*U(j,1)*U(k,1))
     -                         -vs*(U(i,1)*U(j,1)*U(k,2)
     -                             +U(i,1)*U(j,2)*U(k,1)
     -                             +U(i,2)*U(j,1)*U(k,1)))
        enddo
        enddo
        enddo

c     phases 1 and 2
      elseif(iphase.lt.3)then
        ghijk=0
        do i=1,3,1
        do j=i,3,1
        do k=j,3,1
          ghijk(i,j,k)=m12sq/v*(
     -      cb*(U(i,1)*U(j,2)*U(k,2)+U(i,2)*U(j,1)*U(k,2)
     -      +U(i,2)*U(j,2)*U(k,1)-3*U(i,2)*U(j,2)*U(k,2)/tgbet)
     -      + sb*(U(i,2)*U(j,1)*U(k,1)+U(i,1)*U(j,2)*U(k,1)
     -      +U(i,1)*U(j,1)*U(k,2)-3*U(i,1)*U(j,1)*U(k,1)*tgbet)
     -      )
          do l=1,3,1
            ghijk(i,j,k)=ghijk(i,j,k) + amhiggs(l)**2/v*(
     -         U(l,2)*(U(l,1)*(U(i,1)*U(j,2)*U(k,2)+U(i,2)*U(j,1)*U(k,2)
     -         +U(i,2)*U(j,2)*U(k,1))+3*U(l,2)*U(i,2)*U(j,2)*U(k,2))/sb
     -        +U(l,1)*(U(l,2)*(U(i,2)*U(j,1)*U(k,1)+U(i,1)*U(j,2)*U(k,1)
     -        +U(i,1)*U(j,1)*U(k,2))+3*U(l,1)*U(i,1)*U(j,1)*U(k,1))/cb
     -        )
        enddo
        ghijk(i,j,k)=ghijk(i,j,k) + L6*3*U(i,3)*U(j,3)*U(k,3)*vs
     -      + L7 * (vs*(U(i,3)*U(j,1)*U(k,1)+U(i,1)*U(j,3)*U(k,1)
     -          +U(i,1)*U(j,1)*U(k,3))
     -        + v*cb*(U(i,1)*U(j,3)*U(k,3)+U(i,3)*U(j,1)*U(k,3)
     -            +U(i,3)*U(j,3)*U(k,1)))
     -      + L8 * (vs*(U(i,3)*U(j,2)*U(k,2)+U(i,2)*U(j,3)*U(k,2)
     -          +U(i,2)*U(j,2)*U(k,3))
     -        + v*sb*(U(i,2)*U(j,3)*U(k,3)+U(i,3)*U(j,2)*U(k,3)
     -            +U(i,3)*U(j,3)*U(k,2)))
        enddo
        enddo
        enddo
      endif

c      print*,"neutral 3 Higgs couplings"
c      ghijk(1,1,2) = 0D0
c      ghijk(1,1,3) = 0D0
c      print*,112,ghijk(1,1,2),122,ghijk(1,2,2)
c      print*,113,ghijk(1,1,3),133,ghijk(1,3,3)
c      print*,223,ghijk(2,2,3),233,ghijk(2,3,3)
c      print*,123,ghijk(1,2,3)
c      print*," "

      if(iphase.eq.4)then
            ghitop = 0.
            ghibot = 0.
            ghilep = 0.
            ghivv= 0.
            ghihpwm= 0.
            ghiza=0.
            ghipm=0.
            ghiaa=0.
            ghijk = 0.
            gat = 0.
            gab = 0.
            galep = 0.
c           couplings present for Hsm
            ghitop(3) = 1.
            ghibot(3) = 1.
            ghilep(3) = 1.
            ghivv(3) = 1.
c           couplings present for HDD
            ghihpwm(1) = 1.
            ghiza(1) = 1.
c           triple Higgs couplings
            ghipm(3) = 2./v*(mhc**2-AM22SQ_N2HDM)
            ghiaa(3) = 2./v*(ma**2-AM22SQ_N2HDM)
            ghijk(3,3,3) = 3.*amhiggs(3)**2/v
            ghijk(1,1,3) = 2./v*(amhiggs(1)**2-AM22SQ_N2HDM)
            ghijk(2,2,3) = 2./v*(amhiggs(2)**2-AMSSQ_N2HDM)
            do i=1,2,1
                  ghijk(i,3,i) = ghijk(i,i,3)
                  ghijk(3,i,i) = ghijk(i,i,3)
            enddo
      endif
      return
      end
