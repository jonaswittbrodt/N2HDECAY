## Specify fortran compiler and compilation flags ##

FC = gfortran
FFLAGS = -O3


#### Do not change below ####
OBJS = src/close_hdec.o src/hdec.o src/hdecay_core.o src/head_hdec.o src/hgaga.o src/hgg.o src/n2hdecay_main.o src/n2hdmcp_hdec.o src/read_hdec.o src/write_hdec.o


.f.o:
	$(FC) -c $(FFLAGS) $*.f -o $*.o

n2hdecay: $(OBJS)
	$(FC) $(FFLAGS) $(OBJS) -o n2hdecay

clean:
	rm -f $(OBJS)
	rm -f n2hdecay
