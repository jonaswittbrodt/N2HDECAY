# Changelog

All notable changes to this project will be documented in this file.

## 1.0.0 - 02.05.18
Initial Release

## 1.1.0 - 23.01.20
Added the dark singlet and doublet phase
