# N2HDECAY

N2HDECAY is a code to calculate the Higgs boson decay widths and branching
ratios in the N2HDM. The code is based on [HDECAY][] (see *[hep-ph/9704448]* and
*[1801.09506]*). It includes all QCD corrections available in HDECAY as well as
off-shell decays into gauge bosons and top-quarks.

**The latest release can be downloaded [here][tarball] and the latest version of
the manual is available [here][manual].**

### Contributors and Citation Guide

The code was written by Isabell Engeln, Margarete Mühlleitner and Jonas
Wittbrodt in collaboration with Marco Sampaio and Rui Santos.

**If you use N2HDECAY for a publication please cite *[1612.01309]* and the
published version of the manual *[1805.00966]*.**


## The N2HDM

The N2HDM extends the Two-Higgs-Doublet Model (2HDM) by an additional real
singlet with a Z_2 symmetry. Depending on which symmetries are softly or
spontaneously broken, the N2HDM has four distinct phases.

 - The broken phase, where both Higgs doublets and the singlet acquire vacuum
   expectation values (VEVs), has been studied in detail in *[1612.01309]*. It
   features a rich phenomenology at current and future collider experiments.
 - The dark singlet phase in which the singlet does not acquire a VEV and the
   corresponding Z_2 becomes a dark parity. This phase adds scalar singlet DM
   with a Higgs portal to the 2HDM. It has been studied
   [here][Isabell's thesis].
 - The inert doublet phase. This phase can only be realized if the doublet Z_2
   of the N2HDM is neither softly nor spontaneously broken. The result is an
   inert doublet model where the additional real singlet mixes with the SM Higgs
   boson. The dark sector consists of a dark scalar, dark pseudsoscalar and dark
   charged Higgs boson. This phase has also been studied
   [here][Isabell's thesis].
 - The phase where both the singlet and the doublet are dark. This phase has a
   visible sector with a single SM-like Higgs boson and two distinct dark
   sectors.

A detailed description of the model is given in the [manual]. When [the
code][tarball] is downloaded an up-to-date pdf version is included in the `doc`
folder. All changes in the code that affect the results are documented in the
`CHANGELOG`.


## Compilation

Compiling the code requires a Fortran compiler such as `gfortran`. If `gfortran`
is installed, simply run `make` to compile the code. The compiler and the flags
to use can be adjusted in the `Makefile`.

Alternatively the code can be compiled using `cmake`, i.e.

    mkdir build && cd build
    cmake ..
    make
    make test

The last line runs the functional test suite, which checks the compiled version
using a set of example input and output files (located in the `tests` folder).
Running the test suite requires at least Python 3.5.

## User Instructions
Upon compilation an executable called `n2hdecay` is created. By default input is
read from `./n2hdecay.in` and output is written to the current directory. The
input file can be optionally specified via the `-i` flag and the output folder
via the `-o` flag. For example:

    ./n2hdecay -i n2hdecay_example.in -o out/

uses the example input file `n2hdecay_example.in` and writes its output to a
sub-folder called out. The output path always has to end with a `/` and already
exist.

The input file is a simple text file containing all of the required parameters.
They are described in detail in the [manual].


### Python Utility Functions

We provide python utility functions for handling input and output in
`tools/IO_N2HDECAY.py`.

 - The function `IO_N2HDECAY.write(filepath, phase, params)` generates an input
   file at `filepath` for the given `phase` and the parameters in the `params`
   dictionary.
 - The function `IO_N2HDECAY.read(brfolder, phase = None)` reads the output files
   from `brfolder` and returns their contents as a python dictionary. If the
   `phase` is not specified it is deduced from the files present in the folder
   (as long as the folder only contains output files from one phase).

The script `tools/parameterscan.py` demonstrates how this interface can be used for a
parameter scan.


## Structure of the Source Code
The source files of the program are:
 - `src/close_hdec.f`: Subroutine for cleanup.
 - `src/hdec.f`: Subroutine to calculate the partial widths and branching
   ratios.
 - `src/hdecay_core.f`: Utility functions and subroutines. Taken directly from
   HDECAY.
 - `src/head_hdec.f`: Subroutine to write the output file headers.
 - `src/hgaga.f`: Subroutines for the decay into two photons. Taken directly from
   HDECAY.
 - `src/hgg.f`: Subroutines for the decay into two gluons. Taken directly from
   HDECAY.
 - `src/n2hdecay_main.f`: Main program.
 - `src/n2hdmcp_hdec.f`: Subroutine to calculate the N2HDM Higgs couplings from
   the input.
 - `src/read_hdec.f`: Initialization and input subroutine.
 - `src/write_hdec.f`: Subroutine to write the results into the output
   files.

The `doc` subfolder contains the manual. The test script with example input and
outputs is in the `tests` subfolder. The Python utility functions and
examples are located in the `tools` folder.

<!-- Below are the links referenced in the text. -->

<!-- N2HDECAY references -->
[1612.01309]: http://inspirehep.net/record/1501710
[1805.00966]: http://inspirehep.net/record/1671515
[Isabell's thesis]: http://inspirehep.net/record/1667833

<!-- Up to date N2HDECAY source code and manual -->
[tarball]: https://gitlab.com/api/v4/projects/jonaswittbrodt%2Fn2hdecay/jobs/artifacts/release/raw/build/N2HDECAY.tar.gz?job=tarball
[manual]: https://gitlab.com/api/v4/projects/jonaswittbrodt%2Fn2hdecay/jobs/artifacts/release/raw/doc/N2HDECAY.pdf?job=doc

<!-- HDECAY references -->
[HDECAY]:  http://tiger.web.psi.ch/proglist.html
[hep-ph/9704448]: http://inspirehep.net/record/442662
[1801.09506]: http://inspirehep.net/record/1650944
