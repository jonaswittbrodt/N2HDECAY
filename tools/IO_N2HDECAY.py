def read(brfolder, phase=None):
    """
    Reads the N2HDECAY output in the brfolder and returns a dictionary containing all of the N2HDECAY results. If the phase is not specified it is deduced automatically based on the files present in brfolder. This only works if the folder only contains output files for one phase.
    """
    import os.path
    import glob

    BP_channels = {
        "br.A_N2HDM_a": [
            "m_A",
            "b_A_BB",
            "b_A_tautau",
            "b_A_mumu",
            "b_A_ss",
            "b_A_cc",
            "b_A_tt",
        ],
        "br.A_N2HDM_b": ["m_A", "b_A_gg", "b_A_gamgam", "b_A_Zgam"],
        "br.A_N2HDM_c": ["m_A", "b_A_ZH1", "b_A_ZH2", "b_A_ZH3", "b_A_WpHm", "w_A"],
        "br.H+_N2HDM_a": [
            "m_Hc",
            "b_Hc_cb",
            "b_Hc_taunu",
            "b_Hc_us",
            "b_Hc_cs",
            "b_Hc_tb",
        ],
        "br.H+_N2HDM_b": ["m_Hc", "b_Hc_cd", "b_Hc_ub", "b_Hc_ts", "b_Hc_td"],
        "br.H+_N2HDM_c": [
            "m_Hc",
            "b_Hc_WH1",
            "b_Hc_WH2",
            "b_Hc_WH3",
            "b_Hc_WA",
            "w_Hc",
        ],
        "br.H1_N2HDM_a": [
            "m_H1",
            "b_H1_bb",
            "b_H1_tautau",
            "b_H1_mumu",
            "b_H1_ss",
            "b_H1_cc",
            "b_H1_tt",
        ],
        "br.H1_N2HDM_b": [
            "m_H1",
            "b_H1_gg",
            "b_H1_gamgam",
            "b_H1_Zgam",
            "b_H1_WW",
            "b_H1_ZZ",
        ],
        "br.H1_N2HDM_c": [
            "m_H1",
            "b_H1_AA",
            "b_H1_ZA",
            "b_H1_WpHm",
            "b_H1_HpHm",
            "w_H1",
        ],
        "br.H2_N2HDM_a": [
            "m_H2",
            "b_H2_bb",
            "b_H2_tautau",
            "b_H2_mumu",
            "b_H2_ss",
            "b_H2_cc",
            "b_H2_tt",
        ],
        "br.H2_N2HDM_b": [
            "m_H2",
            "b_H2_gg",
            "b_H2_gamgam",
            "b_H2_Zgam",
            "b_H2_WW",
            "b_H2_ZZ",
        ],
        "br.H2_N2HDM_c": [
            "m_H2",
            "b_H2_H1H1",
            "b_H2_AA",
            "b_H2_ZA",
            "b_H2_WpHm",
            "b_H2_HpHm",
            "w_H2",
        ],
        "br.H3_N2HDM_a": [
            "m_H3",
            "b_H3_bb",
            "b_H3_tautau",
            "b_H3_mumu",
            "b_H3_ss",
            "b_H3_cc",
            "b_H3_tt",
        ],
        "br.H3_N2HDM_b": [
            "m_H3",
            "b_H3_gg",
            "b_H3_gamgam",
            "b_H3_Zgam",
            "b_H3_WW",
            "b_H3_ZZ",
        ],
        "br.H3_N2HDM_c": [
            "m_H3",
            "b_H3_H1H1",
            "b_H3_H1H2",
            "b_H3_H2H2",
            "b_H3_AA",
            "b_H3_ZA",
            "b_H3_WpHm",
        ],
        "br.H3_N2HDM_d": ["m_H3", "b_H3_HpHm", "w_H3"],
        "br.top": ["m_Hc", "b_t_Wb", "b_t_Hcb", "w_t"],
    }

    DSP_channels = {
        "br.A_N2HDM_a": [
            "m_A",
            "b_A_BB",
            "b_A_tautau",
            "b_A_mumu",
            "b_A_ss",
            "b_A_cc",
            "b_A_tt",
        ],
        "br.A_N2HDM_b": ["m_A", "b_A_gg", "b_A_gamgam", "b_A_Zgam"],
        "br.A_N2HDM_c": ["m_A", "b_A_ZH1", "b_A_ZH2", "b_A_WpHm", "w_A"],
        "br.H+_N2HDM_a": [
            "m_Hc",
            "b_Hc_cb",
            "b_Hc_taunu",
            "b_Hc_us",
            "b_Hc_cs",
            "b_Hc_tb",
        ],
        "br.H+_N2HDM_b": ["m_Hc", "b_Hc_cd", "b_Hc_ub", "b_Hc_ts", "b_Hc_td"],
        "br.H+_N2HDM_c": ["m_Hc", "b_Hc_WH1", "b_Hc_WH2", "b_Hc_WA", "w_Hc"],
        "br.H1_N2HDM_a": [
            "m_H1",
            "b_H1_bb",
            "b_H1_tautau",
            "b_H1_mumu",
            "b_H1_ss",
            "b_H1_cc",
            "b_H1_tt",
        ],
        "br.H1_N2HDM_b": [
            "m_H1",
            "b_H1_gg",
            "b_H1_gamgam",
            "b_H1_Zgam",
            "b_H1_WW",
            "b_H1_ZZ",
        ],
        "br.H1_N2HDM_c": [
            "m_H1",
            "b_H1_HDHD",
            "b_H1_AA",
            "b_H1_ZA",
            "b_H1_WpHm",
            "b_H1_HpHm",
            "w_H1",
        ],
        "br.H2_N2HDM_a": [
            "m_H2",
            "b_H2_bb",
            "b_H2_tautau",
            "b_H2_mumu",
            "b_H2_ss",
            "b_H2_cc",
            "b_H2_tt",
        ],
        "br.H2_N2HDM_b": [
            "m_H2",
            "b_H2_gg",
            "b_H2_gamgam",
            "b_H2_Zgam",
            "b_H2_WW",
            "b_H2_ZZ",
        ],
        "br.H2_N2HDM_c": [
            "m_H2",
            "b_H2_HDHD",
            "b_H2_H1H1",
            "b_H2_AA",
            "b_H2_ZA",
            "b_H2_WpHm",
            "b_H2_HpHm",
        ],
        "br.H2_N2HDM_d": ["m_H2", "w_H2"],
        "br.top": ["m_Hc", "b_t_Wb", "b_t_Hcb", "w_t"],
    }

    IDP_channels = {
        "br.AD_N2HDM": ["m_AD", "b_AD_ZHD", "b_AD_WpHDm", "w_AD"],
        "br.HD_N2HDM": ["m_HD", "b_HD_ZAD", "b_HD_WpHDm", "w_HD"],
        "br.HD+_N2HDM": ["m_HDc", "b_HDc_WHD", "b_HDc_WAD", "w_HDc"],
        "br.H1_N2HDM_a": [
            "m_H1",
            "b_H1_bb",
            "b_H1_tautau",
            "b_H1_mumu",
            "b_H1_ss",
            "b_H1_cc",
            "b_H1_tt",
        ],
        "br.H1_N2HDM_b": [
            "m_H1",
            "b_H1_gg",
            "b_H1_gamgam",
            "b_H1_Zgam",
            "b_H1_WW",
            "b_H1_ZZ",
        ],
        "br.H1_N2HDM_c": [
            "m_H1",
            "b_H1_HDHD",
            "b_H1_ADAD",
            "b_H1_ZAD",
            "b_H1_WpHDm",
            "b_H1_HDpHDm",
            "w_H1",
        ],
        "br.H2_N2HDM_a": [
            "m_H2",
            "b_H2_bb",
            "b_H2_tautau",
            "b_H2_mumu",
            "b_H2_ss",
            "b_H2_cc",
            "b_H2_tt",
        ],
        "br.H2_N2HDM_b": [
            "m_H2",
            "b_H2_gg",
            "b_H2_gamgam",
            "b_H2_Zgam",
            "b_H2_WW",
            "b_H2_ZZ",
        ],
        "br.H2_N2HDM_c": [
            "m_H2",
            "b_H2_HDHD",
            "b_H2_H1H1",
            "b_H2_ADAD",
            "b_H2_ZAD",
            "b_H2_WpHDm",
            "b_H2_HDpHDm",
        ],
        "br.H2_N2HDM_d": ["m_H2", "w_H2"],
    }

    DSDP_channels = {
        "br.AD_N2HDM": ["m_AD", "b_AD_ZHD", "b_AD_WpHDm", "w_AD"],
        "br.HDD_N2HDM": ["m_HDD", "b_HDD_ZAD", "b_HDD_WpHDm", "w_HD"],
        "br.HD+_N2HDM": ["m_HDc", "b_HDc_WHD", "b_HDc_WAD", "w_HDc"],
        "br.Hsm_N2HDM_a": [
            "m_Hsm",
            "b_Hsm_bb",
            "b_Hsm_tautau",
            "b_Hsm_mumu",
            "b_Hsm_ss",
            "b_Hsm_cc",
            "b_Hsm_tt",
        ],
        "br.Hsm_N2HDM_b": [
            "m_Hsm",
            "b_Hsm_gg",
            "b_Hsm_gamgam",
            "b_Hsm_Zgam",
            "b_Hsm_WW",
            "b_Hsm_ZZ",
        ],
        "br.Hsm_N2HDM_c": [
            "m_Hsm",
            "b_Hsm_HDDHDD",
            "b_Hsm_ADAD",
            "b_Hsm_HDpHDm",
            "b_Hsm_HDSHDS",
            "w_Hsm",
        ],
    }

    channels = [{}, BP_channels, DSP_channels, IDP_channels, DSDP_channels]

    def deduce_phase(files):
        filenames = sorted([os.path.basename(x) for x in files])
        for i in (1, 2, 3, 4):
            if filenames == sorted(list(channels[i].keys())):
                return i
        return False

    files = []
    if not phase:
        files = glob.glob(os.path.join(brfolder, "br.*"))
        phase = deduce_phase(files)
        if not phase:
            raise RuntimeError(
                "Could not deduce phase, make sure that the folder only contains the output of a single run."
            )
    else:
        try:
            files = [os.path.join(brfolder, x) for x in channels[phase].keys()]
        except IndexError:
            raise RuntimeError(
                "Unrecognized phase given, accepted values are 1 (broken phase), 2 (DSP), 3 (IDP) or None (deduce phase from files)."
            )

    brdict = {}
    for infile in files:
        with open(infile, "r") as f:
            lines = f.readlines()
        brdict.update(
            dict(
                zip(
                    channels[phase][os.path.basename(infile)],
                    [float(x) for x in lines[-1].split()],
                )
            )
        )
    return brdict


def defaultSMparams():
    """
    The default values used for the SM parameters.
    """
    return {
        "alpha_s": 0.118,
        "m_s": 0.095,
        "m_c": 0.986,
        "m_b": 4.180,
        "m_t": 172.5,
        "m_tau": 1.77682,
        "m_mu": 0.1056583715,
        "inv_alpha": 137.0359997,
        "GF": 1.1663787e-5,
        "w_W": 2.08430,
        "w_Z": 2.49427,
        "m_W": 80.35797,
        "m_Z": 91.15348,
        "V_tb": 0.9991,
        "V_ts": 0.0404,
        "V_td": 0.00867,
        "V_cb": 0.0412,
        "V_cs": 0.97344,
        "V_cd": 0.22520,
        "V_ub": 0.00351,
        "V_us": 0.22534,
        "V_ud": 0.97427,
    }


def write(filepath, phase, params):
    """
    Write an n2hdecay input file for the given phase to filepath.
    The parameters to use are given in the dict params.
    If any of the SM parameters are specified in params they will be
    substituted for the default values.
    """
    smpars = defaultSMparams()

    for x in params:
        if x in smpars:
            smpars[x] = params[x]

    lines = [
        "********************* N2HDECAY Input **********************",
        "* PHASE: 1 (broken), 2 (dark S), 3 (dark D), 4 (dark S+D) *",
        "***** YUKTYPE: 1, 2, 3 (lepton-specific), 4 (flipped) *****",
        "***** see 1805.00966 or doc/N2HDECAY.pdf for details ******",
    ]
    if phase == 1:
        lines += [
            "PHASE   = 1",
            "YUKTYPE = {}".format(params["yuktype"]),
            "***********************************************************",
            "tbeta   = {}".format(params["tbeta"]),
            "m12sq   = {}".format(params["m12sq"]),
            "m_A     = {}".format(params["m_A"]),
            "m_Hc    = {}".format(params["m_Hc"]),
            "vs      = {}".format(params["vs"]),
            "********************** PHASE = 1, broken phase ************",
            "m_H1    = {}".format(params["m_H1"]),
            "m_H2    = {}".format(params["m_H2"]),
            "m_H3    = {}".format(params["m_H3"]),
            "a1      = {}".format(params["a1"]),
            "a2      = {}".format(params["a2"]),
            "a3      = {}".format(params["a3"]),
            "*************** PHASE = 2, dark singlet phase *************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L7      = 1.",
            "L8      = 1.",
            "L6 is irrelevant for the decays",
            "************* PHASE = 3, dark doublet phase ***************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L8      = 1.",
            "m22sq   = 1.",
            "L2 is irrelevant for the decays",
            "********* PHASE = 4, dark singlet & doublet phase *********",
            "m_Hsm   = 1.",
            "m_HDD   = 1.",
            "m_HDS   = 1.",
            "m22sq   = 1.",
            "mssq    = 1.",
            "L2, L6 and L8 are irrelevant for the decays",
            "***********************************************************",
        ]
    elif phase == 2:
        lines += [
            "PHASE   = 2",
            "YUKTYPE = {}".format(params["yuktype"]),
            "***********************************************************",
            "tbeta   = {}".format(params["tbeta"]),
            "m12sq   = {}".format(params["m12sq"]),
            "m_A     = {}".format(params["m_A"]),
            "m_Hc    = {}".format(params["m_Hc"]),
            "vs      = 0.",
            "********************** PHASE = 1, broken phase ************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_H3    = 1.",
            "a1      = 1.",
            "a2      = 1.",
            "a3      = 1.",
            "*************** PHASE = 2, dark singlet phase *************",
            "m_H1    = {}".format(params["m_H1"]),
            "m_H2    = {}".format(params["m_H2"]),
            "m_HD    = {}".format(params["m_HD"]),
            "alpha   = {}".format(params["alpha"]),
            "L7      = {}".format(params["L7"]),
            "L8      = {}".format(params["L8"]),
            "L6 is irrelevant for the decays",
            "************* PHASE = 3, dark doublet phase ***************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L8      = 1.",
            "m22sq   = 1.",
            "L2 is irrelevant for the decays",
            "********* PHASE = 4, dark singlet & doublet phase *********",
            "m_Hsm   = 1.",
            "m_HDD   = 1.",
            "m_HDS   = 1.",
            "m22sq   = 1.",
            "mssq    = 1.",
            "L2, L6 and L8 are irrelevant for the decays",
            "***********************************************************",
        ]
    elif phase == 3:
        lines += [
            "PHASE   = 3",
            "YUKTYPE = 0",
            "***********************************************************",
            "tbeta   = 1.",
            "m12sq   = 0.",
            "m_A     = {}".format(params["m_AD"]),
            "m_Hc    = {}".format(params["m_HDc"]),
            "vs      = {}".format(params["vs"]),
            "********************** PHASE = 1, broken phase ************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_H3    = 1.",
            "a1      = 1.",
            "a2      = 1.",
            "a3      = 1.",
            "*************** PHASE = 2, dark singlet phase *************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L7      = 1.",
            "L8      = 1.",
            "L6 is irrelevant for the decays",
            "************* PHASE = 3, dark doublet phase ***************",
            "m_H1    = {}".format(params["m_H1"]),
            "m_H2    = {}".format(params["m_H2"]),
            "m_HD    = {}".format(params["m_HD"]),
            "alpha   = {}".format(params["alpha"]),
            "L8      = {}".format(params["L8"]),
            "m22sq   = {}".format(params["m22sq"]),
            "L2 is irrelevant for the decays",
            "********* PHASE = 4, dark singlet & doublet phase *********",
            "m_Hsm   = 1.",
            "m_HDD   = 1.",
            "m_HDS   = 1.",
            "m22sq   = 1.",
            "mssq    = 1.",
            "L2, L6 and L8 are irrelevant for the decays",
            "***********************************************************",
        ]
    elif phase == 4:
        lines += [
            "PHASE   = 4",
            "YUKTYPE = 0",
            "***********************************************************",
            "tbeta   = 1.",
            "m12sq   = 0.",
            "m_A     = {}".format(params["m_AD"]),
            "m_Hc    = {}".format(params["m_HDc"]),
            "vs      = 0.",
            "********************** PHASE = 1, broken phase ************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_H3    = 1.",
            "a1      = 1.",
            "a2      = 1.",
            "a3      = 1.",
            "*************** PHASE = 2, dark singlet phase *************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L7      = 1.",
            "L8      = 1.",
            "L6 is irrelevant for the decays",
            "************* PHASE = 3, dark doublet phase ***************",
            "m_H1    = 1.",
            "m_H2    = 1.",
            "m_HD    = 1.",
            "alpha   = 1.",
            "L8      = 1.",
            "m22sq   = 1.",
            "L2 is irrelevant for the decays",
            "********* PHASE = 4, dark singlet & doublet phase *********",
            "m_Hsm   = {}".format(params["m_Hsm"]),
            "m_HDD   = {}".format(params["m_HDD"]),
            "m_HDS   = {}".format(params["m_HDS"]),
            "m22sq   = {}".format(params["m22sq"]),
            "mssq    = {}".format(params["mssq"]),
            "L2, L6 and L8 are irrelevant for the decays",
            "***********************************************************",
        ]

    lines += [
        "************* SM Input, hdecay default values *************",
        "ALS(MZ)  = {}".format(smpars["alpha_s"]),
        "MSBAR(2) = {}".format(smpars["m_s"]),
        "MCBAR(3) = {}".format(smpars["m_c"]),
        "MBBAR(MB)= {}".format(smpars["m_b"]),
        "MT       = {}".format(smpars["m_t"]),
        "MTAU     = {}".format(smpars["m_tau"]),
        "MMUON    = {}".format(smpars["m_mu"]),
        "1/ALPHA  = {}".format(smpars["inv_alpha"]),
        "GF       = {}".format(smpars["GF"]),
        "GAMW     = {}".format(smpars["w_W"]),
        "GAMZ     = {}".format(smpars["w_Z"]),
        "MZ       = {}".format(smpars["m_Z"]),
        "MW       = {}".format(smpars["m_W"]),
        "VTB      = {}".format(smpars["V_tb"]),
        "VTS      = {}".format(smpars["V_ts"]),
        "VTD      = {}".format(smpars["V_td"]),
        "VCB      = {}".format(smpars["V_cb"]),
        "VCS      = {}".format(smpars["V_cs"]),
        "VCD      = {}".format(smpars["V_cd"]),
        "VUB      = {}".format(smpars["V_ub"]),
        "VUS      = {}".format(smpars["V_us"]),
        "VUD      = {}".format(smpars["V_ud"]),
        "***********************************************************",
    ]

    with open(filepath, "w") as f:
        f.write("\n".join(lines))


### the code used to generate the example input files is below

# phase1 = {
#     "yuktype": 1,
#     "tbeta": 1.17639226,
#     "m12sq": 3.28390121e5,
#     "m_A": 9.02919728e2,
#     "m_Hc": 8.59398112e2,
#     "m_H1": 125.09,
#     "m_H2": 8.17422761e2,
#     "m_H3": 9.76339405e2,
#     "a1": 0.79503834,
#     "a2": 0.13549279,
#     "a3": 1.46729273,
#     "vs": 1.49629673e3,
# }
# write("tests/phase1.in", phase=1, params=phase1)

# phase2 = {
#     "yuktype": 1,
#     "tbeta": 1.17639226,
#     "m12sq": 3.28390121e5,
#     "m_A": 9.02919728e2,
#     "m_Hc": 8.59398112e2,
#     "m_H1": 125.09,
#     "m_H2": 8.17422761e2,
#     "m_HD": 50.0,
#     "alpha": 1.46729273,
#     "L7": 0.5,
#     "L8": 0.3,
# }
# write("tests/phase2.in", phase=2, params=phase2)

# phase3 = {
#     "m_AD": 9.02919728e2,
#     "m_HDc": 8.59398112e2,
#     "vs": 1.49629673e3,
#     "m_H1": 125.09,
#     "m_H2": 8.17422761e2,
#     "m_HD": 5e2,
#     "alpha": 1.46729273,
#     "L8": 0.5,
#     "m22sq": 2.5e2,
# }
# write("tests/phase3.in", phase=3, params=phase3)

# phase4 = {
#     "m_AD": 9.02919728e2,
#     "m_HDc": 8.59398112e2,
#     "m_Hsm": 125.09,
#     "m_HDD": 60.0,
#     "m_HDS": 5e2,
#     "m22sq": 2.5e2,
#     "mssq": 1e3,
# }
# write("tests/phase4.in", phase=4, params=phase4)

