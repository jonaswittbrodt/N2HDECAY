#############################################################################
# A simple example of a parameter scan using the N2HDECAY python interface. #
#############################################################################
import IO_N2HDECAY
import numpy as np
import subprocess
import pandas as pd


results = pd.DataFrame() # create the table to store the results in

for tbeta in np.linspace(1, 40, 50):  # scan over tbeta in 50 steps from 1 to 40
    param = {'yuktype': 1,
             'tbeta': tbeta,
             'm12sq': 3.28390121e5,
             'm_A': 9.02919728e2, 'm_Hc': 8.59398112e2,
             'm_H1': 125.09, 'm_H2': 8.17422761e2, 'm_H3': 9.76339405e2,
             'a1': 0.79503834, 'a2': 0.13549279, 'a3': 1.46729273,
             'vs': 1.49629673e3}
    IO_N2HDECAY.write('scan.in', phase=1, params=param) # write the input file
    subprocess.run(args=['../build/n2hdecay', '-i', 'scan.in', '-o', 'scanout/']) # run n2hdecay (the output folder scanout/ has to exist)
    out = IO_N2HDECAY.read('scanout/') # read the output
    results = results.append({**param, ** out}, ignore_index=True)  # combine input and output into a table

results.to_csv('scanresults.out', sep = '\t')  # write results to a file
